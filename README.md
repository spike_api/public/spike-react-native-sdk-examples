# Spike React Native SDK Examples

## Demo example app

Demo example project provides fully functional React Native application. Project created to demonstrate Spike SDK integration into real project.

## Dev example

Dev example project provides playground to test Spike SDK functions/features with different input. Code fully prepared to be modified to check Spike SDK behavior.
