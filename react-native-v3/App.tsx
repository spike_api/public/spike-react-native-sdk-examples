/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  Button,
  SafeAreaView,
  View,
} from 'react-native';
import StatisticsScreen from './src/StatisticsScreen';
import ActivitiesScreen from './src/ActivitiesScreen';
import SleepScreen from './src/SleepScreen';
import BackgrounScreen from './src/BackgroundScreen';

enum Screen {
  statistics = "statistics",
  activities = "activities",
  sleep = "sleep",
  background = "background",
}

// ------------------------------- READ ME -------------------------------
//
// For examples of using SpikeSDK see:
// - Statistics like steps, distance etc.:  src/StatisticsScreen.tsx
// - Workouts: src/ActivitiesScreen.tsx
// - Sleep: src/SleepScreen.tsx
//
// ------------------------------------------------------------------------

function App(): React.JSX.Element {

  const [screen, setScreen] = useState<Screen | undefined>(undefined);
  
  return (
    <SafeAreaView>
      { screen 
      ? 
        <View>
          {screen === Screen.statistics && <StatisticsScreen />}
          {screen === Screen.activities && <ActivitiesScreen />}
          {screen === Screen.sleep && <SleepScreen />}
          {screen === Screen.background && <BackgrounScreen />}
                  
        </View>
      :
        <View>
          <Button title="Statistics" onPress={() => setScreen(Screen.statistics)} />
          <Button title="Activities" onPress={() => setScreen(Screen.activities)} />
          <Button title="Sleep"      onPress={() => setScreen(Screen.sleep)} />
          <Button title="Background delivery" onPress={() => setScreen(Screen.background)} />
        </View>

      }      

    </SafeAreaView>
  );
}



export default App;
