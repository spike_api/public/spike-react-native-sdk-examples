/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import Config from 'react-native-config';
import Spike, { ActivityConfig, MetricType, SpikeRecord, SpikeRNConnectionAPIv3 } from 'react-native-spike-sdk';

// --------------------------------- CONNECT ---------------------------------
function ActivitiesScreen(): React.JSX.Element {

  const [connectionV3, setConnectionV3] = useState<SpikeRNConnectionAPIv3>();
  const [errorMessage, setError] = useState<string | undefined>();

  const onConnect = async () => {

    console.log(`Start connection for ${Config.APP_USER}`);

    // Create SpikeSDK Connection
    try {
      const connection = await Spike.createConnectionAPIv3({
        applicationId: Number(Config.APP_ID),
        signature: Config.APP_SECRET,
        endUserId: Config.APP_USER,
      });

      setConnectionV3(connection);
    } catch (error) {
      console.log(`${error}`);
    }
  };

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          { errorMessage ? <Text style={{color: '#ff0000'}}>{errorMessage}</Text> : null }

          { connectionV3
          ? <PermissionsView
              spikeConnection={connectionV3}
              onError={(error) => {
                setError(error);
                console.log(error);
              }}
            />
          : <Button
              title="Connect activities"
              onPress={onConnect}
            />
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

// --------------------------------- PERMISSIONS ---------------------------------

type PermissionsViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  onError: (error: string | undefined) => void;
};

function PermissionsView({spikeConnection, onError}: PermissionsViewProps): React.JSX.Element {
  const [permissionsRequested, setPermissionsRequested] = useState(false);

  // Change to type(s) you want
  const metricTypes = [MetricType.distanceTotal];
  const activityConfig = new ActivityConfig({
    includeMetricTypes: metricTypes,
  });

  const onGetHealthPermissions = async () => {

    // Get user permissions for reading data from HealthKit on iOS or Health Connect on Android.
    // If you need more granular control over what happens on Android, please look into SpikeSDK
    // documentation to find out what other functionality is available.
    // 
    // If you plan to have different requests for activities you can pass multiple activityConfigs,
    // to get permissions for all of them in one go.
    try {
      await spikeConnection.requestHealthPermissions({
        activityConfigs: [activityConfig],
        includeEnhancedPermissions: true, // Ask for additional permissions on Android to better detect manual entries
      });
      setPermissionsRequested(true);

    } catch (error) {
      onError(`${error}`);
    }
  };

  return (<View>
    {!permissionsRequested
    ? <Button
      title="Get permissions"
      onPress={onGetHealthPermissions}
    />
    : <View>
        <GetDataView
          spikeConnection={spikeConnection}
          activityConfig={activityConfig}
          onError={onError}
          />
      </View>
    }
    </View>);
}

// --------------------------------- DATA ---------------------------------

type GetDataViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  activityConfig: ActivityConfig;
  onError: (error: string | undefined) => void;
};

function GetDataView({spikeConnection, activityConfig, onError}: GetDataViewProps): React.JSX.Element {

  const [objects, setObjects] = useState<SpikeRecord[]>();

  const onGetData = async () => {
    try {
      setObjects(undefined); // Reset view

      // Get workouts for yesterday and today
      const now = new Date();
      const start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
      const end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

      // Change setting according to your needs:
      const result = await spikeConnection.getActivities({
        config: activityConfig, // Using config from previous step because we got permissions for all it's data types
        from: start,
        to: end,
      });
      
      setObjects(result);

    } catch (error) {
      onError(`${error}`);
    }
  };

  return (<View>
    <Button
      title="Get activities"
      onPress={onGetData}
    />

    {!objects
      ? null
      : <Text>{JSON.stringify(objects, null, 2)}</Text>
    }

    </View>);
}


export default ActivitiesScreen;
