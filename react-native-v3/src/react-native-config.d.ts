declare module 'react-native-config' {
    export interface NativeConfig {
        APP_ID: string;
        APP_SECRET: string;
        APP_USER: string;
    }

    export const Config: NativeConfig;
    export default Config;
  }
