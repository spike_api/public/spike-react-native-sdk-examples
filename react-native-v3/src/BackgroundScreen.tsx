/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import Config from 'react-native-config';
import Spike, { ActivityConfig, BackgroundDeliveryConfig, MetricType, SpikeRecord, SpikeRNConnectionAPIv3, StatisticsType } from 'react-native-spike-sdk';

// --------------------------------- CONNECT ---------------------------------
function BackgrounScreen(): React.JSX.Element {

  const [connectionV3, setConnectionV3] = useState<SpikeRNConnectionAPIv3>();
  const [errorMessage, setError] = useState<string | undefined>();

  const onConnect = async () => {

    console.log(`Start connection for ${Config.APP_USER}`);

    // Create SpikeSDK Connection
    try {
      const connection = await Spike.createConnectionAPIv3({
        applicationId: Number(Config.APP_ID),
        signature: Config.APP_SECRET,
        endUserId: Config.APP_USER,
      });

      setConnectionV3(connection);
    } catch (error) {
      console.log(`${error}`);
    }
  };

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          { errorMessage ? <Text style={{color: '#ff0000'}}>{errorMessage}</Text> : null }

          { connectionV3
          ? <PermissionsView
              spikeConnection={connectionV3}
              onError={(error) => {
                setError(error);
                console.log(error);
              }}
            />
          : <Button
              title="Connect to Spike"
              onPress={onConnect}
            />
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

// --------------------------------- PERMISSIONS ---------------------------------

type PermissionsViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  onError: (error: string | undefined) => void;
};

function PermissionsView({spikeConnection, onError}: PermissionsViewProps): React.JSX.Element {
  const [permissionsRequested, setPermissionsRequested] = useState(false);

  // Change to type(s) you want
  const statisticsTypes = [StatisticsType.steps];
  const metricTypes = [MetricType.distanceTotal];
  const activityConfig = new ActivityConfig({
    includeMetricTypes: metricTypes,
  });

  const onGetHealthPermissions = async () => {

    // Get user permissions for reading data from HealthKit on iOS or Health Connect on Android.
    // If you need more granular control over what happens on Android, please look into SpikeSDK
    // documentation to find out what other functionality is available.
    // 
    // It is important to add `includeBackgroundDelivery` to enable background delivery on Android.
    try {
      await spikeConnection.requestHealthPermissions({
        statisticTypes: statisticsTypes,
        activityConfigs: [activityConfig],
        includeBackgroundDelivery: true // This asks users on Android to give us permission to read data in the background
      });
      setPermissionsRequested(true);

    } catch (error) {
      onError(`${error}`);
    }
  };

  return (<View>
    {!permissionsRequested
    ? <Button
      title="Get permissions"
      onPress={onGetHealthPermissions}
    />
    : <View>
        <GetDataView
            spikeConnection={spikeConnection}
            activityConfig={activityConfig}
            onError={onError}
            statisticsTypes={statisticsTypes}
        />
      </View>
    }
    </View>);
}

// --------------------------------- DATA ---------------------------------

type GetDataViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  statisticsTypes: StatisticsType[];
  activityConfig: ActivityConfig;
  onError: (error: string | undefined) => void;
};

function GetDataView({spikeConnection, statisticsTypes, activityConfig, onError}: GetDataViewProps): React.JSX.Element {

  const [bgChecked, setBgChecked] = useState<boolean>(false);
  const [backgroundConfig, setBackgroundConfig] = useState<BackgroundDeliveryConfig | null>(null);

  // After enabling Background delivery you can close the app and use the phone as usual.
  // Make sure you have set up `Webhook URL` in admin console.
  // After some time you can expect to get webhook call meaning new data has come.
  const onEnable = async () => {
    try {
        onError(undefined);
        // Change setting according to your needs:
        await spikeConnection.enableBackgroundDelivery({
          statisticTypes: statisticsTypes,
          activityConfigs: [activityConfig],
        });
        
        // Update UI with newest config
        onReadConfig()

    } catch (error) {
        setBackgroundConfig(null);
        onError(`${error}`);
    }
  };

  const onDisable = async () => {
    try {
        onError(undefined);

        await spikeConnection.disableBackgroundDelivery()
        
        // Update UI with newest config
        onReadConfig()

    } catch (error) {
        onError(`${error}`);
    }
  };

  const onReadConfig = async () => {
    try {
        onError(undefined);

        const config = await spikeConnection.getBackgroundDeliveryConfig()
        setBackgroundConfig(config)
        setBgChecked(true)

    } catch (error) {
        setBackgroundConfig(null);
        onError(`${error}`);
    }
  };

  return (<View>
    <Button
      title="Read background delivery config"
      onPress={onReadConfig}
    />

    <Button
      title="Enable background delivery"
      onPress={onEnable}
    />

    <Button
      title="Disable background delivery"
      onPress={onDisable}
    />

    {!bgChecked
      ? null
      : <Text>Background delivery config: {JSON.stringify(backgroundConfig, null, 2)}</Text>
    }

    </View>);
}


export default BackgrounScreen;
