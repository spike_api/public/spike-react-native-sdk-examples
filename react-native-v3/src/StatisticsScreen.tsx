/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import Config from 'react-native-config';
import Spike, { SpikeRNConnectionAPIv3, StatisticsFilter, StatisticsInterval, StatisticsType } from 'react-native-spike-sdk';
import { Statistic } from 'react-native-spike-sdk/lib/typescript/v3/DataModels/Statistic';

// --------------------------------- CONNECT ---------------------------------
function StatisticsScreen(): React.JSX.Element {

  const [connectionV3, setConnectionV3] = useState<SpikeRNConnectionAPIv3>();
  const [errorMessage, setError] = useState<string | undefined>();

  const onConnect = async () => {

    console.log(`Start connection for ${Config.APP_USER}`);

    // Create SpikeSDK Connection
    try {
      const connection = await Spike.createConnectionAPIv3({
        applicationId: Number(Config.APP_ID),
        signature: Config.APP_SECRET,
        endUserId: Config.APP_USER,
      });

      setConnectionV3(connection);
    } catch (error) {
      console.log(`${error}`);
    }
  };

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          { errorMessage ? <Text style={{color: '#ff0000'}}>{errorMessage}</Text> : null }

          { connectionV3
          ? <PermissionsView
              spikeConnection={connectionV3}
              onError={(error) => {
                setError(error);
                console.log(error);
              }}
            />
          : <Button
              title="Connect"
              onPress={onConnect}
            />
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

// --------------------------------- PERMISSIONS ---------------------------------

type PermissionsViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  onError: (error: string | undefined) => void;
};

function PermissionsView({spikeConnection, onError}: PermissionsViewProps): React.JSX.Element {
  const [permissionsRequested, setPermissionsRequested] = useState(false);

  // Change to type(s) you want
  const statisticTypes = [StatisticsType.steps];

  const onGetHealthPermissions = async () => {

    // Get user permissions for reading data from HealthKit on iOS or Health Connect on Android.
    // If you need more granular control over what happens on Android, please look into SpikeSDK
    // documentation to find out what other functionality is available.
    try {
      await spikeConnection.requestHealthPermissions({
        statisticTypes: statisticTypes,
        includeEnhancedPermissions: true, // Ask for additional permissions on Android to better detect manual entries
      });
      setPermissionsRequested(true);

    } catch (error) {
      onError(`${error}`);
    }
  };

  return (<View>
    {!permissionsRequested
    ? <Button
      title="Get permissions"
      onPress={onGetHealthPermissions}
    />
    : <View>
        <GetDataView
          spikeConnection={spikeConnection}
          statisticTypes={statisticTypes}
          onError={onError}
          />
      </View>
    }
    </View>);
}

// --------------------------------- DATA ---------------------------------

type GetDataViewProps = {
  spikeConnection: SpikeRNConnectionAPIv3;
  statisticTypes: StatisticsType[];
  onError: (error: string | undefined) => void;
};

function GetDataView({spikeConnection, statisticTypes, onError}: GetDataViewProps): React.JSX.Element {

  const [statistics, setStatistics] = useState<Statistic[]>();

  const onGetStatistics = async () => {
    try {
      setStatistics(undefined); // Reset view

      // Get statistics for yesterday and today
      const now = new Date();
      const start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
      const end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

      // Change setting according to your needs:
      const stats = await spikeConnection.getStatistics({
        ofTypes: statisticTypes,
        from: start,                                          // Period start
        to: end,                                              // Period end (excluding that exact moment)
        interval: StatisticsInterval.hour,                    // Hourly or daily statistics
        filter: new StatisticsFilter({excludeManual: false}),  // Set to true to exclude manual entries
      });
      setStatistics(stats);

    } catch (error) {
      onError(`${error}`);
    }
  };

  return (<View>
    <Button
      title="Get statistics"
      onPress={onGetStatistics}
    />

    {!statistics
      ? null
      : <Text>{JSON.stringify(statistics, null, 2)}</Text>
    }

    </View>);
}


export default StatisticsScreen;
