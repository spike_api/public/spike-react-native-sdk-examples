//
//  SpikeSDK_swift_demoApp.swift
//  SpikeSDK swift demo
//
//  Created by Jaroslav Oo on 28/02/2025.
//

import SwiftUI
import SpikeSDK

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Spike.configure()
        return true
    }
}

@main
struct SpikeSDK_swift_demoApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ConnectView()
        }
    }
}
