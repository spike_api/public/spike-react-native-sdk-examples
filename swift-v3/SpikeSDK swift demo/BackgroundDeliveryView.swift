//
//  BackgroundDeliveryView.swift
//  SpikeSDK swift demo
//
//  Created by Jaroslav Oo on 03/03/2025.
//
import SwiftUI
import SpikeSDK

struct BackgroundDeliveryView: View {
    
    var spikeConnection: SpikeConnectionAPIv3
    var statisticsTypes: [StatisticsType]
    
    @State var errorText: String?
    @State var bgConfig: BackgroundDeliveryConfig?
    @State var bgConfigRead = false
    @State var isLoading = false
    
    private func readConfig() async {
        do {
            errorText = nil
            bgConfig = try await spikeConnection.getBackgroundDeliveryConfig()
            bgConfigRead = true
            
        } catch {
            errorText = "\(error)"
        }
    }
    
    private func enableBgDelivery() async {
        do {
            errorText = nil
            
            // You can request not only statistics, but other data types too.
            // Keep in mind that each call to `enableBackgroundDelivery` overwrites
            // old config, so you have to call it with all the types you want at
            // once.
            try await spikeConnection.enableBackgroundDelivery(
                forStatistics: statisticsTypes,
                forMetrics: nil,
                forActivities: nil,
                forSleep: nil
            )
            
            await readConfig()
        } catch {
            errorText = "\(error)"
        }
    }
    
    private func disableBgDelivery() async {
        do {
            errorText = nil
            try await spikeConnection.disableBackgroundDelivery()
            await readConfig()
            
        } catch {
            errorText = "\(error)"
        }
    }
    
    
    var body: some View {
        VStack(spacing: 8) {
            
            if isLoading {
                ProgressView()
            }
            
            if let errorText {
                Text(errorText)
                    .foregroundColor(.red)
                    .padding()
            }
            
            Button("Read background delivery config") {
                Task {
                    await readConfig()
                }
            }
            
            if bgConfigRead {
                
                
                Button("Enable background delivery") {
                    Task {
                        await enableBgDelivery()
                    }
                }
                Button("Disable background delivery") {
                    Task {
                        await disableBgDelivery()
                    }
                }
                
                if let config = bgConfig {
                    Text("\(config)")
                    
                } else {
                    Text("Background delivery disabled")
                }
            }
            
        }
    }
}
