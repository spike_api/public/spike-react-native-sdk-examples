//
//  ContentView.swift
//  SpikeSDK swift demo
//
//  Created by Jaroslav Oo on 28/02/2025.
//

import SwiftUI
import SpikeSDK

// MARK: - Connection

/// Your Spike application ID. Can be found in your admin console.
private let appId = 0

/// Your  user identifier that you will use when communicating with Spike.
/// Can be any alphanumerical string with dashes. Please do not use any PII in it!
private let userId = "Spike-demo-app-user-123"

/// Your HMAC key – IMPORTANT: Never expose this in a live application!
/// This demo application performs signing on the device solely for demonstration purposes.
/// In a production environment, this functionality should be implemented on your backend.
/// The backend should generate and provide the signed data along with the user ID to the application.
private let secret = ""

struct ConnectView: View {
    
    @State var errorText: String?
    @State var spikeConnection: SpikeConnectionAPIv3?
    
    private func connect() async {
        do {
            let signature = getUserSignature(userId: userId) // Please generate signature in your backend instead!
            spikeConnection = try await Spike.createConnectionAPIv3(
                applicationId: appId,
                signature: signature,
                endUserId: userId
            )
            
        } catch {
            errorText = "\(error)"
        }
    }
    
    /// This signing should happen on the backend and only resulting signature should be passed to the app together with userId.
    private func getUserSignature(userId: String) -> String {
        let generator = SpikeHMACSignatureGenerator(signingKey: secret)
        return generator.sign(userId: userId)
    }
    
    var body: some View {
        VStack {
            
            if let errorText {
                Text(errorText)
                    .foregroundColor(.red)
                    .padding()
            }
            
            if spikeConnection == nil {
                
                Button("Connect") {
                    Task {
                        await connect()
                    }
                }
                
            } else if let spikeConnection {
                PermissionsView(spikeConnection: spikeConnection)
            }
        }
    }
}

// MARK: - Permissions

/// Specify the statistics to be accessed. These will be used for both data retrieval and permission requests.
private var statisticTypes: [StatisticsType] = [
    .steps,
    .distanceTotal,
    .caloriesBurnedTotal,
]

struct PermissionsView: View {
    
    @State var errorText: String?
    var spikeConnection: SpikeConnectionAPIv3
    @State var permissionsChecked: Bool = false
    
    private func checkPermissions() async {
        do {
            // Requesting for permissions should be done every time before
            // asking for any information from SpikeSDK, even if it was done
            // earlier.
            // iOS will not re-ask users for the same permission twice.
            try await spikeConnection.requestPermissionsFromHealthKit(
                forStatistics: statisticTypes
            )
            permissionsChecked = true
        } catch {
            errorText = "\(error)"
        }
    }
    
    var body: some View {
        VStack {
            
            if let errorText {
                Text(errorText)
                    .foregroundColor(.red)
                    .padding()
            }
            
            Text("Statistic types:")
                .font(.headline)
            
            ForEach(statisticTypes, id: \.self) { type in
                Text("\(type)")
            }
            
            if !permissionsChecked {
                Button("Request permissions") {
                    Task {
                        await checkPermissions()
                    }
                }
                Spacer()
            } else {
                DataView(spikeConnection: spikeConnection)
            }
            
        }
    }
}


// MARK: - Data

struct DataView: View {
    
    @State var errorText: String?
    @State var statistics: [Statistic]?
    @State var showBgDelivery: Bool = false
    
    var spikeConnection: SpikeConnectionAPIv3
    
    private func readData() async {
        do {
            // Get statistics for the last 7 days grouped by day:
            statistics = try await spikeConnection.getStatistics(
                ofTypes: statisticTypes,
                from: Date().addingTimeInterval(-7 * 24 * 60 * 60).startOfTheDay,
                to: Date().endOfTheDay,
                interval: .day,
                filter: nil
            )
            
        } catch {
            errorText = "\(error)"
        }
    }
    
    var body: some View {
        VStack {
            
            if let errorText {
                Text(errorText)
                    .foregroundColor(.red)
                    .padding()
            }

            Button("Read data") {
                Task {
                    await readData()
                }
            }
            
            if statistics != nil {
                Text("Results:")
                    .font(.headline)
                
                List {
                    ForEach(groupedStatistics.keys.sorted(by: { $0.rawValue < $1.rawValue }), id: \.self) { type in
                        Section(header: Text(type.rawValue).font(.headline)) {
                            ForEach((groupedStatistics[type] ?? []).sorted{ $0.start < $1.start }, id: \.self) { stat in
                                HStack {
                                    Text("\(dateFormatter.string(from: stat.start))")
                                    Spacer()
                                    Text("\(numberFormatter.string(from: NSNumber(floatLiteral: stat.value))!) \(stat.unit)")
                                }
                            }
                        }
                    }
                }
                
                Button("Background delivery") {
                    showBgDelivery = true
                }.padding()
                
            } else {
                Spacer()
                Button("Background delivery") {
                    showBgDelivery = true
                }.padding()
            }
            
        }
        .sheet(
            isPresented: $showBgDelivery,
            content: {
                BackgroundDeliveryView(
                    spikeConnection: spikeConnection,
                    statisticsTypes: statisticTypes
                )
            }
        )
    }
    
    private var groupedStatistics: [StatisticsType: [Statistic]] {
        Dictionary(grouping: statistics ?? [], by: { $0.type })
    }
    
    private var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        return formatter
    }
    
    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        return formatter
    }
    
}


#Preview {
    ConnectView()
}
