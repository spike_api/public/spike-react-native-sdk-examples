//
//  Date+Extensions.swift
//  SpikeSDK swift demo
//
//  Created by Jaroslav Oo on 28/02/2025.
//
import Foundation

extension Date {
    
    fileprivate var calendar: Calendar {
        Calendar.current
    }
    
    var startOfTheWeek: Date {
        return calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    var endOfTheWeek: Date {
//        calendar.firstWeekday = 2 // 1 = Sunday, 2 = Monday (forces week to start on Monday)
        return calendar.date(byAdding: .day, value: 7, to: startOfTheWeek)!
    }
    
    var startOfTheDay: Date {
        return calendar.date(from: calendar.dateComponents([.year, .month, .day], from: self))!
    }
    
    var endOfTheDay: Date {
        return calendar.date(byAdding: .day, value: 1, to: startOfTheDay)!
    }
    
}
