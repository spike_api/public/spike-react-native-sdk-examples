# Swift SpikeSDK example app

This project provides a straightforward example of using the SpikeSDK in a Swift application. It demonstrates how to:

1. Establish a connection to SpikeSDK.
1. Integrate Apple HealthKit (iOS).
1. Retrieve health data based on your requirements.


## Intended Audience

This app is designed for developers and serves as a foundational starting point for integrating SpikeSDK. The app features minimal UI and focuses on showcasing core functionality through code.

## Usage

To customize the app for your needs, examine the codebase and modify the settings to fetch different data types or adjust configurations.

Before running the app, you need to copy your Application ID and HMAC Key. Please make sure to not release the app with HMAC code compiled!

You’re now ready to dive into the code! All the essential logic and functionality can be found in following files:

- __Statistics__ like steps, distance etc.:  Statistics.swift

