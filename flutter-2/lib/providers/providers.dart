import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk_test_app/providers/connection/connection_credentials_provider.dart';
import 'package:spike_flutter_sdk_test_app/providers/connection/connection_provider.dart';

abstract class Providers {
  static final value = [
    ChangeNotifierProvider<ConnectionCredentialsProvider>(
      create: (_) => ConnectionCredentialsProvider(),
    ),
    ChangeNotifierProvider<ConnectionProvider>(
      create: (_) => ConnectionProvider(),
    ),
  ];
}
