import 'dart:convert';

import 'package:encrypt/encrypt.dart' as enc;
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:spike_flutter_sdk_test_app/bridge/model/operation_result.dart';
import 'package:spike_flutter_sdk_test_app/domain/connection/connection_credentials.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

class ConnectionCredentialsProvider with ChangeNotifier {
  Future<OperationResult<ConnectionCredentials>> loadCredentials(
      final String key) async {
    try {
      final asset = await rootBundle.loadString('assets/credentials.enc');

      final decryptionKey = enc.Key.fromUtf8('${key}______________________');
      final iv = enc.IV.fromUtf8('${key}______');

      final encrypter = enc.Encrypter(enc.AES(
        decryptionKey,
        mode: enc.AESMode.cbc,
      ));

      final decrypted = encrypter.decrypt(
        enc.Encrypted.fromBase64(asset),
        iv: iv,
      );
      final data = jsonDecode(decrypted);

      return OperationResult.success(ConnectionCredentials(
        appId: data['appId'],
        authToken: data['authToken'],
      ));
    } catch (_) {
      return OperationResult.fail(Texts.credentialsInvalid);
    }
  }
}
