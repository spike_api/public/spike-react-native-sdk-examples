import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_flutter_sdk_test_app/bridge/model/operation_result.dart';
import 'package:spike_flutter_sdk_test_app/domain/connection/connection_credentials.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';
import 'package:spike_flutter_sdk_test_app/repositories/webhook_url_repository.dart';

class ConnectionProvider with ChangeNotifier {
  final _webhook = WebhookUrlRepository();
  SpikeConnection? _currentConnection;

  String get connectionInfo {
    const encoder = JsonEncoder.withIndent('  ');
    return encoder.convert({
      'connectionId': _currentConnection?.connectionId,
      'appId': _currentConnection?.appId,
      'customerEndUserId': _currentConnection?.customerEndUserId,
      'callbackUrl': _currentConnection?.callbackUrl,
    });
  }

  Future<OperationResult<SpikeConnection>> connect(
    final ConnectionCredentials credentials,
  ) async {
    if (_currentConnection != null) {
      return OperationResult.success(_currentConnection!);
    }

    try {
      final webhookUrlResult = await _webhook.generateWebHookUrl();
      if (!webhookUrlResult.success || webhookUrlResult.value == null) {
        return OperationResult.fail(
          webhookUrlResult.errorMessage ?? Texts.webhookFail,
        );
      }
      final webhookUrl = webhookUrlResult.value!;

      final connection = await SpikeSDK.createConnection(
        appId: credentials.appId,
        authToken: credentials.authToken,
        customerEndUserId: _getRandomString(32),
        postbackURL: webhookUrl,
      );

      _currentConnection = connection;

      return OperationResult.success(connection);
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  Future<OperationResult<bool>> disconnect() async {
    try {
      await _currentConnection?.close();
      _currentConnection = null;

      return OperationResult.success(true);
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  Future<OperationResult<bool>> revokeAllPermissions() async {
    try {
      await _currentConnection?.revokeAllPermissions();

      return OperationResult.success(true);
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  Future<OperationResult<bool>> requestPermissions() async {
    try {
      final data =
          await _currentConnection?.ensurePermissionsAreGrantedV2(types: [
        SpikeDataType.activitiesSummary,
      ]);

      switch (data) {
        case SpikePermissionsResult.granted:
          return OperationResult.success(true);
        case SpikePermissionsResult.grantedPartially:
          return OperationResult.fail(
            Texts.permissionsRequestFailureGrantedPartially,
          );
        case SpikePermissionsResult.canceled:
          return OperationResult.fail(Texts.permissionsRequestFailureCancelled);
        case SpikePermissionsResult.blocked:
          return OperationResult.fail(Texts.permissionsRequestFailureBlocked);
        case SpikePermissionsResult.undefined:
        default:
          return OperationResult.success(true);
      }
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  Future<OperationResult<String>> extractData() async {
    try {
      final data = await _currentConnection?.extractData(
        SpikeDataType.activitiesSummary,
        from: _midnight(DateTime.now().subtract(const Duration(days: 3))),
        to: DateTime.now(),
      );
      const encoder = JsonEncoder.withIndent('  ');
      return OperationResult.success(encoder.convert(data?.toObject()));
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  Future<OperationResult<String>> extractAndSendData() async {
    try {
      final data = await _currentConnection
          ?.extractAndPostData(SpikeDataType.activitiesSummary);
      const encoder = JsonEncoder.withIndent('  ');
      return OperationResult.success(encoder.convert(data?.toObject()));
    } on SpikeException catch (ex) {
      return OperationResult.fail(ex.message ?? Texts.spikeException);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }

  String _getRandomString(int len) {
    var random = Random.secure();
    var values = List<int>.generate(len, (i) => random.nextInt(255));
    var string = base64UrlEncode(values);
    return string.substring(0, string.length - 1);
  }

  DateTime _midnight(final DateTime date) => DateTime(
        date.year,
        date.month,
        date.day,
        0,
        0,
        0,
        0,
        0,
      );
}
