import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/connection/connection_screen.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/home/home_screen.dart';

abstract class AppRoutes {
  static Map<String, WidgetBuilder> values = {
    HomeScreen.routePath: (ctx) => const HomeScreen(),
    ConnectionScreen.routePath: (ctx) => const ConnectionScreen(),
  };
}
