import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk_test_app/presentation/app_routes.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';
import 'package:spike_flutter_sdk_test_app/providers/providers.dart';

class SDKApp extends StatelessWidget {
  const SDKApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: Providers.value,
      child: MaterialApp(
        title: Texts.appTitle,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.lightBlueAccent),
          useMaterial3: true,
        ),
        routes: AppRoutes.values,
      ),
    );
  }
}
