abstract class FontSize {
  static const base = 14.0;
  static const lg = 18.0;
  static const xl = 20.0;
}
