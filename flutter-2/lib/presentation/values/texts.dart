abstract class Texts {
  static const appTitle = 'Flutter SDK Test App';
  static const homeScreenTitle = 'Home';
  static const homeScreenDescription =
      'In this place, you will initialize connection. Just click the button, enter the code given to you, and you will be connected to the Spike.';
  static const actionConnect = 'Connect';
  static const connectionInProgress = 'Connecting...';
  static const ok = 'Ok';
  static const save = 'Save';
  static const loadCredentials = 'Load credentials';
  static const decryptionKey = 'Decryption key';
  static const loadCredentialsExplanation =
      'Enter the decryption key provided to you by Spike in order to connect.';
  static const error = 'Error';
  static const credentialsReceivedTitle = 'Credentials received!';
  static const credentialsReceivedText =
      'Credentials have been received successfully. Now the real connection to the miraculous Spike will continue. Just sit back and relax while we are connecting you to the new level of awesomeness.';
  static const credentialsInvalid = 'Decryption key is invalid!';
  static const webhookFail =
      'Webhook URL generation failure. Check your network connection.';
  static const spikeException = 'Spike exception (Native part)';
  static const spikeConnectedTitle = 'Spike connection created!!!';
  static const spikeConnectedText =
      'Spike connection has been successfully created. Now let\'s enter the grand miracle of what the Spike is!';
  static const connectionScreenTitle = 'Connection';
  static const connectionScreenDescription =
      'With connection created, you can perform various actions. Just click any button you like, and see how the connection is used in practice.';
  static const getConnectionInfo = 'Get Connection Info';
  static const closeConnection = 'Close Connection';
  static const requestPermissions = 'Request Permissions';
  static const revokeAllPermissions = 'Revoke All Permissions';
  static const extractData = 'Extract Data';
  static const sendDataToWebhook = 'Send Data to Webhook';
  static const output = 'Output';
  static const disconnectSuccessTitle = 'Disconnected';
  static const disconnectSuccessText = 'Disconnect was successful.';
  static const dataExtractedTitle = 'Data extracted';
  static const dataExtractedText =
      'Data extracted successfully. Check for output.';
  static const dataExtractedAndSentTitle = 'Data extracted and sent';
  static const dataExtractedAndSentText =
      'Data extracted and sent successfully. Check for output.';
  static const permissionsReceivedTitle = 'Permissions received';
  static const permissionsReceivedText = 'Permissions received successfully.';
  static const permissionsRevokedTitle = 'Permissions revoked';
  static const permissionsRevokedText = 'Permissions revoked successfully.';
  static const permissionsRequestFailure = 'Failed to request for permissions';
  static const permissionsRequestFailureCancelled =
      'Failed. User cancelled the request.';
  static const permissionsRequestFailureGrantedPartially =
      'Failed. User gave only part of permissions.';
  static const permissionsRequestFailureBlocked =
      'Failed. Health Connect blocked your app. You may want calling to revoke all permissions.';
}
