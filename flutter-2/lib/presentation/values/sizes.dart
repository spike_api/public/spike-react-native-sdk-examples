abstract class Sizes {
  static const x0_5 = 2.0;
  static const x1 = 4.0;
  static const x2 = 8.0;
  static const x3 = 12.0;
  static const x4 = 16.0;
  static const x5 = 20.0;
  static const x8 = 32.0;
  static const x12 = 48.0;
  static const x16 = 64.0;
}
