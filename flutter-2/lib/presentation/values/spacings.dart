import 'package:flutter/cupertino.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';

abstract class Spacings {
  static const h1 = SizedBox(width: Sizes.x1);
  static const v1 = SizedBox(height: Sizes.x1);

  static const h2 = SizedBox(width: Sizes.x2);
  static const v2 = SizedBox(height: Sizes.x2);

  static const h3 = SizedBox(width: Sizes.x3);
  static const v3 = SizedBox(height: Sizes.x3);

  static const h4 = SizedBox(width: Sizes.x4);
  static const v4 = SizedBox(height: Sizes.x4);
}
