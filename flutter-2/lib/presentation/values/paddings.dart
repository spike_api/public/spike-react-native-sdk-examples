import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';

abstract class Paddings {
  static const x1 = EdgeInsets.all(Sizes.x1);
  static const x2 = EdgeInsets.all(Sizes.x2);
  static const x3 = EdgeInsets.all(Sizes.x3);
  static const x4 = EdgeInsets.all(Sizes.x4);
}
