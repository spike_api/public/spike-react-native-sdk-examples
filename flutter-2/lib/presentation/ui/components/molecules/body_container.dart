import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/info_message_box.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/paddings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';

class BodyContainer extends StatelessWidget {
  final List<Widget> children;
  final String? description;

  const BodyContainer({
    required this.children,
    this.description,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: Paddings.x3,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (description != null) InfoMessageBox(description!),
            if (description != null) Spacings.v2,
            ...children,
          ],
        ),
      ),
    );
  }
}
