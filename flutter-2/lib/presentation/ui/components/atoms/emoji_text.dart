import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/font_size.dart';

class EmojiText extends StatelessWidget {
  final String symbol;
  const EmojiText(
    this.symbol, {
    super.key,
  });

  @override
  Widget build(BuildContext context) => Text(
        symbol,
        style: const TextStyle(
          fontSize: FontSize.xl,
        ),
      );
}
