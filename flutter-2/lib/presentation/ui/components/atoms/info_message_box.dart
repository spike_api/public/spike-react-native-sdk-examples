import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/font_size.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/paddings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';

class InfoMessageBox extends StatelessWidget {
  final String message;
  const InfoMessageBox(
    this.message, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: Paddings.x1,
      decoration: BoxDecoration(
        color: Colors.lightBlue,
        borderRadius: BorderRadius.circular(Sizes.x2),
      ),
      child: Row(
        children: [
          const Icon(
            Icons.info_outline,
            color: Colors.white,
            size: Sizes.x8,
          ),
          Spacings.h1,
          Expanded(
            child: Text(
              message,
              style: const TextStyle(
                fontSize: FontSize.base,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
