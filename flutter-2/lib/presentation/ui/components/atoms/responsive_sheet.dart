import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';

class ResponsiveSheet extends StatelessWidget {
  final Widget child;

  const ResponsiveSheet({
    required this.child,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height * 0.75,
        ),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(Sizes.x2),
              topRight: Radius.circular(Sizes.x2),
            ),
          ),
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: child,
          ),
        ),
      ),
    );
  }
}
