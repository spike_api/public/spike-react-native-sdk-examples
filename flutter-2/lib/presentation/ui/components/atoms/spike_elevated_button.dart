import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';

class SpikeElevatedButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String label;
  final bool isLoading;
  final Widget? prefix;

  const SpikeElevatedButton({
    required this.onPressed,
    required this.label,
    this.isLoading = false,
    this.prefix,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isLoading ? null : onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (prefix != null) prefix!,
          if (prefix != null) Spacings.h1,
          Text(label),
          if (isLoading) Spacings.h2,
          if (isLoading)
            const SizedBox(
              width: Sizes.x5,
              height: Sizes.x5,
              child: CircularProgressIndicator(
                strokeWidth: Sizes.x0_5,
              ),
            ),
        ],
      ),
    );
  }
}
