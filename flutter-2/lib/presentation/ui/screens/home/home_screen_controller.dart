import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk_test_app/bridge/controllers/spike_base_controller.dart';
import 'package:spike_flutter_sdk_test_app/domain/connection/connection_credentials.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/forms/load_credentials/load_credentials_form.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/forms/load_credentials/load_credentials_form_callback.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/connection/connection_screen.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/home/home_screen.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/utils/utils_ui.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';
import 'package:spike_flutter_sdk_test_app/providers/connection/connection_provider.dart';

class HomeScreenController extends SpikeBaseController<HomeScreen> {
  late ConnectionProvider _connection;

  @override
  void initialize(
    final State state,
    final HomeScreen widget,
  ) {
    super.initialize(state, widget);

    _connection = Provider.of<ConnectionProvider>(
      context,
      listen: false,
    );
  }

  void connect() {
    final callback = LoadCredentialsFormCallback(
      onDataReceived: _processReceivedData,
    );

    UtilsUi.showSheet(
      context: context,
      builder: (ctx) => LoadCredentialsForm(
        callback: callback,
      ),
    );
  }

  Future<void> _processReceivedData(
    final ConnectionCredentials credentials,
  ) async {
    enableLoading();

    final result = await _connection.connect(credentials);

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value == null) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    await UtilsUi.showSuccess(
      context,
      title: Texts.spikeConnectedTitle,
      message: Texts.spikeConnectedText,
    );

    if (!state.mounted) {
      return;
    }

    Navigator.of(context).pushNamed(ConnectionScreen.routePath);
  }
}
