import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/spike_elevated_button.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/molecules/body_container.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/home/home_screen_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

class HomeScreen extends StatefulWidget {
  static const routePath = '/';

  const HomeScreen({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeScreenController _controller;

  @override
  void initState() {
    super.initState();

    _controller = HomeScreenController()..initialize(this, widget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text(Texts.homeScreenTitle),
      ),
      body: BodyContainer(
        description: Texts.homeScreenDescription,
        children: [
          SpikeElevatedButton(
            onPressed: _controller.connect,
            label: _controller.isLoading
                ? Texts.connectionInProgress
                : Texts.actionConnect,
            isLoading: _controller.isLoading,
          ),
        ],
      ),
    );
  }
}
