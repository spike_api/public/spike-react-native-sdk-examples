import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/emoji_text.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/spike_elevated_button.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/molecules/body_container.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/connection/connection_screen_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/font_size.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/paddings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

class ConnectionScreen extends StatefulWidget {
  static const routePath = '/connection';

  const ConnectionScreen({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ConnectionScreenState();
}

class _ConnectionScreenState extends State<ConnectionScreen> {
  late ConnectionScreenController _controller;

  @override
  void initState() {
    super.initState();

    _controller = ConnectionScreenController()..initialize(this, widget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: _controller.isLoading
            ? const Padding(
                padding: Paddings.x2,
                child: CircularProgressIndicator(),
              )
            : null,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text(Texts.connectionScreenTitle),
      ),
      body: BodyContainer(
        description: Texts.connectionScreenDescription,
        children: [
          SpikeElevatedButton(
            onPressed:
                _controller.isLoading ? null : _controller.getConnectionInfo,
            prefix: const EmojiText('🤖'),
            label: Texts.getConnectionInfo,
          ),
          Spacings.v1,
          SpikeElevatedButton(
            onPressed:
                _controller.isLoading ? null : _controller.closeConnection,
            prefix: const EmojiText('☠'),
            label: Texts.closeConnection,
          ),
          Spacings.v1,
          SpikeElevatedButton(
            onPressed:
                _controller.isLoading ? null : _controller.requestPermissions,
            prefix: const EmojiText('🤳'),
            label: Texts.requestPermissions,
          ),
          Spacings.v1,
          SpikeElevatedButton(
            onPressed:
                _controller.isLoading ? null : _controller.revokeAllPermissions,
            prefix: const EmojiText('☠'),
            label: Texts.revokeAllPermissions,
          ),
          Spacings.v1,
          SpikeElevatedButton(
            onPressed: _controller.isLoading ? null : _controller.extractData,
            prefix: const EmojiText('👾'),
            label: Texts.extractData,
          ),
          Spacings.v1,
          SpikeElevatedButton(
            onPressed:
                _controller.isLoading ? null : _controller.sendDataToWebhook,
            prefix: const EmojiText('😻'),
            label: Texts.sendDataToWebhook,
          ),
          Spacings.v4,
          const Divider(),
          Spacings.v1,
          const Text(
            Texts.output,
            style: TextStyle(
              fontSize: FontSize.lg,
              fontWeight: FontWeight.w500,
            ),
          ),
          Spacings.v1,
          TextFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 3,
                  color: Colors.blueAccent,
                ),
                borderRadius: BorderRadius.circular(Sizes.x8),
              ),
            ),
            readOnly: true,
            controller: _controller.outputController,
            minLines: 10,
            maxLines: 20,
          ),
        ],
      ),
    );
  }
}
