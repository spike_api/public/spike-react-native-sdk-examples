import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk_test_app/bridge/controllers/spike_base_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/screens/connection/connection_screen.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/utils/utils_ui.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';
import 'package:spike_flutter_sdk_test_app/providers/connection/connection_provider.dart';

class ConnectionScreenController extends SpikeBaseController<ConnectionScreen> {
  final outputController = TextEditingController();
  late ConnectionProvider _connection;

  @override
  void initialize(
    final State state,
    final ConnectionScreen widget,
  ) {
    super.initialize(state, widget);

    _connection = Provider.of<ConnectionProvider>(
      context,
      listen: false,
    );
  }

  void getConnectionInfo() => updateState(() {
        outputController.text = _connection.connectionInfo;
      });

  Future<void> closeConnection() async {
    enableLoading();

    final result = await _connection.disconnect();

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value != true) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    await UtilsUi.showSuccess(
      context,
      title: Texts.disconnectSuccessTitle,
      message: Texts.disconnectSuccessText,
    );

    if (!state.mounted) {
      return;
    }

    Navigator.of(context).pop();
  }

  Future<void> requestPermissions() async {
    enableLoading();

    final result = await _connection.requestPermissions();

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value != true) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    await UtilsUi.showSuccess(
      context,
      title: Texts.permissionsReceivedTitle,
      message: Texts.permissionsReceivedText,
    );
  }

  Future<void> revokeAllPermissions() async {
    enableLoading();

    final result = await _connection.revokeAllPermissions();

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value != true) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    await UtilsUi.showSuccess(
      context,
      title: Texts.permissionsRevokedTitle,
      message: Texts.permissionsRevokedText,
    );
  }

  Future<void> extractData() async {
    enableLoading();

    final result = await _connection.extractData();

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value == null) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    updateState(() {
      outputController.text = result.value!;
    });

    await UtilsUi.showSuccess(
      context,
      title: Texts.dataExtractedTitle,
      message: Texts.dataExtractedText,
    );
  }

  Future<void> sendDataToWebhook() async {
    enableLoading();

    final result = await _connection.extractAndSendData();

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (!result.success || result.value == null) {
      UtilsUi.showOperationError(context, result);
      return;
    }

    updateState(() {
      outputController.text = result.value!;
    });

    await UtilsUi.showSuccess(
      context,
      title: Texts.dataExtractedAndSentTitle,
      message: Texts.dataExtractedAndSentText,
    );
  }
}
