import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/bridge/model/operation_result.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/emoji_text.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/responsive_sheet.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

abstract class UtilsUi {
  static void showAlertDialog(
    BuildContext context, {
    required String title,
    required String message,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: [
          TextButton(
            child: const Text(Texts.ok),
            onPressed: () => Navigator.of(ctx).pop(),
          ),
        ],
      ),
    );
  }

  static Future<T?> showSheet<T>({
    required BuildContext context,
    required WidgetBuilder builder,
  }) =>
      showModalBottomSheet<T>(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (ctx) => ResponsiveSheet(
          child: builder(ctx),
        ),
      );

  static Future<void> showOperationError(
    BuildContext context,
    final OperationResult result,
  ) async {
    if (result.success || result.errorMessage == null) {
      return;
    }

    await showDialog(
      context: context,
      builder: (BuildContext ctx) => AlertDialog(
        title: const Row(
          children: [
            EmojiText('💩'),
            Spacings.h2,
            Expanded(
              child: Text(Texts.error),
            ),
          ],
        ),
        content: Text(result.errorMessage!),
        actions: [
          TextButton(
            child: const Text(Texts.ok),
            onPressed: () => Navigator.of(ctx).pop(),
          ),
        ],
      ),
    );
  }

  static Future<void> showSuccess(
    BuildContext context, {
    required String title,
    required String message,
  }) async {
    await showDialog(
      context: context,
      builder: (BuildContext ctx) => AlertDialog(
        title: Row(
          children: [
            const EmojiText('🥳'),
            Spacings.h2,
            Expanded(
              child: Text(title),
            ),
          ],
        ),
        content: Text(message),
        actions: [
          TextButton(
            child: const Text(Texts.ok),
            onPressed: () => Navigator.of(ctx).pop(),
          ),
        ],
      ),
    );
  }
}
