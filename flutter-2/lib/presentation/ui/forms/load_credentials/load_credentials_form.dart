import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/bridge/spike_form.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/components/atoms/info_message_box.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/forms/load_credentials/load_credentials_form_callback.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/forms/load_credentials/load_credentials_form_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/spacings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

class LoadCredentialsForm extends StatefulWidget {
  final LoadCredentialsFormCallback callback;

  const LoadCredentialsForm({
    required this.callback,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LoadCredentialsFormState();
}

class _LoadCredentialsFormState extends State<LoadCredentialsForm> {
  late LoadCredentialsFormController _controller;

  @override
  void initState() {
    super.initState();

    _controller = LoadCredentialsFormController();
  }

  @override
  Widget build(BuildContext context) {
    return SpikeForm<LoadCredentialsForm, LoadCredentialsFormController>(
      parent: widget,
      formTitle: Texts.loadCredentials,
      actionTitle: Texts.loadCredentials,
      controller: _controller,
      builder: (ctrl, ctx) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const InfoMessageBox(Texts.loadCredentialsExplanation),
            Spacings.v4,
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.decryptionKey,
              ),
              controller: ctrl.keyController,
            ),
          ],
        );
      },
    );
  }
}
