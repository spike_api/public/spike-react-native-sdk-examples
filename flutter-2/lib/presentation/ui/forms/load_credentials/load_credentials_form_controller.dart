import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk_test_app/bridge/controllers/spike_form_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/forms/load_credentials/load_credentials_form.dart';
import 'package:spike_flutter_sdk_test_app/presentation/ui/utils/utils_ui.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';
import 'package:spike_flutter_sdk_test_app/providers/connection/connection_credentials_provider.dart';

class LoadCredentialsFormController
    extends SpikeFormController<LoadCredentialsForm> {
  late ConnectionCredentialsProvider _credentials;
  final keyController = TextEditingController();

  @override
  void initialize(
    final State state,
    final LoadCredentialsForm widget,
  ) {
    super.initialize(state, widget);

    _credentials = Provider.of<ConnectionCredentialsProvider>(
      context,
      listen: false,
    );
  }

  @override
  Future<void> save(final FormState? formState) async {
    if (formState?.validate() != true) {
      return;
    }

    enableLoading();

    final result = await _credentials.loadCredentials(keyController.text);

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (result.success && result.value != null) {
      await UtilsUi.showSuccess(
        context,
        title: Texts.credentialsReceivedTitle,
        message: Texts.credentialsReceivedText,
      );

      if (!state.mounted) {
        return;
      }

      Navigator.of(context).pop();
      widget.callback.onDataReceived(result.value!);
    } else {
      UtilsUi.showOperationError(context, result);
    }
  }
}
