import 'package:spike_flutter_sdk_test_app/domain/connection/connection_credentials.dart';

class LoadCredentialsFormCallback {
  final void Function(ConnectionCredentials) onDataReceived;

  const LoadCredentialsFormCallback({
    required this.onDataReceived,
  });
}
