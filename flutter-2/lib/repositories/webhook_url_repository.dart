import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:spike_flutter_sdk_test_app/bridge/model/operation_result.dart';

class WebhookUrlRepository {
  static final WebhookUrlRepository _singleton =
      WebhookUrlRepository._internal();

  factory WebhookUrlRepository() {
    return _singleton;
  }

  WebhookUrlRepository._internal();

  Future<OperationResult<String>> generateWebHookUrl() async {
    try {
      final endpoint = Uri.parse('https://webhook.site/token');
      final response = await http.post(endpoint, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      });

      final uuid = jsonDecode(response.body)['uuid'];
      final url = 'https://webhook.site/$uuid';

      return OperationResult.success(url);
    } catch (ex) {
      return OperationResult.fail(ex.toString());
    }
  }
}
