class OperationResult<TData> {
  final TData? value;
  final String? errorMessage;
  final bool success;

  OperationResult._({
    required this.value,
    required this.errorMessage,
    required this.success,
  }) {
    final hasErrorCorrectly =
        value == null && errorMessage != null && success == false;

    final hasSuccessCorrectly =
        value != null && errorMessage == null && success == true;

    assert(
      hasErrorCorrectly || hasSuccessCorrectly,
      "Must have correct state for error or success.",
    );
  }

  factory OperationResult.success(final TData value) => OperationResult._(
        value: value,
        errorMessage: null,
        success: true,
      );

  factory OperationResult.fail(final String errorMessage) => OperationResult._(
        value: null,
        errorMessage: errorMessage,
        success: false,
      );
}
