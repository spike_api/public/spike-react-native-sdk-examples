import 'package:flutter/material.dart';

abstract class SpikeBaseController<T extends Widget> {
  late BuildContext context;
  late State state;
  late T widget;
  bool _loading = false;
  bool get isLoading => _loading;

  void initialize(final State state, final T widget) {
    this.context = state.context;
    this.state = state;
    this.widget = widget;
  }

  void updateState(final void Function() action) => state.setState(action);

  void enableLoading() => updateState(() {
        _loading = true;
      });

  void disableLoading() => updateState(() {
        _loading = false;
      });
}
