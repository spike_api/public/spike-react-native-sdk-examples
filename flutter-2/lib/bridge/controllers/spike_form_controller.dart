import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/bridge/controllers/spike_base_controller.dart';

abstract class SpikeFormController<T extends Widget>
    extends SpikeBaseController<T> {
  Future<void> save(final FormState? formState);
}
