import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/bridge/controllers/spike_form_controller.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/font_size.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/paddings.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/sizes.dart';
import 'package:spike_flutter_sdk_test_app/presentation/values/texts.dart';

typedef FormWidgetBuilder<TWidget extends Widget,
        TController extends SpikeFormController<TWidget>>
    = Widget Function(TController, BuildContext);

class SpikeForm<TWidget extends Widget,
    TController extends SpikeFormController<TWidget>> extends StatefulWidget {
  final TController controller;
  final FormWidgetBuilder<TWidget, TController> builder;
  final TWidget parent;
  final String? formTitle;
  final Widget Function(BuildContext)? errorBuilder;
  final String? actionTitle;

  const SpikeForm({
    required this.controller,
    required this.builder,
    required this.parent,
    this.formTitle,
    this.errorBuilder,
    this.actionTitle,
    super.key,
  });

  @override
  State<StatefulWidget> createState() =>
      _SpikeNoValidationFormState<TWidget, TController>();
}

class _SpikeNoValidationFormState<TWidget extends Widget,
        TController extends SpikeFormController<TWidget>>
    extends State<SpikeForm<TWidget, TController>> {
  late GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey<FormState>();
    widget.controller.initialize(this, widget.parent);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: _buildForm(),
    );
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      padding: Paddings.x4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (widget.formTitle != null)
            Text(
              widget.formTitle!,
              style: const TextStyle(
                fontSize: FontSize.lg,
                fontWeight: FontWeight.w500,
              ),
            ),
          if (widget.errorBuilder != null) widget.errorBuilder!(context),
          if (widget.formTitle != null) const Divider(),
          widget.builder(widget.controller, context),
          const Divider(),
          SizedBox(
            height: Sizes.x12,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: widget.controller.isLoading
                          ? null
                          : () => widget.controller.save(_formKey.currentState),
                      child: Text(widget.actionTitle ?? Texts.save),
                    ),
                  ),
                ),
                if (widget.controller.isLoading)
                  const Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: Sizes.x2),
                      child: SizedBox(
                        width: Sizes.x4,
                        height: Sizes.x4,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: Sizes.x0_5,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
