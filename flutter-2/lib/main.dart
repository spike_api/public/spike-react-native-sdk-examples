import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk_test_app/presentation/sdk_app.dart';

void main() {
  runApp(const SDKApp());
}
