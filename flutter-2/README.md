# spike_flutter_sdk_test_app

Spike Flutter SDK Test App

## Getting Started

1. Open pubspec.yaml
2. Uncomment '# spike_flutter_sdk: ^1.2.3'.
3. Comment the following lines:
```
  spike_flutter_sdk:
    path: ../spike-flutter-sdk
```
4. Run the project.