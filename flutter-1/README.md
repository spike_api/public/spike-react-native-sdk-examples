# spike_sdk_test_app

Flutter Spike SDK test app.

## Getting Started

Just run this project on your iOS or Android device, and you should be fine. But first, do not forget to run the `flutter pub get`.
