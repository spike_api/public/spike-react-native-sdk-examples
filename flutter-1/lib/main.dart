import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/user_id_generator.dart';
import 'package:spike_sdk_test_app/ui/app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await UserIdGenerator().init();
  runApp(const App());
}
