import 'package:flutter/material.dart';

abstract class SpikeFormController<T extends Widget> {
  late BuildContext context;
  late State state;
  late T widget;
  bool _loading = false;
  bool get isLoading => _loading;

  void setState(final State state) {
    this.state = state;
  }

  Future<void> initialize(final BuildContext context, final T widget) async {
    this.context = context;
    this.widget = widget;
  }

  Future<void> save(final FormState? formState);

  void updateState(final void Function() action) => state.setState(action);

  void enableLoading() => updateState(() {
        _loading = true;
      });

  void disableLoading() => updateState(() {
        _loading = false;
      });
}
