import 'dart:math';

extension StringExtension on String {
  String toMaskedString() {
    final start = substring(0, min(length, 4));
    final end = substring(max(0, length - 4), length);
    return '$start***$end';
  }
}
