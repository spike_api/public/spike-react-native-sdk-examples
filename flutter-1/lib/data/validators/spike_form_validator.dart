abstract class SpikeFormValidator {
  static final _naturalNumberRegex = RegExp(r'^\d+$');

  String? checkIfNotEmpty(
    final String? value, {
    required String errorMessage,
  }) {
    if (value == null || value.trim().isEmpty) {
      return errorMessage;
    }

    return null;
  }

  String? checkIfNatural(
    final String? value, {
    required String requiredErrorMessage,
    required String wrongFormatErrorMessage,
  }) {
    if (value == null || value.trim().isEmpty) {
      return requiredErrorMessage;
    }

    if (!_naturalNumberRegex.hasMatch(value)) {
      return wrongFormatErrorMessage;
    }

    return null;
  }
}
