import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/logging/background_listener.dart';
import 'package:spike_sdk_test_app/data/logging/events_logger.dart';
import 'package:spike_sdk_test_app/data/model/atoms/connection_data_record.dart';
import 'package:spike_sdk_test_app/data/model/atoms/data_mode.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ConnectionsProvider with ChangeNotifier {
  bool _isLoaded = false;
  final List<SpikeConnection> _availableConnections = [];
  List<SpikeConnection> get availableConnections => [..._availableConnections];
  final _retrievedData = <String, List<ConnectionDataRecord>>{};
  String? _errorMessage;
  String? get errorMessage => _errorMessage;

  List<ConnectionDataRecord> getRetrievedData(final String connectionId) =>
      _retrievedData[connectionId] ?? [];

  Future<void> loadConnections() async {
    if (_isLoaded) {
      return;
    }

    final prefs = await SharedPreferences.getInstance();
    final ids = prefs.getStringList('connection_ids') ?? [];
    for (final id in ids) {
      final packed = prefs.getString('connection_$id');
      if (packed == null) {
        continue;
      }

      final unpacked = await _unpackConnection(packed);

      if (unpacked.callbackUrl != null) {
        unpacked.setListener(BackgroundListener());
      }

      _availableConnections.add(unpacked);
    }
    _isLoaded = true;

    notifyListeners();
  }

  Future<void> disconnect({
    required String connectionId,
  }) async {
    final connections = _availableConnections.where(
      (element) => element.connectionId == connectionId,
    );

    if (connections.isNotEmpty) {
      await connections.elementAt(0).close();
    }

    await _eraseConnection(connectionId);
  }

  Future<OperationResultType> connect({
    required String appId,
    required String authToken,
    required String customerEndUserId,
    String? postbackURL,
  }) async {
    try {
      final connection = await SpikeSDK.createConnection(
        appId: appId,
        authToken: authToken,
        customerEndUserId: customerEndUserId,
        logger: EventsLogger(),
        postbackURL: postbackURL,
      );
      _availableConnections.add(connection);

      await _saveConnection(connection, authToken);

      notifyListeners();

      return OperationResultType.success;
    } on SpikeException catch (ex) {
      _errorMessage = ex.message;
      notifyListeners();
    }

    return OperationResultType.error;
  }

  Future<void> _saveConnection(
    final SpikeConnection connection,
    final String authToken,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    final packed = _pack(connection, authToken);
    if (packed.isEmpty) {
      return;
    }

    final ids = prefs.getStringList('connection_ids') ?? [];
    final cid = connection.connectionId;
    if (!ids.contains(cid)) {
      ids.add(cid);
    }

    await Future.wait([
      prefs.setStringList('connection_ids', ids),
      prefs.setString('connection_$cid', packed),
    ]);
  }

  void clearErrors() {
    _errorMessage = null;
    notifyListeners();
  }

  Future<OperationResultType> getData({
    required SpikeConnection connection,
    required DateTime startDate,
    required DateTime endDate,
    required SpikeDataType dataType,
    required bool ignoreDateRange,
    required DataMode mode,
  }) async {
    _errorMessage = null;
    notifyListeners();

    try {
      if (mode == DataMode.extractAndPostData &&
          connection.callbackUrl == null) {
        _errorMessage = Texts.noCallbackURL;
        return OperationResultType.error;
      }

      if (!_retrievedData.containsKey(connection.connectionId)) {
        _retrievedData[connection.connectionId] = [];
      }

      final start = DateTime.now();
      if (mode == DataMode.extractAndPostData) {
        final data = await connection.extractAndPostData(
          dataType,
          from: ignoreDateRange ? null : startDate,
          to: ignoreDateRange ? null : endDate,
        );
        final end = DateTime.now();

        _retrievedData[connection.connectionId]!.insert(
          0,
          ConnectionDataRecord(
            type: dataType,
            start: start,
            end: end,
            data: data.toObject(),
          ),
        );
      } else {
        final data = await connection.extractData(
          dataType,
          from: ignoreDateRange ? null : startDate,
          to: ignoreDateRange ? null : endDate,
        );

        final end = DateTime.now();

        _retrievedData[connection.connectionId]!.insert(
          0,
          ConnectionDataRecord(
            type: dataType,
            start: start,
            end: end,
            data: data.toObject(),
          ),
        );
      }

      return OperationResultType.success;
    } on SpikeException catch (ex) {
      _errorMessage = ex.message;
    } catch (ex) {
      _errorMessage = ex.toString();
    }

    notifyListeners();

    return OperationResultType.error;
  }

  Future<void> _eraseConnection(String connectionId) async {
    final prefs = await SharedPreferences.getInstance();
    final ids = prefs.getStringList('connection_ids') ?? [];
    ids.remove(connectionId);

    await Future.wait([
      prefs.setStringList('connection_ids', ids),
      prefs.remove('connection_$connectionId'),
    ]);

    _availableConnections.removeWhere(
      (element) => element.connectionId == connectionId,
    );

    notifyListeners();
  }

  Future<List<SpikeDataType>> getCurrentTypes({
    required SpikeConnection connection,
  }) async {
    if (connection.callbackUrl == null) {
      return [];
    }

    final types = await connection.getBackgroundDeliveryDataTypes();
    final result = types.toList();

    return result;
  }

  Future<OperationResultType> setBackgroundDeliveryTypes({
    required SpikeConnection connection,
    required List<SpikeDataType> types,
  }) async {
    if (connection.callbackUrl == null) {
      return OperationResultType.error;
    }

    try {
      await connection.enableBackgroundDelivery(types);
      await connection.setListener(BackgroundListener());

      return OperationResultType.success;
    } on SpikeException catch (ex) {
      _errorMessage = ex.message;
    } catch (ex) {
      _errorMessage = ex.toString();
    }

    return OperationResultType.error;
  }

  Future<SpikeConnection> _unpackConnection(final String packed) async {
    final data = jsonDecode(packed);

    final connection = await SpikeSDK.createConnection(
      desiredConnectionId: data['connectionId'],
      appId: data['appId'],
      authToken: data['authToken'],
      customerEndUserId: data['customerEndUserId'],
      logger: data['logger'] == true ? EventsLogger() : null,
      postbackURL: data['callbackUrl'],
    );

    return connection;
  }

  String _pack(
    final SpikeConnection connection,
    final String authToken,
  ) {
    final data = {
      'connectionId': connection.connectionId,
      'appId': connection.appId,
      'customerEndUserId': connection.customerEndUserId,
      'logger': connection.logger != null,
      'callbackUrl': connection.callbackUrl,
      'authToken': authToken,
    };

    final result = jsonEncode(data);

    return result;
  }

  Future<OperationResultType> disableBackgroundDelivery({
    required SpikeConnection connection,
  }) async {
    try {
      await connection.disableBackgroundDelivery();

      return OperationResultType.success;
    } catch (ex) {
      return OperationResultType.error;
    }
  }
}
