import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/data/repositories/events_log_repository.dart';

class EventsLogProvider with ChangeNotifier {
  static final _repository = EventsLogRepository();

  final List<LogRecord> _events = [];
  List<LogRecord> get events => [..._events];

  Future<void> load() async {
    final result = await _repository.getAll();
    _events.clear();
    _events.addAll(result);
    notifyListeners();
  }

  Future<void> clear() async {
    await _repository.clearAll();
    _events.clear();
    notifyListeners();
  }
}
