import 'dart:convert';

import 'package:encrypt/encrypt.dart' as enc;
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:spike_sdk_test_app/data/model/atoms/connection_credentials.dart';

class CredentialsProvider with ChangeNotifier {
  Future<ConnectionCredentials?> loadCredentials(final String key) async {
    try {
      final asset = await rootBundle.loadString('assets/credentials.enc');

      final decryptionKey = enc.Key.fromUtf8('${key}______________________');
      final iv = enc.IV.fromUtf8('${key}______');

      final encrypter = enc.Encrypter(enc.AES(
        decryptionKey,
        mode: enc.AESMode.cbc,
      ));

      final decrypted = encrypter.decrypt(
        enc.Encrypted.fromBase64(asset),
        iv: iv,
      );
      final data = jsonDecode(decrypted);

      return ConnectionCredentials(
        appId: data['appId'],
        authToken: data['authToken'],
      );
    } catch (ex) {
      return null;
    }
  }
}
