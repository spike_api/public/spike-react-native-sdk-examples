import 'dart:convert';

import 'package:http/http.dart' as http;

class WebhookUrlRepository {
  static final WebhookUrlRepository _singleton =
      WebhookUrlRepository._internal();

  factory WebhookUrlRepository() {
    return _singleton;
  }

  WebhookUrlRepository._internal();

  Future<String?> generateWebHookUrl() async {
    try {
      final endpoint = Uri.parse('https://webhook.site/token');
      final response = await http.post(endpoint, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      });

      final uuid = jsonDecode(response.body)['uuid'];
      final url = 'https://webhook.site/$uuid';

      return url;
    } catch (ex) {
      return null;
    }
  }
}
