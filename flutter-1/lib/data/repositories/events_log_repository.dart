import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';

class EventsLogRepository {
  static final EventsLogRepository _singleton = EventsLogRepository._internal();

  factory EventsLogRepository() {
    return _singleton;
  }

  EventsLogRepository._internal();

  Future<void> add(final LogRecord event) async {
    final prefs = await SharedPreferences.getInstance();
    final count = prefs.getInt("events_count") ?? 0;

    await Future.wait([
      prefs.setInt("events_count", count + 1),
      prefs.setString("event_$count", jsonEncode(event.toObject())),
    ]);
  }

  Future<List<LogRecord>> getAll() async {
    final prefs = await SharedPreferences.getInstance();
    final count = prefs.getInt("events_count") ?? 0;

    final result = <LogRecord>[];
    for (var i = 0; i < count; i++) {
      final eventString = prefs.getString("event_$i");
      if (eventString == null) {
        continue;
      }

      final event = LogRecord.fromObject(jsonDecode(eventString));
      result.add(event);
    }

    return result;
  }

  Future<void> clearAll({
    bool hardClean = false,
  }) async {
    final prefs = await SharedPreferences.getInstance();
    final count = prefs.getInt("events_count") ?? 0;
    final futures = <Future>[];

    if (hardClean) {
      for (var i = 0; i < count; i++) {
        futures.add(prefs.remove("event_$i"));
      }
    }

    futures.add(prefs.remove("events_count"));
    await Future.wait(futures);
  }
}
