class ConnectionCredentials {
  final String appId;
  final String authToken;

  const ConnectionCredentials({
    required this.appId,
    required this.authToken,
  });
}
