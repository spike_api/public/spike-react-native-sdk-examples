class LogRecord {
  final String connectionId;
  final String message;
  final String level;
  final DateTime date;

  const LogRecord({
    required this.connectionId,
    required this.message,
    required this.level,
    required this.date,
  });

  factory LogRecord.fromObject(final dynamic object) => LogRecord(
        connectionId: object['connectionId'],
        message: object['message'],
        level: object['level'],
        date: DateTime.parse(object['date']),
      );

  dynamic toObject() => {
        'connectionId': connectionId,
        'message': message,
        'level': level,
        'date': date.toIso8601String(),
      };
}
