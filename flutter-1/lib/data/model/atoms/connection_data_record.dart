import 'package:spike_flutter_sdk/model/data/spike_data_type.dart';

class ConnectionDataRecord {
  final SpikeDataType type;
  final DateTime start;
  final DateTime end;
  final dynamic data;

  const ConnectionDataRecord({
    required this.type,
    required this.start,
    required this.end,
    required this.data,
  });
}
