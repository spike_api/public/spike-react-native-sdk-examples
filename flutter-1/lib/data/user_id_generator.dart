import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class UserIdGenerator {
  static const _uuid = Uuid();

  String? currentUserId;

  Future<String> generateUserId() async {
    final prefs = await SharedPreferences.getInstance();
    currentUserId = _uuid.v4();
    prefs.setString("user_id", currentUserId!);

    return currentUserId!;
  }

  static final UserIdGenerator _singleton = UserIdGenerator._internal();
  factory UserIdGenerator() => _singleton;
  UserIdGenerator._internal();

  Future<void> init() async {
    final prefs = await SharedPreferences.getInstance();
    currentUserId = prefs.getString("user_id");
  }
}
