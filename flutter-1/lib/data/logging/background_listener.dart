import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/data/repositories/events_log_repository.dart';

class BackgroundListener extends SpikeWebhookConnectionListener {
  static final BackgroundListener _singleton = BackgroundListener._internal();

  factory BackgroundListener() {
    return _singleton;
  }

  BackgroundListener._internal();

  @override
  void onBackgroundLog(String log) => EventsLogRepository().add(LogRecord(
        connectionId: "unknown",
        message: log,
        level: "background",
        date: DateTime.now(),
      ));
}
