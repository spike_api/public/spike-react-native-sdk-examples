import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/data/repositories/events_log_repository.dart';

class EventsLogger extends SpikeLogger {
  static final EventsLogger _singleton = EventsLogger._internal();

  factory EventsLogger() {
    return _singleton;
  }

  EventsLogger._internal();

  @override
  bool get isDebugEnabled => true;

  @override
  bool get isErrorEnabled => true;

  @override
  bool get isInfoEnabled => true;

  @override
  void debug(SpikeConnection connection, String message) =>
      _log(connection, message, 'debug');

  @override
  void error(SpikeConnection connection, String message) =>
      _log(connection, message, 'error');

  @override
  void info(SpikeConnection connection, String message) =>
      _log(connection, message, 'info');

  void _log(SpikeConnection connection, String message, String level) =>
      EventsLogRepository().add(LogRecord(
        connectionId: connection.connectionId,
        message: message,
        level: level,
        date: DateTime.now(),
      ));
}
