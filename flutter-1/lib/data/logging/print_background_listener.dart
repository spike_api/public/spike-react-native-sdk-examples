import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';

class PrintBackgroundListener extends SpikeWebhookConnectionListener {
  const PrintBackgroundListener();

  @override
  void onBackgroundLog(String log) => print(log);
}
