import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/connection/spike_sdk.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/header.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/screen_container.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool? _isAvailable;

  @override
  void initState() {
    super.initState();

    Future.microtask(_resolveHealthConnectAvailability);
  }

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      children: [
        const Header(Texts.home),
        const Text(Texts.welcome),
        const Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              Texts.healthConnectAvailable,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
            if (_isAvailable == true)
              const Text(
                Texts.generalYes,
                style: TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.w500,
                ),
              ),
            if (_isAvailable == false)
              const Text(
                Texts.generalNo,
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.w500,
                ),
              ),
            if (_isAvailable == null)
              const Text(
                Texts.generalResolving,
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.w500,
                ),
              ),
          ],
        ),
      ],
    );
  }

  Future<void> _resolveHealthConnectAvailability() async {
    final result = await SpikeSDK.isPackageInstalled();
    if (!mounted) {
      return;
    }

    setState(() {
      _isAvailable = result;
    });
  }
}
