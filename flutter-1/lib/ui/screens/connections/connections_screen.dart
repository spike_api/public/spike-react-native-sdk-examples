import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/header.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/connection/connection_item_action.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/screen_container.dart';
import 'package:spike_sdk_test_app/ui/components/organisms/connections_list.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/background_delivery_configuration/background_delivery_configuration_form.dart';
import 'package:spike_sdk_test_app/ui/forms/create_connection/create_connection_form.dart';
import 'package:spike_sdk_test_app/ui/forms/manage_data/manage_data_form.dart';
import 'package:spike_sdk_test_app/ui/screens/connections/connection_data_screen.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';
import 'package:url_launcher/url_launcher.dart';

class ConnectionsScreen extends StatelessWidget {
  const ConnectionsScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createNewConnection(context),
        child: const Icon(Icons.add),
      ),
      children: [
        const Header(Texts.connections),
        ConnectionsList(
          onActionTriggered: (action, connection) => _processAction(
            context,
            action,
            connection,
          ),
        ),
      ],
    );
  }

  void _createNewConnection(BuildContext context) => UtilsUi.showSheet(
        context: context,
        builder: (ctx) => const CreateConnectionForm(),
      );

  void _processAction(
    BuildContext context,
    ConnectionItemAction action,
    SpikeConnection connection,
  ) {
    final provider = Provider.of<ConnectionsProvider>(context, listen: false);
    switch (action) {
      case ConnectionItemAction.manageData:
        UtilsUi.showSheet(
          context: context,
          builder: (ctx) => ManageDataForm(connection: connection),
        );
        break;
      case ConnectionItemAction.disconnect:
        provider.disconnect(
          connectionId: connection.connectionId,
        );
        break;
      case ConnectionItemAction.backgroundDelivery:
        _processBackgroundDelivery(context, connection);
        break;
      case ConnectionItemAction.viewData:
        _processViewData(context, connection);
        break;
      case ConnectionItemAction.revokeAllPermissions:
        _revokeAllPermissions(context, connection);
        break;
      case ConnectionItemAction.openWebHookURL:
        _openWebHookURL(context, connection);
        break;
    }
  }

  void _processBackgroundDelivery(
    BuildContext context,
    SpikeConnection connection,
  ) {
    if (Platform.isAndroid) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.bgDeliveryAndroidNoSupportTitle,
        message: Texts.bgDeliveryAndroidNoSupportText,
      );
      return;
    }

    if (connection.callbackUrl == null) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.bgDeliveryForWebHookConnectionsAvailableOnlyTitle,
        message: Texts.bgDeliveryForWebHookConnectionsAvailableOnlyText,
      );
      return;
    }

    UtilsUi.showSheet(
      context: context,
      builder: (ctx) => BackgroundDeliveryConfigurationForm(
        connection: connection,
      ),
    );
  }

  void _processViewData(
    BuildContext context,
    SpikeConnection connection,
  ) =>
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ConnectionDataScreen(
            connection: connection,
          ),
        ),
      );

  void _revokeAllPermissions(
    BuildContext context,
    SpikeConnection connection,
  ) =>
      connection.revokeAllPermissions().then((result) {
        UtilsUi.showAlertDialog(
          context,
          title: Texts.revokedTitle,
          message: Texts.revokedText,
        );
      });

  Future<void> _openWebHookURL(
    BuildContext context,
    SpikeConnection connection,
  ) async {
    final webHookUrl = connection.callbackUrl?.replaceAll(
          '.site/',
          '.site/#!/',
        ) ??
        '';
    final url = Uri.tryParse(webHookUrl);

    if (url == null) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.failedLaunchUrlTitle,
        message: Texts.failedLaunchUrlText.replaceAll('{url}', webHookUrl),
      );
      return;
    }

    if (!await launchUrl(url, mode: LaunchMode.externalApplication)) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.failedLaunchUrlTitle,
        message: Texts.failedLaunchUrlText.replaceAll('{url}', webHookUrl),
      );
      return;
    }
  }
}
