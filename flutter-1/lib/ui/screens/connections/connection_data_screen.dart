import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/model/atoms/connection_data_record.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/header.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/connection_records_list.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/screen_container.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/sheets/connection_record_sheet.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';

class ConnectionDataScreen extends StatefulWidget {
  final SpikeConnection connection;

  const ConnectionDataScreen({
    required this.connection,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ConnectionDataScreenState();
}

class _ConnectionDataScreenState extends State<ConnectionDataScreen> {
  late List<ConnectionDataRecord> _connectionData;

  @override
  void initState() {
    super.initState();

    _connectionData = Provider.of<ConnectionsProvider>(
      context,
      listen: false,
    ).getRetrievedData(
      widget.connection.connectionId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      showBackButton: true,
      children: [
        const Header(Texts.connectionData),
        const SizedBox(height: WidgetSize.s2),
        ConnectionRecordsList(
          records: _connectionData,
          onViewData: (record) => _viewData(context, record),
        ),
      ],
    );
  }

  void _viewData(BuildContext context, ConnectionDataRecord record) =>
      UtilsUi.showSheet(
        context: context,
        builder: (ctx) => ConnectionRecordSheet(record: record),
      );
}
