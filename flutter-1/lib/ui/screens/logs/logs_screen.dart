import 'dart:convert';
import 'dart:typed_data';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/data/providers/events_log_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/header.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/screen_container.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/spike_events_list.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/sheets/spike_event/spike_event_data_sheet.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';

class LogsScreen extends StatefulWidget {
  const LogsScreen({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LogsScreenState();
}

class _LogsScreenState extends State<LogsScreen> {
  late EventsLogProvider _logs;
  late Future<void> _loadingFuture;

  @override
  void initState() {
    super.initState();

    _logs = Provider.of<EventsLogProvider>(context, listen: false);
    _loadingFuture = Future.microtask(_logs.load);
  }

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      children: [
        const Header(Texts.logs),
        const Divider(),
        FutureBuilder(
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }

            return Consumer<EventsLogProvider>(builder: (ctx, provider, _) {
              final events = provider.events;
              return SpikeEventsList(
                events: events,
                onClearLogs: _clearLogs,
                onViewData: _viewData,
                onShareLogs: () => _shareLogs(events),
              );
            });
          },
          future: _loadingFuture,
        ),
      ],
    );
  }

  void _clearLogs() {
    setState(() {
      _loadingFuture = Future.microtask(_logs.clear);
    });
  }

  void _viewData(LogRecord event) => UtilsUi.showSheet(
        context: context,
        builder: (ctx) => SpikeEventDataSheet(event: event),
      );

  Future<void> _shareLogs(final List<LogRecord> events) async {
    const encoder = JsonEncoder.withIndent('  ');

    final deviceInfoPlugin = DeviceInfoPlugin();
    final deviceInfo = await deviceInfoPlugin.deviceInfo;
    final allInfo = deviceInfo.data;

    var logs = 'Device info:\r\n\r\n';
    logs += encoder.convert(allInfo);
    logs += '\r\n\r\n\r\n';
    for (final event in events) {
      logs +=
          '${event.level} [${event.connectionId}]: ${event.date.toIso8601String()}\r\n\r\n';
      logs += encoder.convert(event.message);
      logs += '\r\n\r\n\r\n';
    }

    final now = DateTime.now();
    final fileName =
        '${now.year}_${now.month}-${now.day}_${now.hour}.${now.minute}.txt';
    final file = XFile.fromData(
      Uint8List.fromList(utf8.encode(logs)),
      name: fileName,
      mimeType: 'text/plain',
    );

    Share.shareXFiles(
      [file],
      subject: Texts.logsShared,
      text: Texts.logsSharedInfo,
    );

    Share.share(logs, subject: Texts.logsShared);
  }
}
