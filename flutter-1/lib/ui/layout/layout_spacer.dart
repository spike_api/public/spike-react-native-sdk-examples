import 'package:flutter/material.dart';

abstract class LayoutSpacer {
  static Widget horizontal(double width) => SizedBox(width: width);

  static Widget vertical(double height) => SizedBox(height: height);
}
