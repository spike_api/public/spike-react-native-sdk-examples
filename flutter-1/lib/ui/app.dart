import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/data/providers/credentials_provider.dart';
import 'package:spike_sdk_test_app/data/providers/events_log_provider.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/screens/connections/connections_screen.dart';
import 'package:spike_sdk_test_app/ui/screens/home/home_screen.dart';
import 'package:spike_sdk_test_app/ui/screens/logs/logs_screen.dart';

class App extends StatelessWidget {
  const App({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider<ConnectionsProvider>(
          create: (_) => ConnectionsProvider(),
        ),
        ListenableProvider<EventsLogProvider>(
          create: (_) => EventsLogProvider(),
        ),
        ListenableProvider<CredentialsProvider>(
          create: (_) => CredentialsProvider(),
        ),
      ],
      child: MaterialApp(
        title: Texts.appTitle,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: {
          Routes.home: (ctx) => const HomeScreen(),
          Routes.logs: (ctx) => const LogsScreen(),
          Routes.connections: (ctx) => const ConnectionsScreen(),
        },
      ),
    );
  }
}
