import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/extensions/string_extension.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/info_label.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/connection/connection_item_action.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ConnectionItem extends StatelessWidget {
  final SpikeConnection connection;
  final void Function(ConnectionItemAction) onActionTriggered;

  const ConnectionItem({
    required this.connection,
    required this.onActionTriggered,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4.0,
      child: Padding(
        padding: Paddings.p2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Wrap(
              spacing: Spacings.s1,
              children: [
                InfoLabel(
                  prefixText: Texts.connectionsConnectionIdLabel,
                  text: connection.connectionId.toMaskedString(),
                ),
                InfoLabel(
                  prefixText: Texts.customerID,
                  text: connection.customerEndUserId.toMaskedString(),
                  color: Colors.lightGreen,
                ),
                InfoLabel(
                  prefixText: Texts.webHookInfo,
                  text: connection.callbackUrl != null ? Texts.yes : Texts.no,
                  color: Colors.blue,
                ),
                if (connection.callbackUrl != null)
                  InfoLabel(
                    text: connection.callbackUrl!,
                    color: Colors.orange,
                  ),
              ],
            ),
            Wrap(
              spacing: Spacings.s1,
              children: [
                TextButton(
                  onPressed: () => onActionTriggered(
                    ConnectionItemAction.backgroundDelivery,
                  ),
                  child: const Text(Texts.backgroundDelivery),
                ),
                TextButton(
                  onPressed: () => onActionTriggered(
                    ConnectionItemAction.manageData,
                  ),
                  child: const Text(Texts.getData),
                ),
                TextButton(
                  onPressed: () => onActionTriggered(
                    ConnectionItemAction.disconnect,
                  ),
                  child: const Text(Texts.disconnect),
                ),
                TextButton(
                  onPressed: () => onActionTriggered(
                    ConnectionItemAction.viewData,
                  ),
                  child: const Text(Texts.viewResponses),
                ),
                TextButton(
                  onPressed: () => onActionTriggered(
                    ConnectionItemAction.revokeAllPermissions,
                  ),
                  child: const Text(Texts.revokeAllPermissions),
                ),
                if (connection.callbackUrl != null)
                  TextButton(
                    onPressed: () => onActionTriggered(
                      ConnectionItemAction.openWebHookURL,
                    ),
                    child: const Text(Texts.openWebHookURL),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
