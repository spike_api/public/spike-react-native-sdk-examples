enum ConnectionItemAction {
  manageData,
  disconnect,
  backgroundDelivery,
  viewData,
  openWebHookURL,
  revokeAllPermissions,
}
