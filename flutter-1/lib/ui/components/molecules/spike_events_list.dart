import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/spike_event_card.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class SpikeEventsList extends StatelessWidget {
  final List<LogRecord> events;
  final void Function() onClearLogs;
  final void Function() onShareLogs;
  final void Function(LogRecord) onViewData;

  const SpikeEventsList({
    required this.events,
    required this.onClearLogs,
    required this.onShareLogs,
    required this.onViewData,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    if (events.isEmpty) {
      return const Text(Texts.noDataInLog);
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Wrap(
          spacing: Spacings.s2,
          children: [
            TextButton(
              onPressed: onClearLogs,
              style: const ButtonStyle(
                foregroundColor: MaterialStatePropertyAll(Colors.red),
              ),
              child: const Text(Texts.clearLogs),
            ),
            TextButton(
              onPressed: onShareLogs,
              child: const Text(Texts.share),
            ),
          ],
        ),
        const Divider(),
        ListView.separated(
          itemBuilder: (ctx, index) {
            final event = events[index];
            return SpikeEventCard(
              event: event,
              onViewData: () => onViewData(event),
            );
          },
          separatorBuilder: (ctx, index) {
            return const SizedBox(height: Spacings.s3);
          },
          itemCount: events.length,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
        ),
      ],
    );
  }
}
