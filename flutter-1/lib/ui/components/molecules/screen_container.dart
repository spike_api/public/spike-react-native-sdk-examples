import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/drawer_menu/drawer_menu.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ScreenContainer extends StatelessWidget {
  final List<Widget> children;
  final Widget? floatingActionButton;
  final bool showBackButton;

  const ScreenContainer({
    required this.children,
    this.floatingActionButton,
    this.showBackButton = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text(Texts.appTitle),
          leading: showBackButton ? const BackButton() : null,
        ),
        floatingActionButton: floatingActionButton,
        drawer: const Drawer(
          child: DrawerMenu(
            items: [
              DrawerMenuItem(
                leading: Icon(Icons.home),
                title: Texts.home,
                path: Routes.home,
              ),
              DrawerMenuItem(
                leading: Icon(Icons.private_connectivity_outlined),
                title: Texts.connections,
                path: Routes.connections,
              ),
              DrawerMenuItem(
                leading: Icon(Icons.data_exploration),
                title: Texts.logs,
                path: Routes.logs,
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: Paddings.p2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: children,
            ),
          ),
        ),
      );
}
