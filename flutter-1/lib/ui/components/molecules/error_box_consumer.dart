import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/message/message_box.dart';

class ErrorBoxConsumer<T> extends StatelessWidget {
  final String? Function(T) errorResolver;

  const ErrorBoxConsumer({
    required this.errorResolver,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<T>(builder: (ctx, provider, _) {
      final errorMessage = errorResolver(provider);
      if (errorMessage == null) {
        return const SizedBox.shrink();
      }

      return MessageBox(
        type: OperationResultType.error,
        message: errorMessage,
      );
    });
  }
}
