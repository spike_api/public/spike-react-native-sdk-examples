import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/choice_field.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class SpikeDataTypeChoiceField extends StatefulWidget {
  final void Function(SpikeDataType) onChanged;
  final SpikeDataType initialValue;

  const SpikeDataTypeChoiceField({
    required this.onChanged,
    required this.initialValue,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _SpikeDataTypeChoiceFieldState();
}

class _SpikeDataTypeChoiceFieldState extends State<SpikeDataTypeChoiceField> {
  static const supportedTypes = [
    SpikeDataType.activitiesSummary,
    SpikeDataType.activitiesStream,
    SpikeDataType.body,
    SpikeDataType.breathing,
    SpikeDataType.calories,
    SpikeDataType.distance,
    SpikeDataType.glucose,
    SpikeDataType.heart,
    SpikeDataType.oxygenSaturation,
    SpikeDataType.sleep,
    SpikeDataType.steps,
    SpikeDataType.stepsIntraday,
  ];

  late SpikeDataType _selected;

  @override
  void initState() {
    super.initState();

    _selected = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.start,
      spacing: Spacings.s2,
      runSpacing: Spacings.s2,
      children: [
        ...supportedTypes.map(
          (type) => ChoiceField(
            title: Texts.getName(type as SpikeDataType),
            isSelected: _selected == type,
            onTriggered: () => _processChange(type),
          ),
        ),
      ],
    );
  }

  void _processChange(SpikeDataType value) {
    if (value == _selected) {
      return;
    }

    setState(() {
      _selected = value;
    });

    widget.onChanged(value);
  }
}
