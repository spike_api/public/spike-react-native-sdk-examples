import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/connection_data_record.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/connection_data_record_widget.dart';
import 'package:spike_sdk_test_app/ui/config/texts.dart';

class ConnectionRecordsList extends StatelessWidget {
  final List<ConnectionDataRecord> records;
  final void Function(ConnectionDataRecord) onViewData;

  const ConnectionRecordsList({
    required this.records,
    required this.onViewData,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    if (records.isEmpty) {
      return const Text(Texts.noData);
    }

    return ListView.separated(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (itemCtx, index) {
        final record = records[index];
        return ConnectionDataRecordWidget(
          record: record,
          onViewData: () => onViewData(record),
        );
      },
      separatorBuilder: (itemCtx, index) {
        return const Divider();
      },
      itemCount: records.length,
    );
  }
}
