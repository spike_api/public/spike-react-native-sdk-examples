import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/choice_field.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class SpikeDataTypeMultiChoiceField extends StatefulWidget {
  final void Function(List<SpikeDataType>) onChanged;
  final List<SpikeDataType> initialValues;

  const SpikeDataTypeMultiChoiceField({
    required this.onChanged,
    required this.initialValues,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _SpikeDataTypeMultiChoiceFieldState();
}

class _SpikeDataTypeMultiChoiceFieldState
    extends State<SpikeDataTypeMultiChoiceField> {
  static const supportedTypes = [
    SpikeDataType.activitiesSummary,
    SpikeDataType.activitiesStream,
    SpikeDataType.body,
    SpikeDataType.breathing,
    SpikeDataType.calories,
    SpikeDataType.distance,
    SpikeDataType.glucose,
    SpikeDataType.heart,
    SpikeDataType.oxygenSaturation,
    SpikeDataType.sleep,
    SpikeDataType.steps,
    SpikeDataType.stepsIntraday,
  ];

  late List<SpikeDataType> _selected;

  @override
  void initState() {
    super.initState();

    _selected = widget.initialValues;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.start,
      spacing: Spacings.s2,
      runSpacing: Spacings.s2,
      children: [
        ...supportedTypes.map(
          (type) => ChoiceField(
            title: Texts.getName(type as SpikeDataType),
            isSelected: _selected.contains(type),
            onTriggered: () => _processChange(type),
          ),
        ),
      ],
    );
  }

  void _processChange(SpikeDataType value) {
    if (_selected.contains(value)) {
      setState(() {
        _selected.remove(value);
      });
      widget.onChanged(_selected);
      return;
    }

    setState(() {
      _selected.add(value);
    });

    widget.onChanged(_selected);
  }
}
