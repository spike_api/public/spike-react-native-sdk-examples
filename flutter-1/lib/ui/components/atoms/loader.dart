import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class Loader extends StatelessWidget {
  final bool isLoading;
  final String? message;

  const Loader({
    required this.isLoading,
    this.message,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    if (!isLoading) {
      return const SizedBox.shrink();
    }

    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: Spacings.s2,
      children: [
        const SizedBox(
          width: WidgetSize.s4,
          height: WidgetSize.s4,
          child: CircularProgressIndicator(
            strokeWidth: WidgetSize.s0_5,
          ),
        ),
        if (message != null) Text(message!),
      ],
    );
  }
}
