import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/connection_data_record.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ConnectionDataRecordWidget extends StatelessWidget {
  final ConnectionDataRecord record;
  final void Function() onViewData;

  const ConnectionDataRecordWidget({
    required this.record,
    required this.onViewData,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4.0,
      child: Padding(
        padding: Paddings.p2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: Paddings.p1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(Texts.dataType),
                        Text(
                          Texts.getName(record.type),
                          style: const TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    const SizedBox(height: WidgetSize.s1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(Texts.startDate),
                        Text(
                          record.start.toLocal().toIso8601String(),
                          style: const TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    const SizedBox(height: WidgetSize.s1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(Texts.endDate),
                        Text(
                          record.end.toLocal().toIso8601String(),
                          style: const TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    const SizedBox(height: WidgetSize.s1),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(Texts.duration),
                        Text(
                          '${record.end.difference(record.start).inMilliseconds}ms',
                          style: const TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                )),
            TextButton(
              onPressed: onViewData,
              child: const Text(Texts.viewData),
            ),
          ],
        ),
      ),
    );
  }
}
