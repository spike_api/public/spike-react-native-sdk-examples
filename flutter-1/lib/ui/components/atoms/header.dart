import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/font_size.dart';

class Header extends StatelessWidget {
  final String label;
  const Header(
    this.label, {
    super.key,
  });

  @override
  Widget build(BuildContext context) => Text(
        label,
        style: const TextStyle(
          fontSize: FontSize.header,
          fontWeight: FontWeight.w500,
        ),
      );
}
