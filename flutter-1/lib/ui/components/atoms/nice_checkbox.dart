import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/layout/layout_spacer.dart';

class NiceCheckbox extends StatelessWidget {
  final String title;
  final bool checked;
  final void Function() onTriggered;

  const NiceCheckbox({
    required this.title,
    required this.checked,
    required this.onTriggered,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTriggered,
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(6.0),
            ),
            width: WidgetSize.s6,
            height: WidgetSize.s6,
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4.0),
                ),
                width: WidgetSize.s6 - 3.0,
                height: WidgetSize.s6 - 3.0,
                child: checked
                    ? const Center(
                        child: Icon(
                          Icons.check,
                          color: Colors.blue,
                          size: WidgetSize.s4,
                        ),
                      )
                    : null,
              ),
            ),
          ),
          LayoutSpacer.horizontal(Spacings.s2),
          Text(
            title,
            style: const TextStyle(fontSize: FontSize.title),
          ),
        ],
      ),
    );
  }
}
