import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/font_size.dart';

class ErrorText extends StatelessWidget {
  final String? text;

  const ErrorText(
    this.text, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    if (text == null) {
      return const SizedBox.shrink();
    }

    return Text(
      text!,
      style: const TextStyle(
        fontSize: FontSize.normal,
        color: Colors.red,
      ),
    );
  }
}
