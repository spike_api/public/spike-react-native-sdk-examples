import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/layout/layout_spacer.dart';

class NiceRadio extends StatelessWidget {
  final String title;
  final bool checked;
  final void Function() onTriggered;
  final bool disabled;

  const NiceRadio({
    required this.title,
    required this.checked,
    required this.onTriggered,
    this.disabled = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: disabled ? null : onTriggered,
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: disabled ? Colors.grey : Colors.blue,
              borderRadius: BorderRadius.circular(WidgetSize.s6),
            ),
            width: WidgetSize.s6,
            height: WidgetSize.s6,
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(WidgetSize.s6 - 3.0),
                ),
                width: WidgetSize.s6 - 3.0,
                height: WidgetSize.s6 - 3.0,
                child: checked
                    ? Center(
                        child: Container(
                          width: WidgetSize.s6 - 9.0,
                          height: WidgetSize.s6 - 9.0,
                          decoration: BoxDecoration(
                            color: disabled ? Colors.grey : Colors.blue,
                            borderRadius: BorderRadius.circular(
                              WidgetSize.s6 - 9.0,
                            ),
                          ),
                        ),
                      )
                    : null,
              ),
            ),
          ),
          LayoutSpacer.horizontal(Spacings.s2),
          Text(
            title,
            style: TextStyle(
              fontSize: FontSize.title,
              color: disabled ? Colors.grey : Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
