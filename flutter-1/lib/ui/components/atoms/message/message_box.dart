import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class MessageBox extends StatelessWidget {
  final OperationResultType type;
  final String message;
  const MessageBox({
    required this.type,
    required this.message,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: _getColor(),
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: Paddings.p2,
      child: Text(
        message,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }

  Color _getColor() {
    switch (type) {
      case OperationResultType.success:
        return Colors.green;
      case OperationResultType.warning:
        return Colors.orangeAccent;
      case OperationResultType.error:
        return Colors.red;
    }
  }
}
