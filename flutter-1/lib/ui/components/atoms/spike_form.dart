import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/controllers/spike_form_controller.dart';
import 'package:spike_sdk_test_app/data/validators/spike_form_validator.dart';
import 'package:spike_sdk_test_app/ui/config/font_size.dart';
import 'package:spike_sdk_test_app/ui/config/paddings.dart';
import 'package:spike_sdk_test_app/ui/config/texts.dart';

typedef FormWidgetBuilder<
        TWidget extends Widget,
        TController extends SpikeFormController<TWidget>,
        TValidator extends SpikeFormValidator>
    = Widget Function(TController, TValidator, BuildContext);

class SpikeForm<
    TWidget extends Widget,
    TController extends SpikeFormController<TWidget>,
    TValidator extends SpikeFormValidator> extends StatefulWidget {
  final TController controller;
  final TValidator validator;
  final FormWidgetBuilder<TWidget, TController, TValidator> builder;
  final TWidget parent;
  final String? formTitle;
  final Widget Function(BuildContext)? errorBuilder;
  final String? actionTitle;

  const SpikeForm({
    required this.controller,
    required this.validator,
    required this.builder,
    required this.parent,
    this.formTitle,
    this.errorBuilder,
    this.actionTitle,
    super.key,
  });

  @override
  State<StatefulWidget> createState() =>
      _SpikeFormState<TWidget, TController, TValidator>();
}

class _SpikeFormState<
        TWidget extends Widget,
        TController extends SpikeFormController<TWidget>,
        TValidator extends SpikeFormValidator>
    extends State<SpikeForm<TWidget, TController, TValidator>> {
  late GlobalKey<FormState> _formKey;
  bool _initialized = false;

  @override
  void initState() {
    super.initState();

    widget.controller.setState(this);
    _formKey = GlobalKey<FormState>();
    Future.microtask(_initializeForm);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: _initialized ? _buildForm() : const CircularProgressIndicator(),
    );
  }

  Future<void> _initializeForm() async {
    await widget.controller.initialize(context, widget.parent);

    setState(() {
      _initialized = true;
    });
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      padding: Paddings.p4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (widget.formTitle != null)
            Text(
              widget.formTitle!,
              style: const TextStyle(
                fontSize: FontSize.header,
                fontWeight: FontWeight.w500,
              ),
            ),
          if (widget.errorBuilder != null) widget.errorBuilder!(context),
          if (widget.formTitle != null) const Divider(),
          widget.builder(widget.controller, widget.validator, context),
          const Divider(),
          SizedBox(
            height: 48.0,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: widget.controller.isLoading
                          ? null
                          : () => widget.controller.save(_formKey.currentState),
                      child: Text(widget.actionTitle ?? Texts.save),
                    ),
                  ),
                ),
                if (widget.controller.isLoading)
                  const Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: 8.0),
                      child: SizedBox(
                        width: 16.0,
                        height: 16.0,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 2.0,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
