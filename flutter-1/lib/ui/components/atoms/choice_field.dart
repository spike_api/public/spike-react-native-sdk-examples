import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ChoiceField extends StatelessWidget {
  final String title;
  final bool isSelected;
  final void Function() onTriggered;

  const ChoiceField({
    required this.title,
    required this.isSelected,
    required this.onTriggered,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTriggered,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.lightGreen,
          borderRadius: BorderRadius.circular(4.0),
        ),
        padding: Paddings.p1,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CircleAvatar(
              backgroundColor: Colors.white,
              radius: 8.0,
              child: isSelected
                  ? const Icon(
                      Icons.check,
                      size: 16.0,
                      color: Colors.lightGreen,
                    )
                  : null,
            ),
            const SizedBox(
              width: Spacings.s1,
            ),
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
