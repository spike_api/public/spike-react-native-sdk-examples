import 'package:flutter/material.dart';

class DrawerMenuItem {
  final String title;
  final String path;
  final Widget leading;
  final bool? isEnabled;

  const DrawerMenuItem({
    required this.title,
    required this.path,
    required this.leading,
    this.isEnabled,
  });
}
