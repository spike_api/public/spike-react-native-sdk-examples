import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/drawer_menu/drawer_menu_item.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

export 'package:spike_sdk_test_app/ui/components/atoms/drawer_menu/drawer_menu_item.dart';

class DrawerMenu extends StatelessWidget {
  final List<DrawerMenuItem> items;

  const DrawerMenu({
    required this.items,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text(Texts.appTitle),
        ),
        ...items.map(
          (item) => ListTile(
            tileColor: _isSamePath(context, item.path)
                ? Colors.blueAccent.withOpacity(0.25)
                : null,
            leading: Stack(
              children: [
                item.leading,
                if (item.isEnabled != null)
                  CircleAvatar(
                    backgroundColor:
                        item.isEnabled! ? Colors.green : Colors.grey,
                    radius: WidgetSize.s1,
                  ),
              ],
            ),
            title: Text(item.title),
            onTap: _isSamePath(context, item.path)
                ? null
                : () => _navigate(context, item.path),
          ),
        ),
      ],
    );
  }

  void _navigate(BuildContext context, String path) {
    if (_isSamePath(context, path)) {
      return;
    }

    // To close the Drawer menu.
    Navigator.pop(context);

    Navigator.popAndPushNamed(context, path);
  }

  bool _isSamePath(BuildContext context, String path) =>
      ModalRoute.of(context)?.settings.name == path;
}
