import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/model/atoms/log_record.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class SpikeEventCard extends StatelessWidget {
  final LogRecord event;
  final void Function() onViewData;

  const SpikeEventCard({
    required this.event,
    required this.onViewData,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4.0,
      child: Padding(
        padding: Paddings.p2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  event.level,
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                Text(event.date.toLocal().toIso8601String()),
              ],
            ),
            TextButton(
              onPressed: onViewData,
              child: const Text(Texts.viewData),
            ),
          ],
        ),
      ),
    );
  }
}
