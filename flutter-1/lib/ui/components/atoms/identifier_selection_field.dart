import 'package:flutter/material.dart';

class IdentifierSelectionField extends StatelessWidget {
  final String name;
  final void Function() onTriggered;
  final bool isChecked;

  const IdentifierSelectionField({
    required this.name,
    required this.onTriggered,
    required this.isChecked,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: isChecked,
      onChanged: (v) => onTriggered(),
      title: Text(name),
    );
  }
}
