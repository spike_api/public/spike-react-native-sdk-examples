import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/layout/layout_spacer.dart';

class InfoLabel extends StatelessWidget {
  final String text;
  final String? prefixText;
  final Color? color;
  final String? fullText;
  final double? textWidth;

  const InfoLabel({
    required this.text,
    this.prefixText,
    this.color,
    this.fullText,
    this.textWidth,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color ?? Colors.green,
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: Paddings.p1,
      child: SizedBox(
        width: textWidth ?? 160.0,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            if (prefixText != null)
              Text(
                '$prefixText: ',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            Expanded(
              child: Text(
                text,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            LayoutSpacer.horizontal(Spacings.s1),
            InkWell(
              child: const Icon(
                Icons.copy,
                color: Colors.white,
              ),
              onTap: () => _copy(context),
            ),
          ],
        ),
      ),
    );
  }

  void _copy(final BuildContext context) {
    Clipboard.setData(ClipboardData(text: fullText ?? text));

    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text(Texts.copiedConfirmation),
    ));
  }
}
