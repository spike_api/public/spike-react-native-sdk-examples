import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk/connection/spike_connection.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/connection/connection_item.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/connection/connection_item_action.dart';
import 'package:spike_sdk_test_app/ui/config/texts.dart';

class ConnectionsList extends StatefulWidget {
  final void Function(ConnectionItemAction, SpikeConnection) onActionTriggered;
  const ConnectionsList({
    required this.onActionTriggered,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ConnectionsListState();
}

class _ConnectionsListState extends State<ConnectionsList> {
  late Future<void> _connectionsFuture;

  @override
  void initState() {
    super.initState();

    final connections = Provider.of<ConnectionsProvider>(
      context,
      listen: false,
    );
    _connectionsFuture = Future.microtask(connections.loadConnections);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (_, state) {
        if (state.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        return Consumer<ConnectionsProvider>(builder: (ctx, provider, _) {
          final connections = provider.availableConnections;
          if (connections.isEmpty) {
            return const Text(Texts.noConnectionsAvailable);
          }

          return ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (itemCtx, index) {
              final connection = connections[index];
              return ConnectionItem(
                connection: connection,
                onActionTriggered: (action) => widget.onActionTriggered(
                  action,
                  connection,
                ),
              );
            },
            separatorBuilder: (itemCtx, index) {
              return const Divider();
            },
            itemCount: connections.length,
          );
        });
      },
      future: _connectionsFuture,
    );
  }
}
