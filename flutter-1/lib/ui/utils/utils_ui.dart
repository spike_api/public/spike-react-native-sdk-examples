import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/responsive_sheet.dart';
import 'package:spike_sdk_test_app/ui/config/texts.dart';

abstract class UtilsUi {
  static void showAlertDialog(
    BuildContext context, {
    required String title,
    required String message,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: [
          TextButton(
            child: const Text(Texts.ok),
            onPressed: () => Navigator.of(ctx).pop(),
          ),
        ],
      ),
    );
  }

  static Future<T?> showSheet<T>({
    required BuildContext context,
    required WidgetBuilder builder,
  }) =>
      showModalBottomSheet<T>(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (ctx) => ResponsiveSheet(
          child: builder(ctx),
        ),
      );
}
