import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/info_label.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/spike_no_validation_form.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/error_box_consumer.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/spike_data_type_multi_choice_field.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/background_delivery_configuration/background_delivery_configuration_form_controller.dart';
import 'package:spike_sdk_test_app/ui/layout/layout_spacer.dart';

class BackgroundDeliveryConfigurationForm extends StatefulWidget {
  final SpikeConnection connection;

  const BackgroundDeliveryConfigurationForm({
    required this.connection,
    super.key,
  });

  @override
  State<StatefulWidget> createState() =>
      _BackgroundDeliveryConfigurationFormState();
}

class _BackgroundDeliveryConfigurationFormState
    extends State<BackgroundDeliveryConfigurationForm> {
  late BackgroundDeliveryConfigurationFormController _controller;

  @override
  void initState() {
    super.initState();

    _controller = BackgroundDeliveryConfigurationFormController();
  }

  @override
  Widget build(BuildContext context) {
    return SpikeNoValidationForm<BackgroundDeliveryConfigurationForm,
        BackgroundDeliveryConfigurationFormController>(
      parent: widget,
      formTitle: Texts.backgroundDelivery,
      controller: _controller,
      actionTitle: Texts.saveChanges,
      errorBuilder: (ctx) => ErrorBoxConsumer<ConnectionsProvider>(
        errorResolver: (p) => p.errorMessage,
      ),
      builder: (ctrl, ctx) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InfoLabel(
              textWidth: 256.0,
              text: ctrl.selectedDataTypes.isEmpty
                  ? Texts.bgDeliveryDisabled
                  : Texts.bgDeliveryEnabled,
              color: ctrl.selectedDataTypes.isEmpty ? Colors.red : Colors.green,
            ),
            if (ctrl.selectedDataTypes.isNotEmpty)
              TextButton(
                onPressed: ctrl.onDisableBackgroundDelivery,
                child: const Text(Texts.disableBgDelivery),
              ),
            LayoutSpacer.vertical(Spacings.s6),
            const Text(
              Texts.observedDataTypes,
              style: TextStyle(
                fontSize: FontSize.header,
                fontWeight: FontWeight.w500,
              ),
            ),
            LayoutSpacer.vertical(Spacings.s2),
            SpikeDataTypeMultiChoiceField(
              onChanged: ctrl.onSpikeDataTypeSelected,
              initialValues: ctrl.selectedDataTypes,
            ),
          ],
        );
      },
    );
  }
}
