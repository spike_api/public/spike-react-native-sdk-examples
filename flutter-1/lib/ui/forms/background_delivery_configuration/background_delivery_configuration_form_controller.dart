import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/controllers/spike_form_controller.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/config/texts.dart';
import 'package:spike_sdk_test_app/ui/forms/background_delivery_configuration/background_delivery_configuration_form.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';

class BackgroundDeliveryConfigurationFormController
    extends SpikeFormController<BackgroundDeliveryConfigurationForm> {
  late ConnectionsProvider _connections;
  late List<SpikeDataType> selectedDataTypes;

  @override
  Future<void> initialize(
    final BuildContext context,
    final BackgroundDeliveryConfigurationForm widget,
  ) async {
    await super.initialize(context, widget);

    if (!state.mounted) {
      return;
    }

    _connections = Provider.of<ConnectionsProvider>(context, listen: false);
    selectedDataTypes = await _connections.getCurrentTypes(
      connection: widget.connection,
    );

    _connections.clearErrors();
  }

  @override
  Future<void> save(final FormState? formState) async {
    if (formState?.validate() != true) {
      return;
    }

    enableLoading();

    final result = await _connections.setBackgroundDeliveryTypes(
      connection: widget.connection,
      types: selectedDataTypes,
    );

    if (!state.mounted) {
      return;
    }

    if (result == OperationResultType.success) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.bgDeliveryConfigChangedTitle,
        message: Texts.bgDeliveryConfigChangedText,
      );
    }

    disableLoading();
  }

  void onSpikeDataTypeSelected(final List<SpikeDataType> types) =>
      updateState(() {
        selectedDataTypes = types;
      });

  Future<void> onDisableBackgroundDelivery() async {
    enableLoading();

    final result = await _connections.disableBackgroundDelivery(
      connection: widget.connection,
    );

    if (!state.mounted) {
      return;
    }

    if (result == OperationResultType.success) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.bgDeliveryDisabledTitle,
        message: Texts.bgDeliveryDisabledText,
      );
    }

    disableLoading();
  }
}
