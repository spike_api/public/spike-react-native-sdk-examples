import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/spike_form.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/error_box_consumer.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/create_connection/create_connection_form_controller.dart';
import 'package:spike_sdk_test_app/ui/forms/create_connection/create_connection_form_validator.dart';
import 'package:spike_sdk_test_app/ui/layout/layout_spacer.dart';

class CreateConnectionForm extends StatefulWidget {
  const CreateConnectionForm({super.key});

  @override
  State<StatefulWidget> createState() => _CreateConnectionFormState();
}

class _CreateConnectionFormState extends State<CreateConnectionForm> {
  late CreateConnectionFormController _controller;
  late CreateConnectionFormValidator _validator;

  @override
  void initState() {
    super.initState();
    _controller = CreateConnectionFormController();
    _validator = CreateConnectionFormValidator();
  }

  @override
  Widget build(BuildContext context) {
    return SpikeForm<CreateConnectionForm, CreateConnectionFormController,
        CreateConnectionFormValidator>(
      parent: widget,
      formTitle: Texts.createConnection,
      controller: _controller,
      validator: _validator,
      errorBuilder: (ctx) => ErrorBoxConsumer<ConnectionsProvider>(
        errorResolver: (p) => p.errorMessage,
      ),
      builder: (ctrl, validator, ctx) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: TextButton.icon(
                onPressed: _controller.loadCredentials,
                icon: const Icon(Icons.lock),
                label: const Text(Texts.loadCredentials),
              ),
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.connectionsAppIdLabel,
              ),
              controller: ctrl.appIDController,
              validator: validator.validateAppID,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.authTokenLabel,
              ),
              controller: ctrl.authTokenController,
              validator: validator.validateAuthToken,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.customerEndUserIDLabel,
              ),
              controller: ctrl.customerEndUserIDController,
              validator: validator.validateCustomerEndUserID,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: Texts.postbackURLLabel,
                    ),
                    controller: ctrl.postbackURLController,
                  ),
                ),
                LayoutSpacer.horizontal(Spacings.s1),
                Tooltip(
                  message: Texts.generateWebHookEndpointUrl,
                  child: IconButton(
                    onPressed:
                        ctrl.isLoading ? null : () => ctrl.generateWebHookUrl(),
                    icon: const Icon(Icons.refresh),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
