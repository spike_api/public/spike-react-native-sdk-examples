import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_sdk_test_app/data/controllers/spike_form_controller.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/data/repositories/webhook_url_repository.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/create_connection/create_connection_form.dart';
import 'package:spike_sdk_test_app/ui/forms/load_credentials/load_credentials_form.dart';
import 'package:spike_sdk_test_app/ui/forms/load_credentials/load_credentials_form_callback.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';
import 'package:uuid/uuid.dart';

class CreateConnectionFormController
    extends SpikeFormController<CreateConnectionForm> {
  static const _uuid = Uuid();
  late ConnectionsProvider _connections;
  final appIDController = TextEditingController();
  final authTokenController = TextEditingController();
  final customerEndUserIDController = TextEditingController();
  final postbackURLController = TextEditingController();

  @override
  Future<void> initialize(
    final BuildContext context,
    final CreateConnectionForm widget,
  ) async {
    await super.initialize(context, widget);

    if (!state.mounted) {
      return;
    }

    _connections = Provider.of<ConnectionsProvider>(context, listen: false);
    _connections.clearErrors();
    appIDController.text = '';
    authTokenController.text = '';
    customerEndUserIDController.text = _uuid.v4();
    postbackURLController.text = '';
  }

  @override
  Future<void> save(final FormState? formState) async {
    if (formState?.validate() != true) {
      return;
    }

    enableLoading();

    final postbackURL = postbackURLController.text.isNotEmpty
        ? postbackURLController.text
        : null;
    final result = await _connections.connect(
      appId: appIDController.text,
      authToken: authTokenController.text,
      customerEndUserId: customerEndUserIDController.text,
      postbackURL: postbackURL,
    );

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (result == OperationResultType.success) {
      Navigator.of(context).pop();
    }
  }

  Future<void> generateWebHookUrl() async {
    if (postbackURLController.text.trim().isNotEmpty) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.webhookAlreadySetTitle,
        message: Texts.webhookAlreadySetText,
      );
      return;
    }

    enableLoading();

    final url = await WebhookUrlRepository().generateWebHookUrl();

    if (!state.mounted) {
      return;
    }

    if (url == null) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.webhookUrlRetrievalFailedTitle,
        message: Texts.webhookUrlRetrievalFailedText,
      );
    } else {
      updateState(() {
        postbackURLController.text = url;
      });
    }

    disableLoading();
  }

  void loadCredentials() {
    final callback = LoadCredentialsFormCallback(
      onDataReceived: (data) => updateState(
        () {
          appIDController.text = data.appId;
          authTokenController.text = data.authToken;
        },
      ),
    );

    UtilsUi.showSheet(
      context: context,
      builder: (ctx) => LoadCredentialsForm(
        callback: callback,
      ),
    );
  }
}
