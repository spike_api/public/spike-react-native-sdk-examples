import 'package:spike_sdk_test_app/data/validators/spike_form_validator.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class CreateConnectionFormValidator extends SpikeFormValidator {
  String? validateHost(final String? value) => checkIfNotEmpty(
        value,
        errorMessage: Texts.validationHostEmpty,
      );

  String? validateAppID(final String? value) => checkIfNotEmpty(
        value,
        errorMessage: Texts.validationAppIdEmpty,
      );

  String? validateAuthToken(final String? value) => checkIfNotEmpty(
        value,
        errorMessage: Texts.validationAuthTokenEmpty,
      );

  String? validateCustomerEndUserID(final String? value) => checkIfNotEmpty(
        value,
        errorMessage: Texts.validationCustomerEndUserIdEmpty,
      );
}
