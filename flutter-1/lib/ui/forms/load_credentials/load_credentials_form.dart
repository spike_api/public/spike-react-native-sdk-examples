import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/spike_no_validation_form.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/error_box_consumer.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/load_credentials/load_credentials_form_callback.dart';
import 'package:spike_sdk_test_app/ui/forms/load_credentials/load_credentials_form_controller.dart';

class LoadCredentialsForm extends StatefulWidget {
  final LoadCredentialsFormCallback callback;

  const LoadCredentialsForm({
    required this.callback,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LoadCredentialsFormState();
}

class _LoadCredentialsFormState extends State<LoadCredentialsForm> {
  late LoadCredentialsFormController _controller;

  @override
  void initState() {
    super.initState();

    _controller = LoadCredentialsFormController();
  }

  @override
  Widget build(BuildContext context) {
    return SpikeNoValidationForm<LoadCredentialsForm,
        LoadCredentialsFormController>(
      parent: widget,
      formTitle: Texts.loadCredentials,
      actionTitle: Texts.loadCredentials,
      controller: _controller,
      errorBuilder: (ctx) => ErrorBoxConsumer<ConnectionsProvider>(
        errorResolver: (p) => p.errorMessage,
      ),
      builder: (ctrl, ctx) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.decryptionKey,
              ),
              controller: ctrl.keyController,
            ),
          ],
        );
      },
    );
  }
}
