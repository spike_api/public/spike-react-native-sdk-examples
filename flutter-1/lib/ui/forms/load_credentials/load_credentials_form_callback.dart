import 'package:spike_sdk_test_app/data/model/atoms/connection_credentials.dart';

class LoadCredentialsFormCallback {
  final void Function(ConnectionCredentials) onDataReceived;

  const LoadCredentialsFormCallback({
    required this.onDataReceived,
  });
}
