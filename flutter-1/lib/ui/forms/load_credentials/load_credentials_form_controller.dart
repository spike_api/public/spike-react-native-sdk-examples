import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_sdk_test_app/data/controllers/spike_form_controller.dart';
import 'package:spike_sdk_test_app/data/providers/credentials_provider.dart';
import 'package:spike_sdk_test_app/ui/forms/load_credentials/load_credentials_form.dart';

class LoadCredentialsFormController
    extends SpikeFormController<LoadCredentialsForm> {
  late CredentialsProvider _credentials;
  final keyController = TextEditingController();

  @override
  Future<void> initialize(
    final BuildContext context,
    final LoadCredentialsForm widget,
  ) async {
    await super.initialize(context, widget);

    if (!state.mounted) {
      return;
    }

    _credentials = Provider.of<CredentialsProvider>(context, listen: false);
  }

  @override
  Future<void> save(final FormState? formState) async {
    if (formState?.validate() != true) {
      return;
    }

    enableLoading();

    final result = await _credentials.loadCredentials(keyController.text);

    if (!state.mounted) {
      return;
    }

    disableLoading();

    if (result != null) {
      Navigator.of(context).pop();
      widget.callback.onDataReceived(result);
    }
  }
}
