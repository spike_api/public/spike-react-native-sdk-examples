import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/connection/spike_connection.dart';
import 'package:spike_sdk_test_app/data/model/atoms/data_mode.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/nice_checkbox.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/nice_radio.dart';
import 'package:spike_sdk_test_app/ui/components/atoms/spike_form.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/error_box_consumer.dart';
import 'package:spike_sdk_test_app/ui/components/molecules/spike_data_type_choice_field.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/manage_data/manage_data_form_controller.dart';
import 'package:spike_sdk_test_app/ui/forms/manage_data/manage_data_form_validator.dart';

class ManageDataForm extends StatefulWidget {
  final SpikeConnection connection;

  const ManageDataForm({
    required this.connection,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ManageDataFormState();
}

class _ManageDataFormState extends State<ManageDataForm> {
  late ManageDataFormController _controller;
  late ManageDataFormValidator _validator;

  @override
  void initState() {
    super.initState();

    _controller = ManageDataFormController();
    _validator = ManageDataFormValidator();
  }

  @override
  Widget build(BuildContext context) {
    return SpikeForm<ManageDataForm, ManageDataFormController,
        ManageDataFormValidator>(
      parent: widget,
      formTitle: Texts.getData,
      controller: _controller,
      validator: _validator,
      actionTitle: Texts.getData,
      errorBuilder: (ctx) => ErrorBoxConsumer<ConnectionsProvider>(
        errorResolver: (p) => p.errorMessage,
      ),
      builder: (ctrl, validator, ctx) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              Texts.dataMode,
              style: TextStyle(
                fontSize: FontSize.title,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: WidgetSize.s2),
            NiceRadio(
              title: Texts.extractDataMode,
              checked: _controller.mode == DataMode.extractData,
              onTriggered: _controller.setExtractDataMode,
            ),
            const SizedBox(width: WidgetSize.s1),
            NiceRadio(
              title: Texts.extractAndPostDataMode,
              checked: _controller.mode == DataMode.extractAndPostData,
              onTriggered: _controller.setExtractAndPostDataMode,
              disabled: widget.connection.callbackUrl == null,
            ),
            const SizedBox(height: WidgetSize.s6),
            NiceCheckbox(
              title: Texts.ignoreDateRange,
              checked: _controller.ignoreDateRange,
              onTriggered: _controller.handleDateRange,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.startDate,
              ),
              controller: ctrl.startDateController,
              validator: validator.validateStartDate,
              enabled: !ctrl.ignoreDateRange,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: Texts.endDate,
              ),
              controller: ctrl.endDateController,
              validator: (value) => validator.validateEndDate(
                ctrl.startDateController.text,
                value,
              ),
              enabled: !ctrl.ignoreDateRange,
            ),
            const SizedBox(height: Spacings.s2),
            SpikeDataTypeChoiceField(
              onChanged: ctrl.onSpikeDataTypeSelected,
              initialValue: ctrl.selectedDataType,
            ),
          ],
        );
      },
    );
  }
}
