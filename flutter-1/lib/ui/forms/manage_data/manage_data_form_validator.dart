import 'package:spike_sdk_test_app/data/validators/spike_form_validator.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';

class ManageDataFormValidator extends SpikeFormValidator {
  static final dateRegex = RegExp(r'[0-9]{4}-[0-9]{2}-[0-9]{2}');

  String? validateStartDate(final String? value) {
    final date = DateTime.tryParse(value ?? '');
    if (value == null || !dateRegex.hasMatch(value) || date == null) {
      return Texts.validationBadDateFormat;
    }

    return null;
  }

  String? validateEndDate(final String? startDateValue, final String? value) {
    final date = DateTime.tryParse(value ?? '');
    if (value == null || !dateRegex.hasMatch(value) || date == null) {
      return Texts.validationBadDateFormat;
    }

    final startDate = DateTime.tryParse(startDateValue ?? '');
    if (startDate != null && date.isBefore(startDate)) {
      return Texts.validationBadEndDate;
    }

    return null;
  }
}
