import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:spike_sdk_test_app/data/controllers/spike_form_controller.dart';
import 'package:spike_sdk_test_app/data/model/atoms/data_mode.dart';
import 'package:spike_sdk_test_app/data/model/atoms/operation_result_type.dart';
import 'package:spike_sdk_test_app/data/providers/connections_provider.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/forms/manage_data/manage_data_form.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';

class ManageDataFormController extends SpikeFormController<ManageDataForm> {
  late ConnectionsProvider _connections;
  final startDateController = TextEditingController();
  final endDateController = TextEditingController();
  late SpikeDataType selectedDataType;
  late bool ignoreDateRange;
  late DataMode mode;

  @override
  Future<void> initialize(
    final BuildContext context,
    final ManageDataForm widget,
  ) async {
    await super.initialize(context, widget);

    if (!state.mounted) {
      return;
    }

    _connections = Provider.of<ConnectionsProvider>(context, listen: false);
    _connections.clearErrors();

    final today = DateTime.now();
    final yesterday = today.subtract(const Duration(days: 1));
    startDateController.text =
        '${yesterday.year}-${yesterday.month.toString().padLeft(2, '0')}-${yesterday.day.toString().padLeft(2, '0')}';
    endDateController.text =
        '${today.year}-${today.month.toString().padLeft(2, '0')}-${today.day.toString().padLeft(2, '0')}';
    selectedDataType = SpikeDataType.activitiesSummary;
    ignoreDateRange = false;
    mode = widget.connection.callbackUrl != null
        ? DataMode.extractAndPostData
        : DataMode.extractData;
  }

  @override
  Future<void> save(final FormState? formState) async {
    if (formState?.validate() != true) {
      return;
    }

    enableLoading();

    final result = await _connections.getData(
      connection: widget.connection,
      startDate: DateTime.parse(startDateController.text),
      endDate: DateTime.parse(endDateController.text),
      dataType: selectedDataType,
      ignoreDateRange: ignoreDateRange,
      mode: mode,
    );

    if (!state.mounted) {
      return;
    }

    if (result == OperationResultType.success) {
      UtilsUi.showAlertDialog(
        context,
        title: Texts.dataRetrievedTitle,
        message: Texts.dataRetrievedText,
      );
    }

    disableLoading();
  }

  void onSpikeDataTypeSelected(SpikeDataType type) => updateState(() {
        selectedDataType = type;
      });

  void handleDateRange() => updateState(() {
        ignoreDateRange = !ignoreDateRange;
      });

  void setExtractDataMode() => updateState(() {
        mode = DataMode.extractData;
      });

  void setExtractAndPostDataMode() => updateState(() {
        mode = DataMode.extractAndPostData;
      });
}
