abstract class FontSize {
  static const normal = 12.0;
  static const title = 16.0;
  static const header = 18.0;
}
