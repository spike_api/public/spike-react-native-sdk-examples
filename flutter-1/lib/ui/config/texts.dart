import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';

abstract class Texts {
  static const activitiesSummary = 'Activities Summary';
  static const activitiesSummaryData = 'Activities Summary Data';
  static const activitiesSummaryDataLoading = 'Reading Activities Summary data';
  static const getSummaryDataAction = 'Read summary data';
  static const sendData = 'Read and send data';
  static const appTitle = 'Spike SDK Demo';
  static const yes = 'Yes';
  static const no = 'No';
  static const ok = 'OK';
  static const connectAppleHealthHeader = 'Connect Apple Health';
  static const connectedToAppleHealth = 'Connected';
  static const isConnectedToAppleHealth = 'Is connected';
  static const isNotConnectedToAppleHealth = 'Is not connected';
  static const connectingToAppleHealth = 'Connecting to Apple Health';
  static const home = 'Home';
  static const requests = 'Requests';
  static const settings = 'Settings';
  static const userIdHeader = 'User ID';
  static const userIdGeneratedHeader = 'User ID Generated';
  static const userIdGenerateInProgress = 'Generating user ID.';
  static const userIdNone = 'Not generated';
  static const generateNewUserId = 'Generate new user ID';
  static const welcome =
      'Welcome to the Fluter Spike SDK Test App. Please use the menu to trigger various functionalities.';
  static const callBackUrl = 'Callback URL';
  static const backendUrl = 'Backend URL';
  static const backgroundDelivery = 'Background delivery';
  static const save = 'Save';
  static const saved = 'Saved';
  static const connections = 'Connections';
  static const connectionsAppIdLabel = 'App ID';
  static const authTokenLabel = 'Auth token';
  static const customerEndUserIDLabel = 'Customer end user ID';
  static const postbackURLLabel = 'Postback URL';
  static const createConnection = 'Create connection';
  static const noConnectionsAvailable =
      'You have no connections available right now.';
  static const connectionsConnectionIdLabel = 'Connection ID';
  static const disconnect = 'Disconnect';
  static const webHookInfo = 'Web Hook';
  static const customerID = 'Customer ID';
  static const manageData = 'Manage data';
  static const startDate = 'Start date';
  static const endDate = 'End date';
  static const getData = 'Get data';
  static const bgDeliveryForWebHookConnectionsAvailableOnlyTitle =
      'Not available for simple connections';
  static const bgDeliveryForWebHookConnectionsAvailableOnlyText =
      'Background delivery configuration is not available for simple connections. It is available for WebHook connections only. In order to create such connection, please enter the Postback URL when creating the connection.';

  static String getName(SpikeDataType type) {
    switch (type) {
      case SpikeDataType.activitiesSummary:
        return 'Summary';
      case SpikeDataType.activitiesStream:
        return 'Workouts';
      case SpikeDataType.breathing:
        return 'Breathing';
      case SpikeDataType.body:
        return 'Body';
      case SpikeDataType.calories:
        return 'Calories';
      case SpikeDataType.distance:
        return 'Distance';
      case SpikeDataType.glucose:
        return 'Glucose';
      case SpikeDataType.heart:
        return 'Heart';
      case SpikeDataType.oxygenSaturation:
        return 'Oxygen';
      case SpikeDataType.sleep:
        return 'Sleep';
      case SpikeDataType.steps:
        return 'Steps';
      case SpikeDataType.stepsIntraday:
        return 'Steps (Intraday)';
      default:
        break;
    }

    return '';
  }

  static const dataType = 'Data Type';

  static const validationHostEmpty = 'Host can not be empty!';

  static const validationAppIdEmpty = 'App ID can not be empty!';

  static const validationAuthTokenEmpty = 'Auth token can not be empty!';

  static const validationCustomerEndUserIdEmpty =
      'Customer end user ID can not be empty!';

  static const validationBadDateFormat = 'Bad format. Accepted: YYYY-MM-DD.';

  static const validationBadEndDate =
      'Bad end date. It must be bigger than start date.';

  static const dataRetrievedTitle = 'Data retrieved';

  static const dataRetrievedText =
      'Data has been retrieved. Look into the logs to check the process of retrieval. Also, you can take a look into what data has been retrieved by clicking "View responses".';

  static const logs = 'Logs';

  static const viewData = 'View data';
  static const viewResponses = 'View responses';
  static const revokeAllPermissions = 'Revoke All Permissions';
  static const openWebHookURL = 'Open Web Hook URL';

  static const clearLogs = 'Clear logs';

  static const noDataInLog = 'No items available in the log.';
  static const noData = 'No items available.';

  static const copy = 'Copy';

  static const share = 'Share';

  static const copiedInfoTitle = 'Copied!';
  static const copiedInfoText = 'Data copied successfully!';

  static const logsShared = 'Spike SDK Demo App Logs Shared';

  static const logsSharedInfo =
      'These are the logs shared regarding Flutter SDK app.';

  static const missingPermissionsText = 'Permissions are missing!';

  static const noCallbackURL = 'No postback URL!';

  static const connectionsHostLabel = 'Backend URL';

  static const generateWebHookEndpointUrl = 'Generate Postback URL';

  static const webhookUrlRetrievalFailedTitle = 'Failed to retrieve the URL';

  static const webhookUrlRetrievalFailedText =
      'Postback URL retrieval has failed. Please check your network connection.';

  static const webhookAlreadySetTitle = 'Postback URL set';

  static const webhookAlreadySetText =
      'Postback URL has been set already. Do not overuse the API, and use the one that was generated for you.';

  static const bgDeliveryConfigChangedTitle =
      'Background delivery configuration changed';

  static const bgDeliveryConfigChangedText =
      'Background delivery settings have been changed successfully.';

  static const saveChanges = 'Save changes';

  static const bgDeliveryDisabled = 'Background delivery disabled';

  static const bgDeliveryEnabled = 'Background delivery enabled';

  static const observedDataTypes = 'Data types observed';

  static const bgDeliveryAndroidNoSupportTitle = 'No support';

  static const bgDeliveryAndroidNoSupportText =
      'Android does not support background delivery.';

  static const copiedConfirmation = 'Copied!';

  static const ignoreDateRange = 'Ignore range of dates';

  static const dataMode = 'Your data action';
  static const extractDataMode = 'Extract data';
  static const extractAndPostDataMode = 'Extract and post data';

  static const connectionData = 'Retrieved Connection Data';

  static const duration = 'Duration';

  static const failedLaunchUrlTitle = 'Web Hook URL launch failed';
  static const failedLaunchUrlText = 'Web Hook URL launch failed. URL: {url}.';

  static const healthConnectAvailable = 'Health Service Available';

  static const generalYes = 'Yes';
  static const generalNo = 'No';
  static const generalResolving = 'Resolving...';

  static const loadCredentials = 'Load credentials';
  static const decryptionKey = 'Decryption key';
  static const revokedTitle = 'Revoked';
  static const revokedText = 'Revoke done.';

  static const bgDeliveryDisabledTitle = 'Background Delivery disabled';
  static const bgDeliveryDisabledText =
      'Background Delivery has been disabled successfully.';
  static const disableBgDelivery = 'Disable background delivery';
}
