/*
 * Tailwind notation used, e.g., https://tailwindcss.com/docs/gap .
 */
abstract class Spacings {
  static const s1 = 4.0;
  static const s2 = 8.0;
  static const s3 = 12.0;
  static const s6 = 24.0;
}
