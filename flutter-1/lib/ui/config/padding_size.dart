/*
 * Tailwind notation used, e.g., https://tailwindcss.com/docs/padding .
 */
abstract class PaddingSize {
  static const p1 = 4.0;
  static const p2 = 8.0;
  static const p3 = 12.0;
  static const p4 = 16.0;
}
