abstract class Routes {
  static const home = '/';
  static const connections = '/connections';
  static const logs = '/logs';
}
