import 'package:flutter/material.dart';
import 'package:spike_sdk_test_app/ui/config/padding_size.dart';

abstract class Paddings {
  static const p1 = EdgeInsets.all(PaddingSize.p1);

  static const p2 = EdgeInsets.all(PaddingSize.p2);

  static const p3 = EdgeInsets.all(PaddingSize.p3);

  static const p4 = EdgeInsets.all(PaddingSize.p4);
}
