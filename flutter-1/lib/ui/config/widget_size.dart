/*
 * Tailwind notation used, e.g., https://tailwindcss.com/docs/width except
 * instead of 'w', 's' is used.
 */
abstract class WidgetSize {
  static const s0_5 = 2.0;
  static const s1 = 4.0;
  static const s2 = 8.0;
  static const s4 = 16.0;
  static const s6 = 24.0;
  static const s12 = 48.0;
}
