import 'dart:convert';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share_plus/share_plus.dart';
import 'package:spike_sdk_test_app/data/model/atoms/connection_data_record.dart';
import 'package:spike_sdk_test_app/ui/config/config.dart';
import 'package:spike_sdk_test_app/ui/utils/utils_ui.dart';

class ConnectionRecordSheet extends StatefulWidget {
  final ConnectionDataRecord record;

  const ConnectionRecordSheet({
    required this.record,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ConnectionRecordSheetState();
}

class _ConnectionRecordSheetState extends State<ConnectionRecordSheet> {
  static const _encoder = JsonEncoder.withIndent('  ');
  late String _data;

  @override
  void initState() {
    super.initState();
    _data = _encoder.convert(widget.record.data);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: Paddings.p4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Wrap(
            spacing: Spacings.s2,
            children: [
              TextButton(
                onPressed: _copyData,
                child: const Text(Texts.copy),
              ),
              TextButton(
                onPressed: _shareData,
                child: const Text(Texts.share),
              ),
            ],
          ),
          Container(
            padding: Paddings.p2,
            decoration: BoxDecoration(
              color: Colors.blueGrey,
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: SelectableText(
              _data,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _copyData() async {
    Clipboard.setData(ClipboardData(text: _data));

    UtilsUi.showAlertDialog(
      context,
      title: Texts.copiedInfoTitle,
      message: Texts.copiedInfoText,
    );
  }

  Future<void> _shareData() async {
    final deviceInfoPlugin = DeviceInfoPlugin();
    final deviceInfo = await deviceInfoPlugin.deviceInfo;
    final allInfo = deviceInfo.data;

    var data = 'Device info:\r\n\r\n';
    data += _encoder.convert(allInfo);
    data += '\r\n\r\n\r\n';
    data += _data;

    Share.share(
      data,
      subject:
          'Data for ${Texts.getName(widget.record.type)} [${widget.record.start.toIso8601String()} / ${widget.record.end.toIso8601String()}]',
    );
  }
}
