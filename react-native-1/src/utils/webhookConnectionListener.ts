import type { SpikeBackgroundDeliveriesLogger } from 'react-native-spike-sdk';
import { addBackgroundLog } from './settingsStorage';

export class WebhookConnectionListener
  implements SpikeBackgroundDeliveriesLogger
{
  private connectionUUID: string;

  private logBuffer: string[] = [];
  private storeStarted: boolean = false;

  constructor(connectionUUID: string) {
    this.connectionUUID = connectionUUID;
  }

  onBackgroundLog(log: string) {
    this.saveLogs(log);
  }

  private saveLogs(log: string) {
    console.log(log);
    this.logBuffer.push(log);
    if (!this.storeStarted) {
      this.storeLog();
    }
  }

  private async storeLog() {
    this.storeStarted = true;
    if (this.logBuffer.length === 0) {
      this.storeStarted = false;
      return;
    }
    const log = this.logBuffer[0];
    this.logBuffer.shift();
    if (!!log) {
      await addBackgroundLog(this.connectionUUID, log);
    }
    await this.storeLog();
  }
}
