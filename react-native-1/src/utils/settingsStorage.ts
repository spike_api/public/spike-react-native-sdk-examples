import AsyncStorage from '@react-native-async-storage/async-storage';
import { uuidGenerator } from './utils';
import type { SpikeConnection } from 'react-native-spike-sdk';

const CONNECTIONS_KEY = 'app_spike_connections_key';
const CONFIG_KEY = 'app_spike_config_key';

type BaseSpikeConnection = {
  uuid: string;
  appId: string;
  authToken: string;
  customerEndUserId: string;
  callbackUrl?: string;
  isClosed?: boolean;
  createdAt: Date;
  logs: string[];
  backgroundLogs: string[];
  connection?: SpikeConnection;
};

export type AppBaseConnection = BaseSpikeConnection;

export const loadConnections = async (): Promise<AppBaseConnection[]> => {
  const connectionsString = await AsyncStorage.getItem(CONNECTIONS_KEY);
  if (!connectionsString) return [];
  const connections: AppBaseConnection[] = JSON.parse(connectionsString);
  const list = connections ?? [];
  const sorted = list
    .map((conn) => ({
      ...conn,
      backgroundLogs: conn.backgroundLogs ?? [],
    }))
    .sort(
      (a, b) =>
        new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
    )
    .sort((a, b) => (b.isClosed ? 0 : 1) - (a.isClosed ? 0 : 1));
  return sorted;
};

const saveConnections = async (connections: AppBaseConnection[]) => {
  const connectionsData = connections.map((item) => ({
    ...item,
    connection: undefined,
  }));
  const connectionsString = JSON.stringify(connectionsData);
  await AsyncStorage.setItem(CONNECTIONS_KEY, connectionsString);
};

export const generateConnection = async (
  appId: string,
  authToken: string,
  customerEndUserId: string,
  callbackUrl: string | undefined
): Promise<AppBaseConnection> => {
  const connection: AppBaseConnection = {
    uuid: uuidGenerator(),
    appId,
    authToken,
    customerEndUserId,
    callbackUrl,
    createdAt: new Date(),
    logs: [],
    backgroundLogs: [],
  };

  const connections = await loadConnections();
  connections.push(connection);
  await saveConnections(connections);

  return connection;
};

export const removeGeneratedConnection = async (
  connection: AppBaseConnection
): Promise<AppBaseConnection> => {
  const connections = await loadConnections();

  const filteredConnections = connections.filter(
    (conn) => conn.uuid !== connection.uuid
  );
  await saveConnections(filteredConnections);
  return connection;
};

export const setClosedConnection = async (
  connection: AppBaseConnection
): Promise<AppBaseConnection> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connection.uuid
  );

  if (!!foundConnection) {
    foundConnection.isClosed = true;
  }

  await saveConnections(connections);
  return foundConnection ?? connection;
};

export const addConnectionLog = async (
  connectionUUID: string,
  log: string
): Promise<void> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  if (!!foundConnection) {
    if (!foundConnection.logs) {
      foundConnection.logs = [];
    }
    foundConnection.logs.push(log);
  }

  await saveConnections(connections);
};

export const getConnectionLogs = async (
  connectionUUID: string
): Promise<string[]> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  return foundConnection?.logs ?? [];
};

export const clearConnectionLog = async (
  connectionUUID: string
): Promise<void> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  if (!!foundConnection) {
    foundConnection.logs = [];
  }

  await saveConnections(connections);
};

export const addBackgroundLog = async (
  connectionUUID: string,
  log: string
): Promise<void> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  if (!!foundConnection && 'backgroundLogs' in foundConnection) {
    if (!foundConnection.backgroundLogs) {
      foundConnection.backgroundLogs = [];
    }
    foundConnection.backgroundLogs.push(log);
  }

  await saveConnections(connections);
};

export const getBackgroundLogs = async (
  connectionUUID: string
): Promise<string[]> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  if (!!foundConnection && 'backgroundLogs' in foundConnection) {
    return foundConnection.backgroundLogs || [];
  }

  return [];
};

export const clearBackgroundLogs = async (
  connectionUUID: string
): Promise<void> => {
  const connections = await loadConnections();

  const foundConnection = connections.find(
    (conn) => conn.uuid === connectionUUID
  );

  if (!!foundConnection && 'backgroundLogs' in foundConnection) {
    foundConnection.backgroundLogs = [];
  }

  await saveConnections(connections);
};

export type MethodsConfig = {
  startDate?: Date;
  endDate?: Date;
  useDateRange: boolean;
  actionIndex: number;
};

export const defaultMethodsConfig: MethodsConfig = {
  useDateRange: false,
  actionIndex: 0,
};

export const getMethodsConfig = async (): Promise<MethodsConfig> => {
  const configString = await AsyncStorage.getItem(CONFIG_KEY);
  if (!configString) return defaultMethodsConfig;
  const configData = JSON.parse(configString);
  const config: MethodsConfig = {
    useDateRange: configData.useDateRange,
    actionIndex: configData.actionIndex,
    startDate: !!configData.startDateTime
      ? new Date(configData.startDateTime)
      : undefined,
    endDate: !!configData.endDateTime
      ? new Date(configData.endDateTime)
      : undefined,
  };
  return config;
};

export const saveMethodsConfig = async (
  config: MethodsConfig
): Promise<void> => {
  await AsyncStorage.setItem(
    CONFIG_KEY,
    JSON.stringify({
      ...config,
      startDateTime: !!config.startDate
        ? config.startDate.getTime()
        : undefined,
      endDateTime: !!config.endDate ? config.endDate.getTime() : undefined,
    })
  );
};
