import type { SpikeConnection, SpikeLogger } from 'react-native-spike-sdk';
import { addConnectionLog } from './settingsStorage';

export class ConnectionLogger implements SpikeLogger {
  private connectionUUID: string;

  constructor(connectionUUID: string) {
    this.connectionUUID = connectionUUID;
  }

  isDebugEnabled(): boolean {
    return true;
  }

  isInfoEnabled(): boolean {
    return true;
  }

  isErrorEnabled(): boolean {
    return true;
  }

  async debug(connection: SpikeConnection, message: String) {
    await this.saveLogs(`[SPIKE_DEBUG] ${message}`);
  }

  async info(connection: SpikeConnection, message: String) {
    await this.saveLogs(`[SPIKE_INFO] ${message}`);
  }

  async error(connection: SpikeConnection, message: String) {
    await this.saveLogs(`[SPIKE_ERROR] ${message}`);
  }

  private async saveLogs(log: string) {
    console.log(log);
    await addConnectionLog(this.connectionUUID, log);
  }
}
