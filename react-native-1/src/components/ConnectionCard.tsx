import { StyleSheet, Text, TextInput, View } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import {
  setClosedConnection,
  type AppBaseConnection,
} from '../utils/settingsStorage';
import AppButton from './AppButton';
import { useToast } from 'react-native-toast-notifications';
import { TEXT_COLOR } from '../utils/utils';

type Props = {
  baseConnection: AppBaseConnection;
  isListItem?: boolean;
  onSelect?: () => void;
  onClosed?: () => void;
  onRemove?: () => void;
};

const ConnectionCard = ({
  baseConnection,
  isListItem = false,
  onSelect,
  onClosed,
  onRemove,
}: Props) => {
  const toast = useToast();
  const [appId, setAppId] = useState<string>();
  const [authToken] = useState<string>(baseConnection.authToken);
  const [customerEndUserId, setCustomerEndUserId] = useState<string>();
  const [spikeEndUserId, setSpikeEndUserId] = useState<string>();
  const [callbackURL, setCallbackURL] = useState<string>();
  const [createAt, setCreatedAt] = useState<string>();

  const updateConnectionInfo = async () => {
    try {
      const appId = (await baseConnection.connection?.getAppId()) ?? '-';
      setAppId(appId);
      const customerEndUserId =
        (await baseConnection.connection?.getCustomerEndUserId()) ?? '-';
      setCustomerEndUserId(customerEndUserId);
      const spikeEndUserId =
        (await baseConnection.connection?.getSpikeEndUserId()) ?? '-';
      setSpikeEndUserId(spikeEndUserId);

      setCreatedAt(undefined);
    } catch (error) {
      toast.show(`${error}`);
    }

    try {
      const callbackUrl =
        (await baseConnection.connection?.getCallbackUrl()) ?? '-';
      setCallbackURL(callbackUrl);
    } catch (error) {
      setCallbackURL(undefined);
    }
  };

  const updateListConnectionInfo = () => {
    setAppId(baseConnection.appId);
    setCustomerEndUserId(baseConnection.customerEndUserId);
    setSpikeEndUserId(undefined);
    setCallbackURL(baseConnection.callbackUrl ?? undefined);
    setCreatedAt(`${baseConnection.createdAt}`);
  };

  const onCloseConnection = useCallback(async () => {
    try {
      await baseConnection.connection?.close();
      setClosedConnection(baseConnection);
      onClosed?.();
    } catch (error) {
      toast.show(`${error}`);
    }
  }, []);

  useEffect(() => {
    if (isListItem) {
      updateListConnectionInfo();
    } else {
      updateConnectionInfo();
    }
  }, [baseConnection, isListItem]);

  return (
    <View style={styles.connectionInfoContainer}>
      <Text style={styles.connectionInfoText}>App Id:</Text>
      <TextInput
        style={styles.connectionInfoValue}
        multiline={false}
        editable={false}
        scrollEnabled={false}
      >
        {appId ?? '...'}
      </TextInput>

      <Text style={styles.connectionInfoText}>Auth token:</Text>
      <TextInput
        style={styles.connectionInfoValue}
        multiline={false}
        editable={false}
        scrollEnabled={false}
      >
        {authToken}
      </TextInput>

      <Text style={styles.connectionInfoText}>Customer end user Id:</Text>
      <TextInput
        style={styles.connectionInfoValue}
        multiline={false}
        editable={false}
        scrollEnabled={false}
      >
        {customerEndUserId ?? '...'}
      </TextInput>

      {!!spikeEndUserId && (
        <>
          <Text style={styles.connectionInfoText}>Spike end user Id:</Text>
          <TextInput
            style={styles.connectionInfoValue}
            multiline={false}
            editable={false}
            scrollEnabled={false}
          >
            {spikeEndUserId}
          </TextInput>
        </>
      )}

      {!!callbackURL && (
        <>
          <Text style={styles.connectionInfoText}>Callback URL:</Text>
          <TextInput
            style={styles.connectionInfoValue}
            multiline={false}
            editable={false}
            scrollEnabled={false}
          >
            {callbackURL}
          </TextInput>
        </>
      )}

      {!!createAt && (
        <>
          <Text style={styles.connectionInfoText}>Created at:</Text>
          <TextInput
            style={styles.connectionInfoValue}
            multiline={false}
            editable={false}
            scrollEnabled={false}
          >
            {createAt}
          </TextInput>
        </>
      )}

      {!isListItem ? (
        <View style={styles.buttonContainer}>
          <AppButton
            style={styles.button}
            title="Close connection"
            onPress={onCloseConnection}
          />
        </View>
      ) : baseConnection.isClosed ? (
        <View style={styles.buttonContainer}>
          <AppButton
            style={[styles.button, { marginRight: 8 }]}
            type="bordered"
            title="Remove"
            onPress={onRemove}
          />
          <AppButton
            style={[styles.button, { marginLeft: 8 }]}
            title="Recreate connection"
            onPress={onSelect}
          />
        </View>
      ) : (
        <View style={styles.buttonContainer}>
          <AppButton
            style={[styles.button, { marginRight: 8 }]}
            type="bordered"
            title="Remove"
            onPress={onRemove}
          />
          <AppButton
            style={[styles.button, { marginLeft: 8 }]}
            title="Connect"
            onPress={onSelect}
          />
        </View>
      )}
    </View>
  );
};

export default ConnectionCard;

const styles = StyleSheet.create({
  connectionInfoContainer: {
    borderWidth: 1,
    borderColor: 'rgba(0, 0 ,0, 0.2)',
    borderRadius: 12,
    padding: 12,
  },
  connectionInfoText: {
    fontSize: 14,
    fontWeight: '600',
    color: TEXT_COLOR,
  },
  connectionModeText: {
    fontSize: 16,
    fontWeight: '600',
    color: TEXT_COLOR,
    marginBottom: 12,
  },
  connectionInfoValue: {
    fontSize: 13,
    marginBottom: 8,
    color: TEXT_COLOR,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
  },
});
