import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import React from 'react';
import { PRIMARY_COLOR } from '../utils/utils';

type Props = {
  title: string;
  onPress?: () => void;
  type?: 'text' | 'filled' | 'bordered';
  style?: StyleProp<ViewStyle>;
  disabled?: boolean;
  capitalizeWords?: boolean;
};

const AppButton = ({
  title,
  onPress,
  type = 'filled',
  style,
  disabled,
  capitalizeWords,
}: Props) => {
  return (
    <TouchableOpacity
      style={[
        type === 'filled' && styles.filledButton,
        type === 'bordered' && styles.borderedButton,
        style,
      ]}
      onPress={onPress}
      disabled={disabled}
    >
      <Text
        style={[
          styles.buttonText,
          { color: type === 'filled' ? 'white' : PRIMARY_COLOR },
          { textAlign: type === 'text' ? 'left' : 'center' },
          capitalizeWords && { textTransform: 'capitalize' },
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default AppButton;

const styles = StyleSheet.create({
  filledButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 6,
    paddingHorizontal: 8,
    minHeight: 40,
    backgroundColor: PRIMARY_COLOR,
    borderRadius: 4,
  },
  borderedButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 6,
    paddingHorizontal: 8,
    minHeight: 40,
    borderRadius: 4,
    borderColor: PRIMARY_COLOR,
    borderWidth: 2,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '400',
  },
});
