import { ScrollView, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import {
  AppBaseConnection,
  loadConnections,
  removeGeneratedConnection,
} from '../utils/settingsStorage';
import ConnectionCard from './ConnectionCard';
import AppButton from './AppButton';
import Spike from 'react-native-spike-sdk';
import { ConnectionLogger } from '../utils/connectionLogger';
import { useToast } from 'react-native-toast-notifications';
import { TEXT_COLOR } from '../utils/utils';
import { WebhookConnectionListener } from '../utils/webhookConnectionListener';

type Props = {
  onConnect?: (connection: AppBaseConnection) => void;
  onCreate?: () => void;
  setIsLoading: (isLoading: boolean) => void;
};

const Connect = ({ onConnect, onCreate, setIsLoading }: Props) => {
  const toast = useToast();
  const [connections, setConnections] = useState<AppBaseConnection[]>([]);

  const preload = async () => {
    setIsLoading(true);
    const connections = await loadConnections();
    setConnections(connections);
    setIsLoading(false);
  };

  const onUnpackConnection = async (connection: AppBaseConnection) => {
    if (!!connection.connection) {
      onConnect?.(connection);
      return;
    }

    setIsLoading(true);
    try {
      const logger = new ConnectionLogger(connection.uuid);
      const spikeConnection = await Spike.createConnection(
        {
          appId: connection.appId,
          authToken: connection.authToken,
          customerEndUserId: connection.customerEndUserId,
          callbackUrl: connection.callbackUrl,
        },
        logger
      );
      connection.connection = spikeConnection;
      if (!!connection.callbackUrl) {
        await connection.connection!.setListener(
          new WebhookConnectionListener(connection.uuid)
        );
      }
      onConnect?.(connection);
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  const onRemoveConnection = async (connection: AppBaseConnection) => {
    setIsLoading(true);
    if (!!connection.connection) {
      await connection.connection.close();
      await removeGeneratedConnection(connection);
      setIsLoading(false);
      return;
    }

    try {
      const spikeConnection = await Spike.createConnection({
        appId: connection.appId,
        authToken: connection.authToken,
        customerEndUserId: connection.customerEndUserId,
        callbackUrl: connection.callbackUrl,
      });
      await spikeConnection.close();
      await removeGeneratedConnection(connection);
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    preload();
  }, []);

  return (
    <>
      <View style={styles.header}>
        <Text style={styles.title}>Connections</Text>
      </View>
      <View style={styles.contentContainer}>
        <AppButton title="Create new connection" onPress={onCreate} />
        <ScrollView style={styles.connectionList}>
          {connections.map((connection) => (
            <View key={connection.uuid} style={styles.cardWrapper}>
              <ConnectionCard
                baseConnection={connection}
                isListItem={true}
                onSelect={() => onUnpackConnection(connection)}
                onRemove={() => onRemoveConnection(connection)}
              />
            </View>
          ))}
        </ScrollView>
      </View>
    </>
  );
};

export default Connect;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 16,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
  },
  connectionList: {
    marginTop: 16,
  },
  cardWrapper: {
    marginBottom: 16,
  },
});
