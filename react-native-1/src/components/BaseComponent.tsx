import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Connect from './Connect';
import CreateConnection from './CreateConnection';
import ConnectionView from './ConnectionView';
import type { AppBaseConnection } from '../utils/settingsStorage';
import ConnectionInfo from './ConnectionInfo';
import DataOutput from './DataOutput';
import Spike from 'react-native-spike-sdk';
import AndroidPermissions from './AndroidPermissions';
import AndroidMultiplePermissions from './AndroidMultiplePermissions';

type Screens =
  | 'create_connection'
  | 'info'
  | 'android_permissions'
  | 'android_multiple_permissions';

const BaseComponent = () => {
  const [connection, setConnection] = useState<AppBaseConnection>();
  const [screen, setScreen] = useState<Screens>();
  const [isLoading, setIsLoading] = useState(false);
  const [dataOutput, setDataOutput] = useState<{
    title: string;
    data: string;
  }>();
  const [isHealthDataAvailable, setIsHealthDataAvailable] = useState(true);

  const checkHealthDataPackage = async () => {
    if (Platform.OS === 'android') {
      try {
        const isPackageInstalled = await Spike.isPackageInstalled();
        if (!isPackageInstalled) {
          console.log('Health Connect is not available on this device.');
          setIsHealthDataAvailable(false);
        }
      } catch (error) {
        console.log(`${error}`);
      }
    }
    if (Platform.OS === 'ios') {
      try {
        const isAvailable = await Spike.isHealthDataAvailable();
        if (!isAvailable) {
          console.log('Health Store data is not available on this device.');
          setIsHealthDataAvailable(false);
        }
      } catch (error) {
        console.log(`${error}`);
      }
    }
  };

  useEffect(() => {
    checkHealthDataPackage();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {isLoading && (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color={'white'} />
        </View>
      )}
      {!isHealthDataAvailable && (
        <Text style={styles.notAvailable}>
          {Platform.OS === 'android'
            ? 'Health Connect is not available'
            : 'Health Store is not available'}
        </Text>
      )}
      {!!dataOutput ? (
        <DataOutput
          onBack={() => {
            setDataOutput(undefined);
          }}
          title={dataOutput.title}
          data={dataOutput.data}
        />
      ) : !connection && !screen ? (
        <Connect
          onConnect={(connection) => {
            setConnection(connection);
            setScreen(undefined);
          }}
          onCreate={() => setScreen('create_connection')}
          setIsLoading={setIsLoading}
        />
      ) : !!connection && screen === 'info' ? (
        <ConnectionInfo
          baseConnection={connection}
          onBack={() => {
            setScreen(undefined);
          }}
          onConnectionClosed={() => {
            setScreen(undefined);
            setConnection(undefined);
          }}
          onShowData={(title, data) => setDataOutput({ title, data })}
          setIsLoading={setIsLoading}
        />
      ) : !!connection && screen === 'android_permissions' ? (
        <AndroidPermissions
          baseConnection={connection}
          onBack={() => {
            setScreen(undefined);
          }}
          manageMultiplePermissions={() => {
            setScreen('android_multiple_permissions');
          }}
          setIsLoading={setIsLoading}
        />
      ) : !!connection && screen === 'android_multiple_permissions' ? (
        <AndroidMultiplePermissions
          baseConnection={connection}
          onBack={() => {
            setScreen('android_permissions');
          }}
          setIsLoading={setIsLoading}
        />
      ) : screen === 'create_connection' ? (
        <CreateConnection
          onCreated={(connection) => {
            setConnection(connection);
            setScreen(undefined);
          }}
          onBack={() => {
            setScreen(undefined);
            setConnection(undefined);
          }}
          setIsLoading={setIsLoading}
        />
      ) : !!connection ? (
        <ConnectionView
          baseConnection={connection}
          onBack={() => {
            setScreen(undefined);
            setConnection(undefined);
          }}
          onInfo={() => setScreen('info')}
          onAndroidPermissions={() => setScreen('android_permissions')}
          onShowData={(title, data) => setDataOutput({ title, data })}
          setIsLoading={setIsLoading}
        />
      ) : null}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0 ,0, 0.7)',
  },
  notAvailable: {
    color: 'red',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 12,
  },
});

export default BaseComponent;
