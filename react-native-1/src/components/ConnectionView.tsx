import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  View,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { PRIMARY_COLOR, TEXT_COLOR } from '../utils/utils';
import {
  getMethodsConfig,
  type AppBaseConnection,
  MethodsConfig,
  defaultMethodsConfig,
  saveMethodsConfig,
} from '../utils/settingsStorage';
import AppButton from './AppButton';
import { SpikeDataType, SpikeDataTypes } from 'react-native-spike-sdk';
import { useToast } from 'react-native-toast-notifications';
import SegmentedControl from '@react-native-segmented-control/segmented-control';
import DatePicker from 'react-native-date-picker';

type Props = {
  baseConnection: AppBaseConnection;
  onBack: () => void;
  onInfo: () => void;
  onAndroidPermissions: () => void;
  onShowData: (title: string, data: string) => void;
  setIsLoading: (isLoading: boolean) => void;
};

const dataTypesOptions = [
  { type: SpikeDataTypes.activitiesStream },
  { type: SpikeDataTypes.activitiesSummary },
  { type: SpikeDataTypes.breathing },
  { type: SpikeDataTypes.calories },
  { type: SpikeDataTypes.distance },
  { type: SpikeDataTypes.glucose },
  { type: SpikeDataTypes.heart },
  { type: SpikeDataTypes.oxygenSaturation },
  { type: SpikeDataTypes.sleep },
  { type: SpikeDataTypes.steps },
  { type: SpikeDataTypes.stepsIntraday },
  { type: SpikeDataTypes.body },
];

const ConnectionView = ({
  baseConnection,
  onBack,
  onInfo,
  onAndroidPermissions,
  onShowData,
  setIsLoading,
}: Props) => {
  const toast = useToast();
  const [config, setConfig] = useState<MethodsConfig>(defaultMethodsConfig);
  const [showDatePicker, setShowDatePicker] = useState<'start' | 'end'>();

  const webhookConnection =
    'callbackUrl' in baseConnection ? baseConnection.connection : undefined;

  const preloadMethodsConfig = async () => {
    const newConfig = await getMethodsConfig();
    setConfig({
      ...newConfig,
      actionIndex: !!webhookConnection ? newConfig.actionIndex : 0,
    });
  };

  const updateConfig = async (newConfig: MethodsConfig) => {
    setConfig(newConfig);
    await saveMethodsConfig(newConfig);
  };

  useEffect(() => {
    preloadMethodsConfig();
  }, []);

  const manageAndroidPermissions = async () => {
    onAndroidPermissions();
  };

  const manageiOSPermissions = async () => {
    toast.show('Go to Settings -> Privacy -> Health -> Your App', {
      duration: 6000,
    });
  };

  const extractData = async (type: SpikeDataType) => {
    if (!baseConnection.connection) return;

    setIsLoading(true);
    try {
      if (config.actionIndex === 0) {
        if (config.useDateRange) {
          if (config.startDate && config.endDate) {
            const data = await baseConnection.connection.extractData({
              dataType: type,
              from: config.startDate,
              to: config.endDate,
            });
            onShowData(
              type.rawValue.replaceAll('"', '').replaceAll('_', ' '),
              JSON.stringify(data, null, 2)
            );
          } else {
            toast.show('Select start and end dates');
          }
        } else {
          const data = await baseConnection.connection.extractData({
            dataType: type,
          });
          onShowData(
            type.rawValue.replaceAll('"', '').replaceAll('_', ' '),
            JSON.stringify(data, null, 2)
          );
        }
      } else if ('callbackUrl' in baseConnection) {
        if (config.useDateRange) {
          if (config.startDate && config.endDate) {
            const data = await baseConnection.connection.extractAndPostData({
              dataType: type,
              from: config.startDate,
              to: config.endDate,
            });
            onShowData(
              type.rawValue.replaceAll('"', '').replaceAll('_', ' '),
              JSON.stringify(data, null, 2)
            );
          } else {
            toast.show('Select start and end dates');
          }
        } else {
          const data = await baseConnection.connection.extractAndPostData({
            dataType: type,
          });
          onShowData(
            type.rawValue.replaceAll('"', '').replaceAll('_', ' '),
            JSON.stringify(data, null, 2)
          );
        }
      }
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  return (
    <>
      <View style={styles.header}>
        <AppButton title="Select connection" onPress={onBack} type="text" />
        <AppButton title="Settings" onPress={onInfo} type="text" />
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.permissionsContainer}>
          {Platform.OS === 'android' && (
            <AppButton
              title={'Manage permission'}
              onPress={manageAndroidPermissions}
            />
          )}
          {Platform.OS === 'ios' && (
            <AppButton
              title={'Manage permission'}
              onPress={manageiOSPermissions}
            />
          )}

          <View style={styles.horizontalLine} />
        </View>
        <View style={styles.controlWrapper}>
          <AppButton
            title={
              !!config.startDate
                ? `Start Date: ${config.startDate.toDateString()}`
                : 'Select start date'
            }
            onPress={() => setShowDatePicker('start')}
            type="text"
          />

          <AppButton
            title={
              !!config.endDate
                ? `End Date: ${config.endDate.toDateString()}`
                : 'Select end date'
            }
            onPress={() => setShowDatePicker('end')}
            type="text"
            style={{ marginTop: 12 }}
          />

          <View style={styles.switchContainer}>
            <Text style={styles.buttonText}>Use date range</Text>
            <Switch
              trackColor={{ true: PRIMARY_COLOR }}
              onValueChange={(value) =>
                updateConfig({ ...config, useDateRange: value })
              }
              value={config.useDateRange}
            />
          </View>
        </View>
        {!!webhookConnection && (
          <View style={styles.controlWrapper}>
            <SegmentedControl
              values={['Extract', 'Post']}
              selectedIndex={config.actionIndex}
              tintColor={PRIMARY_COLOR}
              onChange={(event) => {
                updateConfig({
                  ...config,
                  actionIndex: event.nativeEvent.selectedSegmentIndex,
                });
              }}
            />
          </View>
        )}
        <ScrollView style={styles.actionList}>
          {dataTypesOptions.map((option) => (
            <AppButton
              key={`${JSON.stringify(option.type.rawValue)}`}
              style={{ marginBottom: 12 }}
              title={`${JSON.stringify(option.type.rawValue)
                .replaceAll('"', '')
                .replaceAll('_', ' ')}`}
              capitalizeWords={true}
              onPress={() => extractData(option.type)}
            />
          ))}
        </ScrollView>
      </View>
      <DatePicker
        modal
        open={!!showDatePicker}
        date={
          (showDatePicker === 'end' ? config.endDate : config.startDate) ??
          new Date()
        }
        mode="date"
        onConfirm={(date) => {
          if (showDatePicker === 'end') {
            updateConfig({ ...config, endDate: date });
          } else {
            updateConfig({ ...config, startDate: date });
          }
          setShowDatePicker(undefined);
        }}
        onCancel={() => {
          setShowDatePicker(undefined);
        }}
        maximumDate={
          showDatePicker === 'start' ? config.endDate ?? new Date() : new Date()
        }
      />
    </>
  );
};

export default ConnectionView;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
  },
  buttonText: {
    fontSize: 15,
    color: TEXT_COLOR,
  },
  permissionsContainer: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  horizontalLine: {
    marginTop: 16,
    height: 1,
    backgroundColor: 'rgba(0, 0 ,0, 0.2)',
  },
  controlWrapper: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  actionList: {
    flex: 1,
    marginTop: 16,
    paddingHorizontal: 16,
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 12,
  },
});
