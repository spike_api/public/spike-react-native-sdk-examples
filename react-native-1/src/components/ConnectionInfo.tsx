import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  View,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { PRIMARY_COLOR, TEXT_COLOR } from '../utils/utils';
import {
  AppBaseConnection,
  clearBackgroundLogs,
  clearConnectionLog,
  getBackgroundLogs,
  getConnectionLogs,
} from '../utils/settingsStorage';
import ConnectionCard from './ConnectionCard';
import AppButton from './AppButton';
import {
  SpikeConnection,
  SpikeDataType,
  SpikeDataTypes,
} from 'react-native-spike-sdk';
import { useToast } from 'react-native-toast-notifications';

type Props = {
  baseConnection: AppBaseConnection;
  onBack: () => void;
  onConnectionClosed: () => void;
  onShowData: (title: string, data: string) => void;
  setIsLoading: (isLoading: boolean) => void;
};

const initialDataTypesDeliveries = [
  { type: SpikeDataTypes.activitiesStream, enabled: false },
  { type: SpikeDataTypes.activitiesSummary, enabled: false },
  { type: SpikeDataTypes.breathing, enabled: false },
  { type: SpikeDataTypes.calories, enabled: false },
  { type: SpikeDataTypes.distance, enabled: false },
  { type: SpikeDataTypes.glucose, enabled: false },
  { type: SpikeDataTypes.heart, enabled: false },
  { type: SpikeDataTypes.oxygenSaturation, enabled: false },
  { type: SpikeDataTypes.sleep, enabled: false },
  { type: SpikeDataTypes.steps, enabled: false },
  { type: SpikeDataTypes.body, enabled: false },
];

const ConnectionInfo = ({
  baseConnection,
  onBack,
  onConnectionClosed,
  onShowData,
  setIsLoading,
}: Props) => {
  const toast = useToast();
  const [backgroundDeliveries, setBackgroundDeliveries] = useState(
    initialDataTypesDeliveries
  );

  const isWebhookConnection = !!baseConnection.callbackUrl;

  const preloadConnectionTypes = async (connection: SpikeConnection) => {
    setIsLoading(true);
    try {
      const types = await connection.getBackgroundDeliveryDataTypes();
      const rawTypes = types.map((t) => t.rawValue);

      setBackgroundDeliveries((list) => {
        const newList = [...list];
        for (let i = 0; i < newList.length; i++) {
          const item = newList[i];
          if (!!item) {
            item.enabled = rawTypes.includes(item.type.rawValue);
          }
        }
        return newList;
      });
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  const onUpdateBackgroundDeliveries = async (
    list: { type: SpikeDataType; enabled: boolean }[]
  ) => {
    if (isWebhookConnection) {
      setIsLoading(true);
      const backgroundDeliveriesList = list
        .filter((item) => item.enabled)
        .map((item) => item.type);
      await baseConnection.connection?.enableBackgroundDelivery(
        backgroundDeliveriesList
      );
      setIsLoading(false);
    }
  };

  const enableBackgroundDeliveriesForAllTypes = async () => {
    const newList = backgroundDeliveries.map((item) => {
      return { ...item, enabled: true };
    });
    setBackgroundDeliveries(newList);
    await onUpdateBackgroundDeliveries(newList);
  };

  const disableBackgroundDeliveriesForAllTypes = async () => {
    const newList = backgroundDeliveries.map((item) => {
      return { ...item, enabled: false };
    });
    setBackgroundDeliveries(newList);
    await onUpdateBackgroundDeliveries(newList);
  };

  useEffect(() => {
    if (isWebhookConnection && !!baseConnection.connection) {
      preloadConnectionTypes(baseConnection.connection);
    }
  }, [baseConnection]);

  return (
    <>
      <View style={styles.header}>
        <AppButton title="Back" onPress={onBack} type="text" />
      </View>
      <View style={styles.contentContainer}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.cardWrapper}>
            <ConnectionCard
              baseConnection={baseConnection}
              onClosed={onConnectionClosed}
            />
          </View>
          <AppButton
            title="View connection logs"
            style={{ marginTop: 16, marginHorizontal: 16 }}
            onPress={async () => {
              const logs = await getConnectionLogs(baseConnection.uuid);
              onShowData(`Logs`, logs.join('\n'));
            }}
          />
          <AppButton
            title="Clear connection logs"
            type="bordered"
            style={{ marginTop: 6, marginHorizontal: 16 }}
            onPress={async () => {
              await clearConnectionLog(baseConnection.uuid);
            }}
          />
          {Platform.OS === 'ios' && isWebhookConnection && (
            <>
              <AppButton
                title="View background logs"
                style={{ marginTop: 16, marginHorizontal: 16 }}
                onPress={async () => {
                  const logs = await getBackgroundLogs(baseConnection.uuid);
                  onShowData(`Background logs`, logs.join('\n'));
                }}
              />
              <AppButton
                title="Clear background logs"
                type="bordered"
                style={{ marginTop: 6, marginHorizontal: 16 }}
                onPress={async () => {
                  await clearBackgroundLogs(baseConnection.uuid);
                }}
              />
              <Text
                style={[styles.title, { marginTop: 16, marginHorizontal: 16 }]}
              >
                Background deliveries
              </Text>
              {backgroundDeliveries.findIndex((item) => !item.enabled) >= 0 && (
                <AppButton
                  title="Enable All"
                  style={{ marginTop: 16, marginHorizontal: 16 }}
                  onPress={enableBackgroundDeliveriesForAllTypes}
                />
              )}
              {backgroundDeliveries.findIndex((item) => item.enabled) >= 0 && (
                <AppButton
                  title="Disable All"
                  style={{ marginTop: 16, marginHorizontal: 16 }}
                  onPress={disableBackgroundDeliveriesForAllTypes}
                />
              )}
              <View style={styles.backgroundDeliveriesList}>
                {backgroundDeliveries.map((option) => (
                  <View
                    key={`${JSON.stringify(option.type.rawValue)}`}
                    style={styles.switchContainer}
                  >
                    <Text style={styles.backgroundDeliveriesOptionText}>
                      {option.type.rawValue.replace('_', ' ')}
                    </Text>
                    <Switch
                      trackColor={{ true: PRIMARY_COLOR }}
                      onValueChange={async (value) => {
                        const newList = [...backgroundDeliveries];
                        for (let i = 0; i < newList.length; i++) {
                          const item = newList[i];
                          if (!!item && item.type === option.type) {
                            item.enabled = value;
                          }
                        }
                        setBackgroundDeliveries(newList);
                        await onUpdateBackgroundDeliveries(newList);
                      }}
                      value={option.enabled}
                    />
                  </View>
                ))}
              </View>
            </>
          )}
        </ScrollView>
      </View>
    </>
  );
};

export default ConnectionInfo;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
  },
  cardWrapper: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  deliveriesHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  backgroundDeliveriesList: {
    marginTop: 8,
    marginBottom: 32,
    marginHorizontal: 16,
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 12,
  },
  backgroundDeliveriesOptionText: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: TEXT_COLOR,
  },
});
