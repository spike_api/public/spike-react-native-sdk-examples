import { Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { PRIMARY_COLOR, TEXT_COLOR } from '../utils/utils';
import AppButton from './AppButton';
import type { AppBaseConnection } from '../utils/settingsStorage';
import { SpikeDataType, SpikeDataTypes } from 'react-native-spike-sdk';
import { useToast } from 'react-native-toast-notifications';

type Props = {
  baseConnection: AppBaseConnection;
  onBack: () => void;
  setIsLoading: (isLoading: boolean) => void;
  manageMultiplePermissions: () => void;
};

type PermissionOption = {
  type: SpikeDataType;
  enabled: boolean;
};

const initialDataTypesPermissions: PermissionOption[] = [
  { type: SpikeDataTypes.activitiesStream, enabled: false },
  { type: SpikeDataTypes.activitiesSummary, enabled: false },
  { type: SpikeDataTypes.breathing, enabled: false },
  { type: SpikeDataTypes.calories, enabled: false },
  { type: SpikeDataTypes.distance, enabled: false },
  { type: SpikeDataTypes.glucose, enabled: false },
  { type: SpikeDataTypes.heart, enabled: false },
  { type: SpikeDataTypes.oxygenSaturation, enabled: false },
  { type: SpikeDataTypes.sleep, enabled: false },
  { type: SpikeDataTypes.steps, enabled: false },
  { type: SpikeDataTypes.body, enabled: false },
];

const AndroidPermissions = ({
  baseConnection,
  onBack,
  setIsLoading,
  manageMultiplePermissions,
}: Props) => {
  const toast = useToast();
  const [permissions, setPermissions] = useState(initialDataTypesPermissions);

  const handleManageHealthConnect = async () => {
    setIsLoading(true);
    try {
      await baseConnection.connection?.manageHealthConnect();
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  const handleRevokeAllPermissions = async () => {
    setIsLoading(true);
    try {
      await baseConnection.connection?.revokeAllPermissions();
      await updatePermissionsGranted();
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  const handleManageMultiplePermissions = async () => {
    manageMultiplePermissions();
  };

  const handleRequestPermission = async (type: SpikeDataType) => {
    try {
      if (!!baseConnection.connection) {
        setIsLoading(true);
        const newList: PermissionOption[] = [...permissions];
        for (let i = 0; i < newList.length; i++) {
          const item = newList[i];
          if (!!item && item.type === type) {
            await baseConnection.connection.requestHealthPermissions(type);
            const granted =
              await baseConnection.connection.checkPermissionsGranted(type);
            item.enabled = granted;
          }
        }

        setPermissions(newList);
      }
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  const updatePermissionsGranted = async () => {
    try {
      if (!!baseConnection.connection) {
        setIsLoading(true);
        const newList: PermissionOption[] = [];
        for (let index = 0; index < permissions.length; index++) {
          const option = permissions[index];

          if (!!option) {
            const granted =
              await baseConnection.connection.checkPermissionsGranted(
                option.type
              );
            newList.push({ ...option, enabled: granted });
          }
        }
        setPermissions(newList);
      }
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (Platform.OS === 'android') {
      updatePermissionsGranted();
    }
  }, [baseConnection]);

  return (
    <>
      <View style={styles.header}>
        <AppButton title="Back" onPress={onBack} type="text" />
      </View>
      <View style={styles.contentContainer}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.bodyContainer}>
            <AppButton
              title={'Manage Health Connect'}
              onPress={handleManageHealthConnect}
            />
            <AppButton
              style={{ marginTop: 12 }}
              title={'Manage Multiple Permissions'}
              onPress={handleManageMultiplePermissions}
            />
            <AppButton
              style={{ marginTop: 12 }}
              title={'Revoke permissions'}
              onPress={handleRevokeAllPermissions}
            />
          </View>
          <Text style={styles.title}>Permissions</Text>
          <View style={styles.permissionsList}>
            {permissions.map((option) => (
              <View
                key={`${JSON.stringify(option.type.rawValue)}`}
                style={styles.rowContainer}
              >
                <Text style={styles.optionText}>
                  {option.type.rawValue
                    .replaceAll('"', '')
                    .replaceAll('_', ' ')}
                </Text>
                {option.enabled ? (
                  <Text style={styles.valueText}>Enabled</Text>
                ) : (
                  <AppButton
                    title="Request"
                    onPress={() => handleRequestPermission(option.type)}
                    type="text"
                  />
                )}
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default AndroidPermissions;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
    marginHorizontal: 16,
    marginTop: 16,
  },
  bodyContainer: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  permissionsList: {
    marginTop: 8,
    marginBottom: 32,
    marginHorizontal: 16,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 12,
  },
  optionText: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: TEXT_COLOR,
  },
  valueText: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: PRIMARY_COLOR,
  },
});
