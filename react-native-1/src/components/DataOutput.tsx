import {
  Platform,
  ScrollView,
  Share,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React from 'react';
import { TEXT_COLOR } from '../utils/utils';
import AppButton from './AppButton';
import { useToast } from 'react-native-toast-notifications';

type Props = {
  title: string;
  data: string;
  onBack: () => void;
};

const DataOutput = ({ title, data, onBack }: Props) => {
  const toast = useToast();

  const onShare = async () => {
    try {
      await Share.share({
        message: data,
      });
    } catch (error: any) {
      toast.show(`${error.message}`);
    }
  };

  return (
    <>
      <View style={styles.header}>
        <AppButton title="Back" onPress={onBack} type="text" />
        <AppButton title="Share" onPress={onShare} type="text" />
      </View>
      <View style={styles.contentContainer}>
        <Text style={styles.title}>{title}</Text>
        {Platform.OS === 'ios' ? (
          <TextInput
            style={styles.dataTextView}
            multiline={true}
            editable={false}
            scrollEnabled={true}
          >
            {data}
          </TextInput>
        ) : (
          <ScrollView
            contentContainerStyle={{ flexGrow: 1, padding: 8 }}
            style={styles.dataTextScrollView}
          >
            <Text style={styles.dataText}>{data}</Text>
          </ScrollView>
        )}
      </View>
    </>
  );
};

export default DataOutput;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    marginTop: 16,
    fontSize: 18,
    textAlign: 'center',
    textTransform: 'capitalize',
    color: TEXT_COLOR,
  },
  dataTextScrollView: {
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 16,
    marginBottom: 48,
    marginHorizontal: 16,
  },
  dataText: {
    fontSize: 15,
    color: TEXT_COLOR,
  },
  dataTextView: {
    flex: 1,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1,
    borderRadius: 8,
    padding: 8,
    marginTop: 16,
    marginBottom: 48,
    marginHorizontal: 16,
    fontSize: 15,
    color: TEXT_COLOR,
  },
});
