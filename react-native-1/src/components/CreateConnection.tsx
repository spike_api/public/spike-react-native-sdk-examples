import React, { useState } from 'react';
import { StyleSheet, Text, TextInput } from 'react-native';
import { TEXT_COLOR, uuidGenerator } from '../utils/utils';
import { View } from 'react-native';
import Spike from 'react-native-spike-sdk';
import { AppBaseConnection, generateConnection, removeGeneratedConnection } from '../utils/settingsStorage';
import AppButton from './AppButton';
import { ConnectionLogger } from '../utils/connectionLogger';
import { useToast } from 'react-native-toast-notifications';
import { WebhookConnectionListener } from '../utils/webhookConnectionListener';

type Props = {
  onBack?: () => void;
  onCreated?: (connection: AppBaseConnection) => void;
  setIsLoading: (isLoading: boolean) => void;
};

const CreateConnection = ({ onCreated, onBack, setIsLoading }: Props) => {
  const toast = useToast();
  const [appId, setAppId] = useState('');
  const [authToken, setAuthToken] = useState('');
  const [customerEndUserId, setCustomerEndUserId] = useState('');
  const [callbackURL, setCallbackURL] = useState('');

  const canCreate = !!appId && !!authToken && !!customerEndUserId;

  const onCreate = async () => {
    if (!canCreate) return;

    setIsLoading(true);
    try {
      const baseConnection = await generateConnection(
        appId,
        authToken,
        customerEndUserId,
        !!callbackURL ? callbackURL : undefined,
      );
      const logger = new ConnectionLogger(baseConnection.uuid);
      try {
        const connection = await Spike.createConnection(
          {
            appId,
            authToken,
            customerEndUserId,
            callbackUrl: !!callbackURL ? callbackURL : undefined,
          },
          logger,
        );
        if (!!callbackURL) {
          await connection.setListener(new WebhookConnectionListener(baseConnection.uuid));
        }
        baseConnection.connection = connection;
        onCreated?.(baseConnection);
      } catch (error) {
        await removeGeneratedConnection(baseConnection);
        toast.show(`${error}`);
      }
      setIsLoading(false);
      return;
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  return (
    <>
      <View style={styles.header}>
        <Text style={styles.title}>Create connection</Text>
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.inputsContainer}>
          <View style={styles.inputItemTitleContainer}>
            <Text style={styles.inputItemTitle}>App Id</Text>
            <AppButton
              title="Generate"
              style={styles.generateButton}
              type="text"
              onPress={() => {
                const newId = uuidGenerator();
                setAppId(newId);
              }}
            />
          </View>
          <TextInput
            style={styles.inputItem}
            placeholder="App id"
            placeholderTextColor={'#aaa'}
            value={appId}
            onChange={(e) => setAppId(e.nativeEvent.text)}
            autoCapitalize={'none'}
            autoCorrect={false}
          />

          <View style={styles.inputItemTitleContainer}>
            <Text style={styles.inputItemTitle}>Auth Token</Text>
            <AppButton
              title="Generate"
              style={styles.generateButton}
              type="text"
              onPress={() => {
                const newId = uuidGenerator();
                setAuthToken(newId);
              }}
            />
          </View>
          <TextInput
            style={styles.inputItem}
            placeholder="Auth token"
            placeholderTextColor={'#aaa'}
            value={authToken}
            onChange={(e) => setAuthToken(e.nativeEvent.text)}
            autoCapitalize={'none'}
            autoCorrect={false}
          />

          <View style={styles.inputItemTitleContainer}>
            <Text style={styles.inputItemTitle}>Customer end user id</Text>
            <AppButton
              title="Generate"
              style={styles.generateButton}
              type="text"
              onPress={() => {
                const newId = uuidGenerator();
                setCustomerEndUserId(newId);
              }}
            />
          </View>
          <TextInput
            style={styles.inputItem}
            placeholder="Customer end user id"
            placeholderTextColor={'#aaa'}
            value={customerEndUserId}
            onChange={(e) => setCustomerEndUserId(e.nativeEvent.text)}
            autoCapitalize={'none'}
            autoCorrect={false}
          />

          <Text style={styles.inputItemTitle}>Callback URL</Text>
          <TextInput
            style={styles.inputItem}
            placeholder="Callback url"
            placeholderTextColor={'#aaa'}
            value={callbackURL}
            onChange={(e) => setCallbackURL(e.nativeEvent.text)}
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
        <AppButton
          title="Create"
          style={[!canCreate && { opacity: 0.4, backgroundColor: 'gray' }]}
          onPress={onCreate}
          disabled={!canCreate}
        />
        <AppButton title="Cancel" style={{ marginTop: 16 }} onPress={onBack} />
      </View>
    </>
  );
};

export default CreateConnection;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    paddingHorizontal: 16,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
  },
  inputsContainer: {
    marginTop: 24,
    color: TEXT_COLOR,
  },
  inputItemTitle: {
    fontSize: 15,
    color: TEXT_COLOR,
  },
  inputItemTitleContainer: {
    flexDirection: 'row',
  },
  inputItem: {
    marginTop: 4,
    paddingHorizontal: 4,
    paddingVertical: 8,
    borderColor: 'rgba(0, 0 ,0, 0.1)',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 16,
    color: TEXT_COLOR,
  },
  generateButton: {
    marginLeft: 8,
  },
  textButtonTitle: {
    fontSize: 15,
  },
  buttonText: {
    fontSize: 15,
    color: TEXT_COLOR,
  },
});
