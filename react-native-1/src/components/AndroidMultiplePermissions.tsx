import { ScrollView, StyleSheet, Switch, Text, View } from 'react-native';
import React, { useState } from 'react';
import { PRIMARY_COLOR, TEXT_COLOR } from '../utils/utils';
import AppButton from './AppButton';
import type { AppBaseConnection } from '../utils/settingsStorage';
import { SpikeDataType, SpikeDataTypes } from 'react-native-spike-sdk';
import { useToast } from 'react-native-toast-notifications';

type Props = {
  baseConnection: AppBaseConnection;
  onBack: () => void;
  setIsLoading: (isLoading: boolean) => void;
};

type PermissionOption = {
  type: SpikeDataType;
};

const permissions: PermissionOption[] = [
  { type: SpikeDataTypes.activitiesStream },
  { type: SpikeDataTypes.activitiesSummary },
  { type: SpikeDataTypes.breathing },
  { type: SpikeDataTypes.calories },
  { type: SpikeDataTypes.distance },
  { type: SpikeDataTypes.glucose },
  { type: SpikeDataTypes.heart },
  { type: SpikeDataTypes.oxygenSaturation },
  { type: SpikeDataTypes.sleep },
  { type: SpikeDataTypes.steps },
  { type: SpikeDataTypes.body },
];

const AndroidMultiplePermissions = ({
  baseConnection,
  onBack,
  setIsLoading,
}: Props) => {
  const toast = useToast();
  const [selectedPermissions, setSelectedPermissions] = useState<
    PermissionOption[]
  >([]);

  const handleRequestPermission = async () => {
    try {
      if (!!baseConnection.connection) {
        setIsLoading(true);
        await baseConnection.connection.requestHealthPermissions(
          selectedPermissions.map((it) => it.type)
        );
      }
    } catch (error) {
      toast.show(`${error}`);
    }
    setIsLoading(false);
  };

  return (
    <>
      <View style={styles.header}>
        <AppButton title="Back" onPress={onBack} type="text" />
      </View>
      <View style={styles.contentContainer}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.bodyContainer}>
            <AppButton
              title={'Request all selected permissions'}
              onPress={handleRequestPermission}
            />
          </View>
          <Text style={styles.title}>Permissions</Text>
          <View style={styles.permissionsList}>
            {permissions.map((option) => (
              <View
                key={`${JSON.stringify(option.type.rawValue)}`}
                style={styles.rowContainer}
              >
                <Text style={styles.optionText}>
                  {option.type.rawValue
                    .replaceAll('"', '')
                    .replaceAll('_', ' ')}
                </Text>
                <Switch
                  trackColor={{ true: PRIMARY_COLOR }}
                  onValueChange={async (value) => {
                    setSelectedPermissions((array) => {
                      const newArray = [...array];
                      if (value) {
                        const exist = array.find(
                          (it) => it.type.rawValue === option.type.rawValue
                        );
                        if (!exist) {
                          newArray.push(option);
                        }
                        return newArray;
                      }
                      return newArray.filter(
                        (it) => it.type.rawValue !== option.type.rawValue
                      );
                    });
                  }}
                  value={
                    !!selectedPermissions.find(
                      (it) => it.type.rawValue === option.type.rawValue
                    )
                  }
                />
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default AndroidMultiplePermissions;

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  header: {
    height: 48,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0 ,0, 0.2)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 18,
    color: TEXT_COLOR,
    marginHorizontal: 16,
    marginTop: 16,
  },
  bodyContainer: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  permissionsList: {
    marginTop: 8,
    marginBottom: 32,
    marginHorizontal: 16,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 12,
  },
  optionText: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: TEXT_COLOR,
  },
  valueText: {
    fontSize: 15,
    textTransform: 'capitalize',
    color: PRIMARY_COLOR,
  },
});
