import * as React from 'react';
import BaseComponent from './components/BaseComponent';
import { ToastProvider } from 'react-native-toast-notifications';

export default function App() {
  return (
    <ToastProvider>
      <BaseComponent />
    </ToastProvider>
  );
}
