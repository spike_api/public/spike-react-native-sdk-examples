//
//  DataResponseModel.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 22/04/2024.
//

import Foundation

class DataResponseModel: Codable {
    let date: String?
    let avgValue: Double?
    let minValue: Double?
    let maxValue: Double?
    let unit: String?
    let source: String?
    let intradayData: [IntradayData]?
    let dataType: String?
    
    enum CodingKeys: String, CodingKey {
        case date
        case avgValue = "avg_value"
        case minValue = "min_value"
        case maxValue = "max_value"
        case unit
        case intradayData = "intraday_data"
        case source
        case dataType = "dataType"
    }
    
    class IntradayData: Codable {
        let time: String
        let value: Double?
        let realtimeValue: Double?
        let smoothedValue: Double?
        let status: String?
        let trend: String?
        let trendRate: Double?
        
        enum CodingKeys: String, CodingKey {
            case time
            case value
            case realtimeValue = "realtime_value"
            case smoothedValue = "smoothed_value"
            case status
            case trend
            case trendRate = "trend_rate"
        }
    }
}

