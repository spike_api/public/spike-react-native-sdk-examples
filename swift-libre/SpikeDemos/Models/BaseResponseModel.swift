//
//  BaseResponseModel.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 22/04/2024.
//

import Foundation

class BaseResponseModel<DataModel: Codable>: Codable {
    let data: DataModel
}
