//
//  ChartViewController.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 19/04/2024.
//

import UIKit
import DGCharts

class ChartViewController: UIViewController {
    
    private let colors: [UIColor] = [
        .systemRed,
        .systemGreen,
        .systemBlue,
        .systemOrange,
        .systemPink,
        .systemCyan
    ]
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lineChartView.chartDescription.enabled = false

        lineChartView.leftAxis.drawAxisLineEnabled = true
        lineChartView.leftAxis.axisLineColor = .black
        lineChartView.leftAxis.labelTextColor = .black
        lineChartView.rightAxis.enabled = false
        lineChartView.xAxis.enabled = false
        
        lineChartView.drawBordersEnabled = false
        lineChartView.setScaleEnabled(true)

        let l = lineChartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .top
        l.orientation = .horizontal
        l.drawInside = false
        l.textColor = .black
        
        startPolling()
    }
    
    @IBAction func logOut(_ sender: UIBarButtonItem) {
        DataStorage.resetUserId()
        navigationController?.popViewController(animated: true)
    }
    
    func startPolling() {
        Api.shared.fetchData { [weak self] result in
            guard let weakSelf = self else { return }
        
            switch result {
            case .success(let response):
                print("New data!")
                weakSelf.updateChartData(data: response.data)
            case .failure(let error):
                print(error.message)
            }
            
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 5) { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.startPolling()
            }
        }
    }
    
    func updateChartData(data: [DataResponseModel]) {
        let dataSets = data.enumerated().map { (index, model) -> LineChartDataSet in
            let yVals = model.intradayData?.enumerated().map{ (i, item) -> ChartDataEntry in
                return ChartDataEntry(x: Double(i), y: item.value ?? 0)
            } ?? []
            let set = LineChartDataSet(entries: yVals, label: "\(model.date ?? "Day \(index + 1)")")
            set.lineWidth = 2
            set.circleRadius = 4
            set.circleHoleRadius = 2
            let color = colors[index % colors.count]
            set.setColor(color)
            set.setCircleColor(color)
            
            return set
        }
        
        let data = LineChartData(dataSets: dataSets)
        data.setValueFont(.systemFont(ofSize: 8, weight: .light))
        data.setValueTextColor(.black)
        lineChartView.data = data
    }

    func configLineChart() {
        
    }
}
