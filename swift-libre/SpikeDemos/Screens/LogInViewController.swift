//
//  LogInViewController.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 19/04/2024.
//

import UIKit
import WebKit

class LogInViewController: UIViewController {
    
    @IBOutlet weak var wkWebView: WKWebView!
    
    var loadedUserId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .white
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
        let storageUserId = DataStorage.userId
        if loadedUserId != storageUserId {
            loadWebView(userId: storageUserId)
        }
    }
    
    func loadWebView(userId: String) {
        loadedUserId = userId
        WKWebsiteDataStore.default().removeData(ofTypes: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache], modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        let url = URL(string: "https://api.spikeapi.com/init-user-integration?provider=freestyle_libre&client_id=3f3c5a4f-043c-44ba-b213-d55679902f13&user_id=\(userId)")!
        let request = URLRequest(url: url)
        wkWebView.load(request)
        
        wkWebView.navigationDelegate = self
    }

}

extension LogInViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction) async -> WKNavigationActionPolicy {
        if let urlString = navigationAction.request.url?.absoluteString {
            
            if urlString == "https://console.spikeapi.com/success" {
                self.performSegue(withIdentifier: "showChart", sender: self)
                return .cancel
            }
        }
        
        return .allow
    }
}
