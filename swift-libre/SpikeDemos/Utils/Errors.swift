//
//  Errors.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 19/04/2024.
//

import Foundation

enum Errors: Error {
    case ApiError(String)
    
    var message: String {
        switch self {
        case .ApiError(let message):
            return message
        }
    }
}
