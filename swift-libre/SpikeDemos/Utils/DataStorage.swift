//
//  DataStorage.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 19/04/2024.
//

import Foundation

class DataStorage {
    private static let USER_ID_STORAGE_KEY = "USER_ID_STORAGE_KEY"
    
    private static let userDefaults = UserDefaults.standard
    
    private init() {}
    
    static var userId: String {
        get {
            if let id = userDefaults.value(forKey: USER_ID_STORAGE_KEY) as? String {
                return id
            }
            let newId = UUID().uuidString
            userDefaults.setValue(newId, forKey: USER_ID_STORAGE_KEY)
            return newId
        }
        set {
            userDefaults.setValue(newValue, forKey: USER_ID_STORAGE_KEY)
        }
    }
    
    static func resetUserId() {
        userId = UUID().uuidString
    }
}
