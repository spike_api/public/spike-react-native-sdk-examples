//
//  Api.swift
//  SpikeDemos
//
//  Created by Edgar Kroman on 19/04/2024.
//

import Foundation

class Api {
    private let session = URLSession(configuration: .default)
    
    static let shared = Api()
    private init() {}
    
    func fetchData(handler: @escaping (Result<BaseResponseModel<[DataResponseModel]>, Errors>) -> Void) {
        let url = URL(string: "https://spike-core-api-test-data.s3.eu-central-1.amazonaws.com/libre1.json")!
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    handler(.failure(.ApiError("Request error: \(error)")))
                    return
                } else if let data = data {
                    do {
                        let jsonDecoder = JSONDecoder()
                        let model = try jsonDecoder.decode(BaseResponseModel<[DataResponseModel]>.self, from: data)
                        handler(.success(model))
                        return
                    } catch let parseError {
                        handler(.failure(.ApiError("Response error: \(parseError)")))
                        return
                    }
                }
                
                handler(.failure(.ApiError("Server error")))
            }
        }
        task.resume()
    }
//    
//    static func dataPollingAsyncSequence() -> AsyncStream<Result<String, Errors>> {
//        return AsyncStream(Result<String, Errors>.self) { continuation in
//            let task = Task {
//                try? await Task.sleep(nanoseconds: 5_000_000_000)
//                let result = await fetchData()
//                continuation.yield(result)
//            }
//            
//            continuation.onTermination = { _ in
//                task.cancel()
//            }
//        }
//    }
}
