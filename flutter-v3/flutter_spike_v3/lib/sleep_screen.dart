import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'dart:convert';

class SleepScreen extends StatefulWidget {
  final SpikeConnectionV3 connection;

  const SleepScreen({super.key, required this.connection});

  @override
  State<SleepScreen> createState() => _SleepScreenState();
}

class _SleepScreenState extends State<SleepScreen> {
  // ------------------------------- CONFIGURATION -----------------------------
  final SleepConfig _sleepConfig = SleepConfig(
    // Configure which sleep data you want to include:
    includeMetricTypes: {MetricType.heartrate},
  );
  // ---------------------------- end of CONFIGURATION ------------------------
  bool _hasPermissions = false;
  String _sleepData = '';

  // ------------------------------- PERMISSIONS -------------------------------
  Future<void> _requestPermissions() async {
    try {
      await widget.connection.requestHealthPermissions(
        sleepConfigs: [_sleepConfig],
      );

      setState(() {
        _hasPermissions = true;
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of PERMISSIONS ---------------------------

  // -------------------------------- GET DATA ---------------------------------
  Future<void> _getSleepData() async {
    try {
      final now = DateTime.now();
      final tomorrow = DateTime(now.year, now.month, now.day + 1);
      final from = now.subtract(const Duration(days: 2));

      final sleepRecords = await widget.connection.getSleep(
        config: _sleepConfig,
        from: from,
        to: tomorrow,
      );

      setState(() {
        _sleepData = sleepRecords
            .map((record) =>
                const JsonEncoder.withIndent('  ').convert(record.toJson()))
            .join('\n\n');
      });
    } catch (e) {
      print(e);
    }
  }

  // ---------------------------- end of GET DATA -----------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sleep Screen'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (!_hasPermissions)
                  ElevatedButton(
                    onPressed: _requestPermissions,
                    child: const Text('Get Permissions'),
                  ),
                if (_hasPermissions) ...[
                  ElevatedButton(
                    onPressed: _getSleepData,
                    child: const Text('Get data'),
                  ),
                ],
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16),
              child: Text(_sleepData),
            ),
          ),
        ],
      ),
    );
  }
}
