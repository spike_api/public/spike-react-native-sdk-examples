import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/model/data_v3/background_delivery_config.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';

class BackgroundScreen extends StatefulWidget {
  final SpikeConnectionV3 connection;

  const BackgroundScreen({super.key, required this.connection});

  @override
  State<BackgroundScreen> createState() => _BackgroundScreenState();
}

class _BackgroundScreenState extends State<BackgroundScreen> {
  // ------------------------------- PERMISSIONS -------------------------------
  // Change to type(s) you want to request.
  // Make sure to add the corresponding permissions to the AndroidManifest.xml
  // file.
  final List<StatisticsType> _statisticsTypes = [StatisticsType.steps];

  bool _hasPermissions = false;
  BackgroundDeliveryConfig? _backgroundDeliveryConfig;
  bool _hasReadConfig = false;

  Future<void> _requestPermissions() async {
    try {
      // Get user permissions for reading data from HealthKit on iOS or Health
      // Connect on Android.
      // If you need more granular control over what happens on Android, please
      // look into SpikeSDK documentation to find out what other functionality
      // is available.
      await widget.connection.requestHealthPermissions(
        statisticTypes: _statisticsTypes,
        // Ask for additional permissions on Android to enable background
        // delivery:
        includeBackgroundDelivery: true,
      );
      setState(() {
        _hasPermissions = true;
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of PERMISSIONS ---------------------------

  // ------------------------ BACKGROUND DELIVERY -----------------------
  Future<void> _enableBackgroundDelivery() async {
    try {
      await widget.connection.enableBackgroundDelivery(
        statisticTypes: _statisticsTypes,
      );
      await _getBackgroundDeliveryConfig();
    } catch (e) {
      print(e);
    }
  }

  Future<void> _disableBackgroundDelivery() async {
    try {
      await widget.connection.disableBackgroundDelivery();
      await _getBackgroundDeliveryConfig();
    } catch (e) {
      print(e);
    }
  }

  Future<void> _getBackgroundDeliveryConfig() async {
    try {
      final bacgroundDeliveryConfig = await widget.connection.getBackgroundDeliveryConfig();

      setState(() {
        _backgroundDeliveryConfig = bacgroundDeliveryConfig;
        _hasReadConfig = true;
      });
    } catch (e) {
      print(e);
      setState(() {
        _hasReadConfig = true;
      });
    }
  }

  // ------------------- end of BACKGROUND DELIVERY ----------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Background delivery Screen'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (!_hasPermissions)
                  ElevatedButton(
                    onPressed: _requestPermissions,
                    child: const Text('Get Permissions'),
                  ),
                if (_hasPermissions) ...[
                  ElevatedButton(
                    onPressed: _enableBackgroundDelivery,
                    child: const Text('Enable background delivery'),
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: _disableBackgroundDelivery,
                    child: const Text('Disable background delivery'),
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: _getBackgroundDeliveryConfig,
                    child: const Text('Read background delivery config'),
                  ),
                ],
              ],
            ),
          ),
          if (_hasReadConfig) ...[
            const Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Background Delivery Settings',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Enabled: ${_backgroundDeliveryConfig == null ? 'No' : 'Yes'}'),
                      const SizedBox(height: 8),
                      Text(
                          'Configuration: ${_backgroundDeliveryConfig != null ? jsonEncode(_backgroundDeliveryConfig!.toJson()) : 'Not configured'}'),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ],
      ),
    );
  }
}
