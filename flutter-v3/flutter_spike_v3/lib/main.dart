import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spike_v3/activities_screen.dart';
import 'package:flutter_spike_v3/background_screen.dart';
import 'package:flutter_spike_v3/sleep_screen.dart';
import 'package:flutter_spike_v3/statistics_screen.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'package:crypto/crypto.dart';

// ---------------------------------- READ ME ----------------------------------
//
// Before moving forward, we have to connect to SpikeSDK.
// Scroll down to see example of connection.
//
// For examples of using SpikeConnectionV3 see:
// - Statistics like steps, distance etc.:  lib/statistics_screen.dart
// - Workouts: lib/activities_screen.dart
// - Sleep: lib/sleep_screen.dart
// - Background delivery: lib/background_screen.dart
//
// -----------------------------------------------------------------------------

void main() async {
  await dotenv.load(fileName: ".env");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SpikeSDK Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.amber),
        useMaterial3: true,
      ),
      home: const ConnectPage(title: 'SpikeSDK Flutter Demo'),
    );
  }
}

class ConnectPage extends StatefulWidget {
  const ConnectPage({super.key, required this.title});
  final String title;

  @override
  State<ConnectPage> createState() => _ConnectPageState();
}

class _ConnectPageState extends State<ConnectPage> {
  SpikeConnectionV3? connection;
  int _selectedIndex = 0;

  void _connect() async {
    print('Connect button pressed!');

    String appId = dotenv.env['APP_ID'] ?? 'Set value in `.env` file';
    String appSecret = dotenv.env['APP_SECRET'] ?? 'Set value in `.env` file';
    String appUser = dotenv.env['APP_USER'] ?? 'Set value in `.env` file';

    // Never include the HMAC signing secret in production applications!
    // This is strictly for development use only.
    // Before deployment, ensure this code is moved to your backend, and never expose the HMAC
    // signing secret in the app.
    final signature = _generateSignature(appSecret, appUser);

    try {
      // ----------------------- CONNECT to SpikeSDK ---------------------------

      final spikeConnection = await SpikeSDKV3.createConnection(
        applicationId: int.parse(appId),
        signature: signature,
        endUserId: appUser,
      );

      // --------------------- end of CONNECT to SpikeSDK ----------------------

      setState(() {
        connection = spikeConnection;
      });
    } catch (e) {
      print(e);
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _getSelectedWidget() {
    switch (_selectedIndex) {
      case 0:
        return StatisticsScreen(connection: connection!);
      case 1:
        return ActivitiesScreen(connection: connection!);
      case 2:
        return SleepScreen(connection: connection!);
      case 3:
        return BackgroundScreen(connection: connection!);
      default:
        return const Center(child: Text('Unknown tab'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: connection == null
            ? ElevatedButton(
                onPressed: _connect,
                child: const Text('Connect to SpikeSDK'),
              )
            : _getSelectedWidget(),
      ),
      bottomNavigationBar: connection != null
          ? BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.bar_chart),
                  label: 'Statistics',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.directions_run),
                  label: 'Activities',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.bedtime),
                  label: 'Sleep',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.cloud_upload),
                  label: 'Background',
                ),
              ],
              currentIndex: _selectedIndex,
              onTap: _onItemTapped,
            )
          : null,
    );
  }
}

// DO NOT sign in the app itself!
// This is strictly for development use only.
// Before deployment, ensure this code is moved to your backend, and never expose the HMAC
// signing secret in the app.
String _generateSignature(String appSecret, String appUser) {
  var key = utf8.encode(appSecret);
  var bytes = utf8.encode(appUser);
  var hmacSha256 = Hmac(sha256, key);
  var signature = hmacSha256.convert(bytes);
  return signature.toString();
}
