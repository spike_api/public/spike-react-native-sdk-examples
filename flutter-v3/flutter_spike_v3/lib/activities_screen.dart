import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';
import 'dart:convert';

class ActivitiesScreen extends StatefulWidget {
  final SpikeConnectionV3 connection;

  const ActivitiesScreen({super.key, required this.connection});

  @override
  State<ActivitiesScreen> createState() => _ActivitiesScreenState();
}

class _ActivitiesScreenState extends State<ActivitiesScreen> {
  // ------------------------------- CONFIGURATION -----------------------------
  final ActivityConfig _activityConfig = ActivityConfig(
    // Change to metric type(s) you want:
    includeMetricTypes: {MetricType.distanceRunning},
    includeAdditionalData: null,
  );
  // ---------------------------- end of CONFIGURATION ------------------------

  bool _hasPermissions = false;
  String _activitiesData = '';

  // ------------------------------- PERMISSIONS -------------------------------
  Future<void> _requestPermissions() async {
    try {
      // Get user permissions for reading data from HealthKit on iOS or Health Connect on Android.
      // If you need more granular control over what happens on Android, please look into SpikeSDK
      // documentation to find out what other functionality is available.
      //
      // If you plan to have different requests for activities you can pass multiple activityConfigs,
      // to get permissions for all of them in one go.
      await widget.connection.requestHealthPermissions(
        activityConfigs: [_activityConfig],
        // Ask for additional permissions on Android to better detect manual
        // entries (optional, needed only if you care about manual entries):
        includeEnhancedPermissions: true,
      );

      setState(() {
        _hasPermissions = true;
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of PERMISSIONS ---------------------------

  // -------------------------------- GET DATA ---------------------------------
  Future<void> _getActivities() async {
    try {
      final now = DateTime.now();
      final tomorrow = DateTime(now.year, now.month, now.day + 1);
      final from = now.subtract(const Duration(days: 2));

      final activities = await widget.connection.getActivities(
        config: _activityConfig,
        from: from,
        to: tomorrow,
      );

      setState(() {
        _activitiesData = activities
            .map((activity) =>
                const JsonEncoder.withIndent('  ').convert(activity.toJson()))
            .join('\n\n');
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of GET DATA -----------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Activities Screen'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (!_hasPermissions)
                  ElevatedButton(
                    onPressed: _requestPermissions,
                    child: const Text('Get Permissions'),
                  ),
                if (_hasPermissions) ...[
                  ElevatedButton(
                    onPressed: _getActivities,
                    child: const Text('Get data'),
                  ),
                ],
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16),
              child: Text(_activitiesData),
            ),
          ),
        ],
      ),
    );
  }
}
