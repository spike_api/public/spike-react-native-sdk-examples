import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';

class StatisticsScreen extends StatefulWidget {
  final SpikeConnectionV3 connection;

  const StatisticsScreen({super.key, required this.connection});

  @override
  State<StatisticsScreen> createState() => _StatisticsScreenState();
}

class _StatisticsScreenState extends State<StatisticsScreen> {
  // ------------------------------- PERMISSIONS -------------------------------
  // Change to type(s) you want to request.
  // Make sure to add the corresponding permissions to the AndroidManifest.xml
  // file.
  final List<StatisticsType> _statisticsTypes = [StatisticsType.steps];

  bool _hasPermissions = false;
  String _statisticsData = '';

  Future<void> _requestPermissions() async {
    try {
      // Get user permissions for reading data from HealthKit on iOS or Health
      // Connect on Android.
      // If you need more granular control over what happens on Android, please
      // look into SpikeSDK documentation to find out what other functionality
      // is available.
      await widget.connection.requestHealthPermissions(
        statisticTypes: _statisticsTypes,
        // Ask for additional permissions on Android to better detect manual
        // entries (optional, needed only if you care about manual entries):
        includeEnhancedPermissions: true,
      );
      setState(() {
        _hasPermissions = true;
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of PERMISSIONS ---------------------------

  // -------------------------------- GET DATA ---------------------------------
  Future<void> _getStatistics() async {
    try {
      final now = DateTime.now();
      final tomorrow = DateTime(now.year, now.month, now.day + 1);
      final weekAgo = now.subtract(const Duration(days: 7));

      final statistics = await widget.connection.getStatistics(
        ofTypes: _statisticsTypes,
        from: weekAgo,
        to: tomorrow,
        interval: StatisticsInterval.day,
      );

      setState(() {
        _statisticsData = statistics
            .map((stat) =>
                const JsonEncoder.withIndent('  ').convert(stat.toJson()))
            .join('\n\n');
      });
    } catch (e) {
      print(e);
    }
  }
  // ---------------------------- end of GET DATA -----------------------------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Statistics Screen'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (!_hasPermissions)
                  ElevatedButton(
                    onPressed: _requestPermissions,
                    child: const Text('Get Permissions'),
                  ),
                if (_hasPermissions) ...[
                  ElevatedButton(
                    onPressed: _getStatistics,
                    child: const Text('Get data'),
                  ),
                ],
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16),
              child: Text(_statisticsData),
            ),
          ),
        ],
      ),
    );
  }
}
