# Flutter SpikeSDK v3 example app

This project provides a straightforward example of using the SpikeSDK in a Flutter application. It demonstrates how to:

1. Establish a connection to SpikeSDK.
1. Integrate Apple HealthKit (iOS) and Android Health Connect.
1. Retrieve health data based on your requirements.


# Intended Audience

This app is designed for developers and serves as a foundational starting point for integrating SpikeSDK. The app features minimal UI and focuses on showcasing core functionality through code.


# Usage

To customize the app for your needs, examine the codebase and modify the settings to fetch different data types or adjust configurations.

Before running the app, you need to set up the environment variables with your app credentials. To do this, copy the example .env file and update it with your credentials:

```
cp .env.example .env
```

You’re now ready to dive into the code! All the essential logic and functionality can be found in following files:

- __Connection__: lib/main.dart
- __Statistics__ like steps, distance etc.:  lib/statistics_screen.dart
- __Workouts__: lib/activities_screen.dart
- __Sleep__: lib/sleep_screen.dart

