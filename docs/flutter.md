
#  SpikeSDK Flutter (v3)

## Requirements

iOS 13.0+

Android 9.0+ (Level 28, P, Pie)

# Installation

Add ependency on SpikeSDK in your pubspec.yaml file:

```yaml
dependencies:
  spike_flutter_sdk: ^4.0.14
```

# iOS Setup Guide

Use `pod install` and `pod update` commands from `ios/` folder of your app to install/update SpikeSDK.

## iOS Signing & Capabilities

To add HealthKit support to your application's Capabilities.

* Open the `ios/` folder of your project in Xcode
* Select the project name in the left sidebar
* Open Signing & Capabilities section
* In the main view select '+ Capability' and double click HealthKit

More details you can find [here](https://developer.apple.com/documentation/healthkit/setting_up_healthkit).

## Info.plist

Add Health Kit permissions descriptions to your Info.plist file:

```
<key>NSHealthShareUsageDescription</key>
<string>We will use your health information to better track workouts.</string>

<key>NSHealthUpdateUsageDescription</key>
<string>We will update your health information to better track workouts.</string>
```

# Android Setup Guide

To add the SDK to your Android project, you have to add the following to your project's `build.gradle` file in the repositories block.

```
allprojects {
    repositories {
        // Other repositories
        maven {
            url 'https://gitlab.com/api/v4/projects/43396247/packages/maven'
        }
    }
}
```

Set Android SDK version in `local.properties` file:

```
flutter.minSdkVersion=28
flutter.compileSdkVersion=34
```

## Android permissions

Include the necessary health permissions in your `AndroidManifest.xml` to fully leverage the Spike SDK and access data from apps integrated with Health Connect. Please refer to [this guide](https://developer.android.com/health-and-fitness/guides/health-connect/get-started#declare-permissions) for details on the required permissions.

**Note**: Only request permissions essential to your app’s functionality. Requesting unused permissions will lead to app store rejection!

```xml
    <uses-permission android:name="android.permission.health.READ_NUTRITION"/>
    <uses-permission android:name="android.permission.health.READ_ACTIVE_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_TOTAL_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_STEPS"/>
    <uses-permission android:name="android.permission.health.READ_DISTANCE"/>
    <uses-permission android:name="android.permission.health.READ_ELEVATION_GAINED"/>
    <uses-permission android:name="android.permission.health.READ_RESTING_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE_VARIABILITY"/>
    <uses-permission android:name="android.permission.health.READ_FLOORS_CLIMBED"/>
    <uses-permission android:name="android.permission.health.READ_BASAL_METABOLIC_RATE"/>
    <uses-permission android:name="android.permission.health.READ_SLEEP"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_EXERCISE"/>
    <uses-permission android:name="android.permission.health.READ_SPEED"/>
    <uses-permission android:name="android.permission.health.READ_POWER"/>
    <uses-permission android:name="android.permission.health.READ_OXYGEN_SATURATION"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_GLUCOSE"/>
    <uses-permission android:name="android.permission.health.READ_RESPIRATORY_RATE"/>
    <uses-permission android:name="android.permission.health.READ_WEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_HEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_BODY_FAT"/>
    <uses-permission android:name="android.permission.health.READ_LEAN_BODY_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_WATER_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_TEMPERATURE"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_PRESSURE"/>
    <uses-permission android:name="android.permission.health.READ_BONE_MASS"/>

```

Add an intent filter to your activity definition so that you can request the permissions at runtime.

```xml
<intent-filter>
    <action android:name="androidx.health.ACTION_SHOW_PERMISSIONS_RATIONALE" />
</intent-filter>
```

Check the contents of `android/app/src/main/kotlin/{YOUR_PACKAGE_ID}/MainActivity.kt`. You must see something like that in case it is the new app being developed:

```kotlin
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {
}
```

You must change `FlutterActivity` to the `FlutterFragmentActivity` which means that your code should turn into the result similar to the one below:

```kotlin
import io.flutter.embedding.android.FlutterFragmentActivity

class MainActivity: FlutterFragmentActivity() {
}
```

In the Health Connect permissions activity, there is a link to your privacy policy. You need to grant the Health Connect app access in order to link back to your privacy policy. In the example below, you should either replace .MainActivity with an activity that presents the privacy policy or have the Main Activity route the user to the policy. This step may be required to pass Google app review when requesting access to sensitive permissions.

```xml
<activity-alias
     android:name="ViewPermissionUsageActivity"
     android:exported="true"
     android:targetActivity=".MainActivity"
     android:permission="android.permission.START_VIEW_PERMISSION_USAGE">
        <intent-filter>
            <action android:name="android.intent.action.VIEW_PERMISSION_USAGE" />
            <category android:name="android.intent.category.HEALTH_PERMISSIONS" />
        </intent-filter>
</activity-alias>
```



**Note**: If permissions handling is not working, this might be related to launch mode being singleTop. This might be not needed, but some apps face problems when requesting permissions. If you face them, then you should try removing the following line:

```xml
android:launchMode="singleTop"
```


**Note**: If app is not building, it might be related to label replacement issue. In this case, you should add the following line:

```xml
tools:replace="android:label"
```

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
>

<application
        tools:replace="android:label"

```

# Spike SDK Usage 

### Step 1 - Create Spike connection

```dart
import 'package:spike_flutter_sdk/spike_flutter_sdk.dart';

final spikeConnection = await SpikeSDKV3.createConnection(
    appId: appId,
    authToken: appSecret,
    customerEndUserId: appUser,
);
```

### Step 2 - ask user for permissions

If you plan to use Android Health Connect and/or Apple HealthKit data providers you should first get users permission to use their data:

```dart
// This call may present user with OS native modal asking for read 
// permissions needed to get steps and distance data. If you want to 
// present it only once, you should list all the statistic types you 
// plan to use. Otherwise, you can call this only before you actually 
// want to use the data.
await widget.connection.requestHealthPermissions(
    statisticTypes: [StatisticsType.steps],
    includeEnhancedPermissions: false,
);
``` 

* - for Android there are more methods that let you get permissions granted by user, check if Health Connect is installed or should be updated, etc. If you are OK with SpikeSDK handling these, you can use `requestHealthPermissions` only. On iOS `requestHealthPermissions` is the only method that has to be called. Keep in mind, that you should do it before trying to read data from Android Health Connect or Apple HealthKit.


### Step 3 - Get data

Info: The maximum permitted date range is 90 days

There are two types of data you can retrieve from Spike: records and statistics. Records consist of the raw data points collected from user devices or applications. Statistics, on the other hand, are calculated values derived from these samples, providing insights into user behavior.


#### Statistics

Now you can read hourly statistics data of steps and distance for today:

```dart
final now = DateTime.now();
final tomorrow = DateTime(now.year, now.month, now.day + 1);
final weekAgo = now.subtract(const Duration(days: 7));

final statistics = await widget.connection.getStatistics(
    ofTypes: [StatisticsType.steps],
    from: weekAgo,
    to: tomorrow,
    interval: StatisticsInterval.day,
);
```

where:

```dart
enum StatisticsType {
  steps,
  distanceTotal,
  distanceWalking,
  distanceCycling,
  distanceRunning,
  caloriesBurnedTotal,
  caloriesBurnedBasal,
  caloriesBurnedActive;
}

enum StatisticsInterval {
  hour,
  day;
}

// Filter
class StatisticsFilter {
  final bool excludeManual;
  final Set<Provider>? providers;
  final Set<ActivityTag>? activityTags;
}

// Result:

class Statistic {
  final DateTime start;
  final DateTime end;
  final int duration;
  final StatisticsType type;
  final num value;
  final Unit unit;
  final int? rowCount;
  final List<String>? recordIds;
}
```


#### Records

```dart
final now = DateTime.now();
final tomorrow = DateTime(now.year, now.month, now.day + 1);
final weekAgo = now.subtract(const Duration(days: 7));

final records = await connection.getRecords(
    ofTypes: [MetricType.stepsTotal],
    from: weekAgo,
    to: tomorrow,
    filter: StatisticsFilter(excludeManual: false),
);

```

where:

```dart
enum MetricType {
  heartrateMax,
  heartrateAvg,
  heartrateMin,
  heartrate,
  heartrateResting,
  heartrateVariabilityRmssd,
  heartrateVariabilityRmssdDeepSleep,
  elevationMax,
  elevationAvg,
  elevationMin,
  elevationGain,
  elevationLoss,
  ascent,
  descent,
  caloriesBurnedActive,
  caloriesBurnedBasal,
  caloriesBurned,
  caloriesIntake,
  stepsTotal,
  floorsClimbed,
  distanceTotal,
  distanceWalking,
  distanceCycling,
  distanceRunning,
  distanceWheelchair,
  distanceSwimming,
  speedMax,
  speedAvg,
  speedMin,
  airTemperatureMax,
  airTemperatureAvg,
  airTemperatureMin,
  spo2Max,
  spo2Avg,
  spo2Min,
  longitude,
  latitude,
  elevation,
  durationActive,
  swimmingLengths,
  swimmingDistancePerStroke;
}


// Result: 

class Record {
  final String recordId;
  final String? inputMethod;
  final DateTime startAt;
  final DateTime? endAt;
  final DateTime modifiedAt;
  final int? duration;
  final String? provider;
  final String? providerSource;
  final bool? isSourceAggregated;
  final RecordSource? source;
  final Map<String, num>? metrics;
  final List<ActivityTag>? activityTags;
  final String? activityType;
  final List<ActivityEntry>? sessions;
  final List<ActivityEntry>? laps;
  final List<ActivityEntry>? segments;
  final List<ActivityEntry>? splits;
  final List<ActivityEntry>? samples;
  final List<ActivityEntry>? routePoints;
  final List<ActivityEntry>? sleep;
}

class ActivityEntry {
  final int? divisionRef;
  final String? divisionLabel;
  final DateTime? startAt;
  final DateTime? endAt;
  final int? duration;
  final Map<String, num>? metrics;
}
```


# Backgrdound delivery

Background delivery ensures that data updates are sent to your backend via webhooks, even when the application is in the background or closed.

## iOS

### Important notes about background delivery on iOS

* For most data types the most possible frequency of updates is 1 hour.
* iOS can update data more frequently for some data types, for example, vo2 max.
* iOS may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Background delivery is not possible while device is locked, so it will be executed only when the device is unlocked.
* iOS may stop background delivery if it detects that the app is not active for a long time.
* The feature is available starting with iOS 15.

> **Important:** The SpikeSDK, along with any other HealthKit applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by iOS. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


### Setup

#### Enable background delivery for app target

* Open XCode with your ios project
* Open the folder of your project in Xcode
* Select the project name in the left sidebar
* Open Signing & Capabilities section 
* Select HealthKit background delivery under HealthKit section

### Initialization at app startup

For background delivery to work properly, you need to initialize the SpikeSDK at app startup. 

`AppDelegate.swift`:
```swift
import SpikeSDK
...
  override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    ...
    Spike.configure()
    ...
  }
...
```

## Android

### Important notes about background delivery on Android

* Background delivery is scheduled to run every hour. But ultimately Android decides when the delivery will be executed.
* Android may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Android may stop background delivery if it detects that the app is not active for a long time.
* There is a limit of queries that can be done in Health Connect and it is different for foreground and background reads, so please request only essential data to be delivered in background. More information on (Health Connect documentation)[https://developer.android.com/health-and-fitness/guides/health-connect/plan/rate-limiting]


> **Important:** The SpikeSDK, along with any other applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by Android. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


### Setup

Add the following permission to your AndroidManifest.xml:

```xml
<uses-permission android:name="android.permission.health.READ_HEALTH_DATA_IN_BACKGROUND" />
```


## Flutter

Add `includeBackgroundDelivery: true` when asking for permissions:

```dart
try {
  final request = await connection.requestHealthPermissions(
      statisticTypes: [StatisticsType.steps],
      includeBackgroundDelivery: true,
  );
} on SpikeException catch (e) {
  log("Error: $e");
}
```

You can also ask for background delivery permission at the same time as other permissions.


Now you can enable background delivery:

```dart
try {
  await connection.enableBackgroundDelivery(
      statisticTypes: [StatisticsType.steps],
      sleepConfigs: [
        SleepConfig(includeMetricTypes: {MetricType.heartrate})
      ],
  );

} on SpikeException catch (e) {
  log("Error: $e");
}
```

Keep in mind that calling `enableBackgroundDelivery` will overwrite previous configuration. So you have to call it with all the data types you want in one call.

To check current configuration call `getBackgroundDeliveryConfig()` method. 

To stop background delivery call `disableBackgroundDelivery()` method.

