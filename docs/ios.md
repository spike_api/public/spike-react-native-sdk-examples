#  SpikeSDK iOS (v3)

## Requirements

iOS 13.0+

# Setup Guide

## Installation

### CocoaPods

CocoaPods is a dependency manager for Cocoa projects. To integrate SpikeSDK into your Xcode project using CocoaPods, specify it in your Podfile:

`pod 'SpikeSDK'`

Use `pod install` and `pod update` commands to install/update pods afterward.


### Swift Package Manager

To integrate SpikeSDK into your Xcode project using Swift Package Manager, add it in your Package.swift or through the Project's Package Dependencies tab:

```
dependencies: [
    .package(url: "https://gitlab.com/spike_api/spike-ios-sdk", .upToNextMinor(from: "4.0.0"))
]
```


## iOS Signing & Capabilities

To add HealthKit support to your application's Capabilities.

* Select the project name in the left sidebar
* Open Signing & Capabilities section
* In the main view select '+ Capability' and double click HealthKit

More details you can find [here](https://developer.apple.com/documentation/healthkit/setting_up_healthkit).


## Info.plist

Add Health Kit permissions descriptions to your Info.plist file.

```
<key>NSHealthShareUsageDescription</key>
<string>We will use your health information to better track workouts.</string>

<key>NSHealthUpdateUsageDescription</key>
<string>We will update your health information to better track workouts.</string>
```


# Spike SDK Usage

Start getting Spike data in 3 steps. All Spike SDK method calls should be wrapped into try catch block.

### Step 1 - Create Spike connection

To set up the Spike SDK create `SpikeConnectionV3` instance with your Spike application id, auth token and user id unique to each of your users:

```swift
import SpikeSDK

let spikeConnection = try await Spike.createConnectionV3(
    appId: "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    authToken: "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    customerEndUserId: userId
)
```

### Step 2 - ask user for permissions

Provide permissions to access iOS HealthKit data. Spike SDK method will check required permissions and request them if needed. Permission dialog may not be shown according on iOS permissions rules.

```swift
try await spikeConnectionTemp.requestPermissions(forStatistics: [
        .stepsTotal,
        .distanceWalking,
    ]
)
```


### Step 3 - Get data

Info: The maximum permitted date range is 90 days

There are two types of data you can retrieve from Spike: records and statistics. Records consist of the raw data points collected from user devices or applications. Statistics, on the other hand, are calculated values derived from these samples, providing insights into user behavior.


#### Statistics

```swift
func getStatistics(
    ofTypes types: [StatisticsType.steps],
    from: Date.now.addingTimeInterval(-60 * 60 * 24),
    to: Date.now,
    interval: StatisticsInterval.hour,
    filter: StatisticsFilter(providers: [.apple])
) async throws -> [Statistic]
```

where: 

```swift
public enum StatisticsType: String {
    case steps
    case distanceTotal = "distance_total"
    case distanceWalking = "distance_walking"
    case distanceCycling = "distance_cycling"
    case distanceRunning = "distance_running"
    case caloriesBurnedTotal = "calories_burned_total"
    case caloriesBurnedBasal = "calories_burned_basal"
    case caloriesBurnedActive = "calories_burned_active"
}

public enum StatisticsInterval: String, Codable {
    case hour
    case day
}

public struct StatisticsFilter {
    public var excludeManual: Bool = false
    public var providers: [Provider]? = nil
}

// Result:

public struct Statistic: Codable, Hashable {
    public var start: Date
    public var end: Date
    public var duration: Int
    public var type: StatisticsType
    public var value: Double
    public var unit: Unit
    public var rowCount: Int?
    public var recordIds: [UUID]?
}
```


#### Records

```swift 
func getRecords(
    ofTypes types: [MetricType.stepsTotal],
    from: Date.now.addingTimeInterval(-60 * 60 * 24),
    to: Date.now,
    filter: StatisticsFilter(providers: [.apple])
) async throws -> [Record]
```

where:

```swift 

public enum MetricType: String, Codable, Hashable {
    case stepsTotal
    case distanceTotal
    case distanceWalking
    case distanceCycling
    case distanceRunning
    case caloriesBurnedActive
    case caloriesBurnedBasal
    case caloriesBurnedTotal
}

// Result

public struct Record: Codable, Hashable {
    public var recordId: UUID
    public var inputMethod: InputMethod?
    public var startAt: Date
    public var endAt: Date?
    public var modifiedAt: Date
    public var duration: Int?
    public var provider: Provider?
    public var providerSource: ProviderSource?
    public var isSourceAggregated: Bool?
    public var source: RecordSource?
    public var metrics: [String: Double]?
    public var activityTags: [ActivityTag]?
    public var activityType: ActivityType?
    public var sessions: [ActivityEntry]?
    public var laps: [ActivityEntry]?
    public var segments: [ActivityEntry]?
    public var splits: [ActivityEntry]?
    public var samples: [ActivitySamples]?
    public var routePoints: [ActivitySamples]?
}

```

# Background delivery

Background delivery ensures that data updates are sent to your backend via webhooks, even when the application is in the background or closed.

## Important notes about background delivery on iOS

* For most data types the most possible frequency of updates is 1 hour.
* iOS can update data more frequently for some data types, for example, vo2 max.
* iOS may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Background delivery is not possible while device is locked, so it will be executed only when the device is unlocked.
* iOS may stop background delivery if it detects that the app is not active for a long time.
* The feature is available starting with iOS 15.

> **Important:** The SpikeSDK, along with any other HealthKit applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by iOS. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


## Setup

### Enable background delivery for app target

* Open the folder of your project in Xcode
* Select the project name in the left sidebar
* Open Signing & Capabilities section 
* Select HealthKit background delivery under HealthKit section

### Initialization at app startup

#### UIKit based apps

Add Spike initialization code to your AppDelegate inside `application:didFinishLaunchingWithOptions:` method:

```swift
import SpikeSDK
...

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    ...
    Spike.configure()
    ...
}```

#### SwiftUI based apps

For SwiftUI based apps follow few steps:

Create a custom class that inherits from NSObject and conforms to the UIApplicationDelegate protocol:

```swift
import SpikeSDK
...

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Spike.configure()
        return true
    }
}
```

And now in your App struct, use the `UIApplicationDelegateAdaptor` property wrapper to tell SwiftUI it should use your `AppDelegate` class for the application delegate:

```swift
@main
struct YourApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    ...
}
```

### Ask SpikeSDK for background delivery

To enable background delivery, you need to call the `enableBackgroundDelivery` method:

```swift
do {
    try await spikeConnection.enableBackgroundDelivery(
        forStatisticsTypes: [.steps]
    )
} catch {
    print("\(error)")
}
```

Calling `enableBackgroundDelivery` will overwrite all the previous settings, so you have to call it with all the data types that you want in one call.
It accepts all the possible data types that can be delivered in the background (statistics, records, activities and sleep).

```swift
func enableBackgroundDelivery(
        forStatistics: [StatisticsType]?,
        forMetrics: [MetricType]?,
        forActivities: [ActivityConfig]?,
        forSleep: [SleepConfig]?
    ) async throws

or

public struct BackgroundDeliveryConfig: Codable, Hashable, Sendable {
    public var statisticsTypes: [StatisticsType]?
    public var metricsTypes: [MetricType]?
    public var activityConfigs: [ActivityConfig]?
    public var sleepConfigs: [SleepConfig]?
}
```

You can also call `getBacgroundDeliveryConfig()` to get the current configuration and `disableBacgroundDelivery()` to disable background delivery.



# Reading logs

If you want to receive logs from SpikeSDK, you can use the following code:

```swift
Spike.setLogCallback { level, message in
    print("\(message)")
}
```

If you are using background delivery, this code should be run in your application's `didFinishLaunchingWithOptions` method before you start using SpikeSDK:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    ...
    Spike.setLogCallback { level, message in
        print("\(message)")
    }
    Spike.configure()
    ...
}
```

