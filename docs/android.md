#  SpikeSDK Android (v3)

## Requirements

Android 9.0+ (Level 28, P, Pie)

# Setup Guide

To add the SDK to your project, you have to add the following to your project's `build.gradle` file in the repositories block.

```gradle
allprojects {
    repositories {
        // Other repositories
        maven {
            url 'https://gitlab.com/api/v4/projects/43396247/packages/maven'
        }
    }
}
```

After that, add the following to your app's `build.gradle` file in the dependencies block.

```
dependencies {
    implementation "com.spikeapi.sdk:spike-sdk:4.0.12-beta.2"
}
```

## Android permissions

Include the necessary health permissions in your AndroidManifest.xml to fully leverage the Spike SDK and access data from apps integrated with Health Connect. Please refer to [this guide](https://developer.android.com/health-and-fitness/guides/health-connect/get-started#declare-permissions) for details on the required permissions.

**Note**: Only request permissions essential to your app’s functionality. Requesting unused permissions will lead to Play Store rejection!

```xml

    <uses-permission android:name="android.permission.health.READ_NUTRITION"/>
    <uses-permission android:name="android.permission.health.READ_ACTIVE_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_TOTAL_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_STEPS"/>
    <uses-permission android:name="android.permission.health.READ_DISTANCE"/>
    <uses-permission android:name="android.permission.health.READ_ELEVATION_GAINED"/>
    <uses-permission android:name="android.permission.health.READ_RESTING_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE_VARIABILITY"/>
    <uses-permission android:name="android.permission.health.READ_FLOORS_CLIMBED"/>
    <uses-permission android:name="android.permission.health.READ_BASAL_METABOLIC_RATE"/>
    <uses-permission android:name="android.permission.health.READ_SLEEP"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_EXERCISE"/>
    <uses-permission android:name="android.permission.health.READ_SPEED"/>
    <uses-permission android:name="android.permission.health.READ_POWER"/>
    <uses-permission android:name="android.permission.health.READ_OXYGEN_SATURATION"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_GLUCOSE"/>
    <uses-permission android:name="android.permission.health.READ_RESPIRATORY_RATE"/>
    <uses-permission android:name="android.permission.health.READ_WEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_HEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_BODY_FAT"/>
    <uses-permission android:name="android.permission.health.READ_LEAN_BODY_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_WATER_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_TEMPERATURE"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_PRESSURE"/>
    <uses-permission android:name="android.permission.health.READ_BONE_MASS"/>

```

Add an intent filter to your activity definition so that you can request the permissions at runtime.

```xml
<intent-filter>
    <action android:name="androidx.health.ACTION_SHOW_PERMISSIONS_RATIONALE" />
</intent-filter>
```

To handle Android 14 you also need to add activity-alias to your AndroidManifest.xml It is just a wrapper for the activity that requests permissions so no real activity is necessary.

```xml
<activity-alias
    android:name="ViewPermissionUsageActivity"
    android:exported="true"
    android:targetActivity=".MainActivity"
    android:permission="android.permission.START_VIEW_PERMISSION_USAGE">
    <intent-filter>
        <action android:name="android.intent.action.VIEW_PERMISSION_USAGE" />
        <category android:name="android.intent.category.HEALTH_PERMISSIONS" />
    </intent-filter>
</activity-alias>
```

# Spike SDK Usage

### Step 1 - Create Spike connection

```kotlin
// Replace with your API credentials provided exclusively to you
private const val AUTH_TOKEN = "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
private const val APP_ID = "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

val spikeConnection = SpikeConnectionAPIv3.createConnection(
    appId = APP_ID,
    authToken = AUTH_TOKEN,
    customerEndUserId = "user-id",
    context = context
)
```

### Step 2 - ask user for permissions

If you want to read data from Android Health Connect, you have to ensure user gives your app permissions. 

First, you have to check if Health Connect is available on users phone:
```kotlin
val hcAvailability = spikeConnection.checkHealthConnectAvailability()
```

where
```kotlin
public enum class HealthConnectAvailabilityStatus(public val value: String) {
    /**
     * The Health Connect SDK is unavailable on this device at the time. This can be due to the
     * device running a lower than required Android Version. Apps should hide any integration
     * points to Health Connect in this case.
     */
    NOT_INSTALLED("NOT_INSTALLED"),

    /**
     * The Health Connect SDK APIs are currently unavailable, the provider is either not installed
     * or needs to be updated. Apps may choose to redirect to package installers to find a suitable
     * APK.
     */
    UPDATE_REQUIRED("UPDATE_REQUIRED"),

    /**
     * The Health Connect SDK APIs are available.
     */
    INSTALLED("INSTALLED"),
}
```

If update is required, you can use Spike helper to open Play Store for user to install Health Connect:

```kotlin
spikeConnection.openHealthConnectInstallation()
```

If Health Connect is installed, you can get permissions that are needed and a list of already provided permissions:
```kotlin
// HC integration has to be enabled in SpikeSDK connection before 
// using further methods for reading data or managing permissions:
spikeConnection.enableHealthConnectIntegration()

val permissionManager = spikeConnection.getHealthConnectPermissionManager()
val requiredPermissions = permissionManager.getPermissions(
    statisticsTypes = setOf(StatisticsType.STEPS, StatisticsType.DISTANCE_TOTAL)
)

val grantedPermissions = permissionManager.getGrantedPermissions()
```

If you have missing permissions you can ask Android to present user with a modal asking user for permission to read the data. Example for Compose:

```kotlin
val permissionLauncher = rememberLauncherForActivityResult(
    permissionManager.getRequestPermissionResultContract(),
    onResult = {
        
    }
)
```

* - Please note that users might only grant partial permissions. In such cases, it’s up to you to decide whether your app can function effectively with limited access. The SpikeSDK itself will still operate even without full permissions; however, it may result in no data being returned in certain scenarios. Conversely, if your app has been granted additional permissions beyond the minimum required for specific data types, we may enhance certain entries by incorporating data sourced from other types (e.g., identifying manually entered data).

You can now use `StatisticsFilter(providers = listOf(Provider.HEALTH_CONNECT))` to specifically retrieve data from Health Connect. Alternatively, you can omit the providers parameter entirely and allow Spike to choose the most suitable data source based on your request.


### Step 3 - Get data

Info: The maximum permitted date range is 90 days

There are two types of data you can retrieve from Spike: records and statistics. Records consist of the raw data points collected from user devices or applications. Statistics, on the other hand, are calculated values derived from these samples, providing insights into user behavior.

#### Statistics

Get daily statistics for steps and total distance from health Connect:

```kotlin
val dailyStatistics = spikeConnection.getStatistics(
    types = setOf(StatisticsType.STEPS, StatisticsType.DISTANCE_TOTAL),
    from = LocalDate.now().minusWeeks(1).atStartOfDay(ZoneId.systemDefault()).toInstant(),
    to = Instant.now(),
    interval = StatisticsInterval.DAY,
    filter = StatisticsFilter(providers = listOf(Provider.HEALTH_CONNECT))
)
```

where:

```kotlin

enum class StatisticsType(public val value: String) {
    STEPS("steps"),
    DISTANCE_TOTAL("distance_total"),
    DISTANCE_WALKING("distance_walking"),
    DISTANCE_CYCLING("distance_cycling"),
    DISTANCE_RUNNING("distance_running"),
    CALORIES_BURNED_TOTAL("calories_burned_total"),
    CALORIES_BURNED_BASAL("calories_burned_basal"),
    CALORIES_BURNED_ACTIVE("calories_burned_active")
}

// Interval
StatisticsInterval.HOUR
StatisticsInterval.DAY

public data class StatisticsFilter(
    val excludeManual: Boolean = false,
    val providers: List<Provider>? = null
)

// Result:

public data class Statistic(
    val start: Instant,
    val end: Instant,
    val duration: Int,
    val type: StatisticsType,
    val value: Double,
    val unit: com.spikeapi.apiv3.datamodels.Unit,
    val rowCount: Int?,
    val recordIds: List<UUID>?
)


```

### Records

Get all records we have from Garmin provider:

```kotlin
val records = spikeConnection.getRecords(
    types = setOf(MetricType.STEPS_TOTAL, MetricType.CALORIES_BURNED_TOTAL),
    from = LocalDate.now().minusWeeks(1).atStartOfDay(ZoneId.systemDefault()).toInstant(),
    to = Instant.now(),
    filter = StatisticsFilter(providers = listOf(Provider.GARMIN))
)
```

where:

```kotlin

public enum class MetricType(public val value: String) {
    CALORIES_BURNED_ACTIVE("calories_burned_active"),
    CALORIES_BURNED_BASAL("calories_burned_basal"),
    CALORIES_BURNED("calories_burned"),
    CALORIES_INTAKE("calories_intake"),
    STEPS_TOTAL("steps"),
    DISTANCE_TOTAL("distance"),
    DISTANCE_WALKING("distance_walking"),
    DISTANCE_CYCLING("distance_cycling"),
    DISTANCE_RUNNING("distance_running"),
    DISTANCE_WHEELCHAIR("distance_wheelchair"),
    DISTANCE_SWIMMING("distance_swimming"),
}


// Result: 

public data class Record(
    val recordId: UUID,
    val inputMethod: InputMethod?,
    val startAt: Instant,
    val endAt: Instant?,
    val modifiedAt: Instant,
    val duration: Int?,
    val provider: Provider?,
    val providerSource: ProviderSource?,
    val isSourceAggregated: Boolean?,
    val source: RecordSource?,
    val metrics: Map<String, Double>?,
    val activityTags: List<ActivityTag>?,
    val activityType: ActivityType?,
    val sessions: List<ActivityEntry>?,
    val laps: List<ActivityEntry>?,
    val segments: List<ActivityEntry>?,
    val splits: List<ActivityEntry>?,
    val samples: List<ActivitySamples>?,
    val routePoints: List<ActivitySamples>?
)

```


# Backgrdound delivery

Background delivery ensures that data updates are sent to your backend via webhooks, even when the application is in the background or closed.


## Important notes about background delivery on Android

* Background delivery is scheduled to run every hour. But ultimately Android decides when the delivery will be executed.
* Android may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Android may stop background delivery if it detects that the app is not active for a long time.
* There is a limit of queries that can be done in Health Connect and it is different for foreground and background reads, so please request only essential data to be delivered in background. More information on (Health Connect documentation)[https://developer.android.com/health-and-fitness/guides/health-connect/plan/rate-limiting]

> **Important:** The SpikeSDK, along with any other applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by Android. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


## Setup

Add the following permission to your AndroidManifest.xml:

```xml
<uses-permission android:name="android.permission.health.READ_HEALTH_DATA_IN_BACKGROUND" />
```

Check if background delivery is possible with current Health Connect version:

```kotlin
val hcAvailability = spikeConnection.getHealthConnectPermissionManager()
                    .isFeatureAvailable(HealthConnectFeature.READ_HEALTH_DATA_IN_BACKGROUND)
```

If not, you can ask user to update Health Connect to the latest version.


Ask for background read permission:

```kotlin
val permissionManager = spikeConnection.getHealthConnectPermissionManager()
val requiredPermissions = permissionManager.getPermissions(
    // You can add other permissions as well: statistics, sleep, etc.
    includeBackgroundDelivery = true
)

val permissionLauncher = rememberLauncherForActivityResult(
    permissionManager.getRequestPermissionResultContract(),
    onResult = {
        ...
    }
)
permissionLauncher.launch(requiredPermissions)

```

You can also ask for background delivery permission in the same time as other permissions.


Now you can enable background delivery:

```kotlin
connection.enableBackgroundDelivery(
    BackgroundDeliveryConfig(
        statisticsTypes = setOf(StatisticsType.STEPS)
    )
)```

Keep in mind that calling `enableBackgroundDelivery` will overwrite previous configuration. If you want to add more types, you 
have to call `enableBackgroundDelivery` again with updated configuration:

```kotlin
connection.enableBackgroundDelivery(
    BackgroundDeliveryConfig(
        statisticsTypes = setOf(StatisticsType.STEPS),
        activityTypes = setOf(ActivityType.RUNNING, ActivityType.WALKING)
    )
)```

To check current configuration call `getBackgroundDeliveryConfig()` method. 

To stop background delivery call `disableBackgroundDelivery()` method.


# Reading logs

By default SPikeSDK logs are sent to standard Android Logcat. If for some reason you want to read them, you can use the following code (running it before you start using SpikeSDK):

```kotlin
SpikeConnectionAPIv3.setLogCallback { level, message ->
    when (level) {
        LogLevel.VERBOSE -> {}
        LogLevel.DEBUG -> if(BuildConfig.DEBUG) { 
            Log.d("SpikeSDK", message)
        }
        LogLevel.WARNING -> Log.w("SpikeSDK", message)
        LogLevel.ERROR -> Log.e("SpikeSDK", message)
    }
}
```

If you are utilising Background Delivery, this code should be run in your application's `onCreate` method:

```kotlin
class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        SpikeConnectionAPIv3.setLogCallback { level, message ->
            when (level) {
                LogLevel.VERBOSE -> {}
                LogLevel.DEBUG -> if(BuildConfig.DEBUG) { 
                    Log.d("SpikeSDK", message)
                }
                LogLevel.WARNING -> Log.w("SpikeSDK", message)
                LogLevel.ERROR -> Log.e("SpikeSDK", message)
            }
        }
    }
}
```

Also application should be set in AndroidManifest.xml:

```xml
<application
    android:name=".MyApplication"
    ...
>
```
