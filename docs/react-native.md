#  SpikeSDK React Native (v3)

## Requirements

iOS 13.0+
Android 9.0+ (Level 28, P, Pie)

# Installation

Install the react-native-spike-sdk package from [npm](https://www.npmjs.com/package/react-native-spike-sdk):

```
yarn add react-native-spike-sdk
```

# iOS Setup Guide

Use `pod install` and `pod update` commands from `ios/` folder of your app to install/update SpikeSDK.

## iOS Signing & Capabilities

To add HealthKit support to your application's Capabilities.

* Open the iOS/ folder of your project in Xcode
* Select the project name in the left sidebar
* Open Signing & Capabilities section
* In the main view select '+ Capability' and double click HealthKit

More details you can find [here](https://developer.apple.com/documentation/healthkit/setting_up_healthkit).

## Info.plist

Add Health Kit permissions descriptions to your Info.plist file:

```
<key>NSHealthShareUsageDescription</key>
<string>We will use your health information to better track workouts.</string>

<key>NSHealthUpdateUsageDescription</key>
<string>We will update your health information to better track workouts.</string>
```

# Android Setup Guide

To add the SDK to your Android project, you have to add the following to your project's `build.gradle` file in the repositories block.

```
allprojects {
    repositories {
        // Other repositories
        maven {
            url 'https://gitlab.com/api/v4/projects/43396247/packages/maven'
        }
    }
}
```

## Android permissions

Include the necessary health permissions in your AndroidManifest.xml to fully leverage the Spike SDK and access data from apps integrated with Health Connect. Please refer to [this guide](https://developer.android.com/health-and-fitness/guides/health-connect/get-started#declare-permissions) for details on the required permissions.

**Note**: Only request permissions essential to your app’s functionality. Requesting unused permissions will lead to app store rejection!

```xml
    <uses-permission android:name="android.permission.health.READ_NUTRITION"/>
    <uses-permission android:name="android.permission.health.READ_ACTIVE_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_TOTAL_CALORIES_BURNED"/>
    <uses-permission android:name="android.permission.health.READ_STEPS"/>
    <uses-permission android:name="android.permission.health.READ_DISTANCE"/>
    <uses-permission android:name="android.permission.health.READ_ELEVATION_GAINED"/>
    <uses-permission android:name="android.permission.health.READ_RESTING_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE_VARIABILITY"/>
    <uses-permission android:name="android.permission.health.READ_FLOORS_CLIMBED"/>
    <uses-permission android:name="android.permission.health.READ_BASAL_METABOLIC_RATE"/>
    <uses-permission android:name="android.permission.health.READ_SLEEP"/>
    <uses-permission android:name="android.permission.health.READ_HEART_RATE"/>
    <uses-permission android:name="android.permission.health.READ_EXERCISE"/>
    <uses-permission android:name="android.permission.health.READ_SPEED"/>
    <uses-permission android:name="android.permission.health.READ_POWER"/>
    <uses-permission android:name="android.permission.health.READ_OXYGEN_SATURATION"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_GLUCOSE"/>
    <uses-permission android:name="android.permission.health.READ_RESPIRATORY_RATE"/>
    <uses-permission android:name="android.permission.health.READ_WEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_HEIGHT"/>
    <uses-permission android:name="android.permission.health.READ_BODY_FAT"/>
    <uses-permission android:name="android.permission.health.READ_LEAN_BODY_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_WATER_MASS"/>
    <uses-permission android:name="android.permission.health.READ_BODY_TEMPERATURE"/>
    <uses-permission android:name="android.permission.health.READ_BLOOD_PRESSURE"/>
    <uses-permission android:name="android.permission.health.READ_BONE_MASS"/>

```

Add an intent filter to your activity definition so that you can request the permissions at runtime.

```xml
<intent-filter>
    <action android:name="androidx.health.ACTION_SHOW_PERMISSIONS_RATIONALE" />
</intent-filter>
```

To handle Android 14 you also need to add activity-alias to your AndroidManifest.xml It is just a wrapper for the activity that requests permissions so no real activity is necessary.

```xml
<activity-alias
    android:name="ViewPermissionUsageActivity"
    android:exported="true"
    android:targetActivity=".MainActivity"
    android:permission="android.permission.START_VIEW_PERMISSION_USAGE">
    <intent-filter>
        <action android:name="android.intent.action.VIEW_PERMISSION_USAGE" />
        <category android:name="android.intent.category.HEALTH_PERMISSIONS" />
    </intent-filter>
</activity-alias>
```


# Spike SDK Usage 

### Step 1 - Create Spike connection

```javascript
import Spike from 'react-native-spike-sdk';

const connection = await Spike.createConnectionAPIv3(
    "APP_ID",
    "APP_SECRET", 
    "APP_USER"
);
```

### Step 2 - ask user for permissions

If you plan to use Android Health Connect and/or Apple HealthKit data providers you should first get users permission to use their data:

```javascript
// This call may present user with OS native modal asking for read 
// permissions needed to get steps and distance data. If you want to 
// present it only once, you should list all the statistic types you 
// plan to use. Otherwise, you can call this only before you actually 
// want to use the data.
spikeConnection.requestHealthPermissions([StatisticsType.steps, StatisticsType.distanceTotal])
``` 

* - for Android there are more methods that let you get permissions granted by user, check if Health Connect is installed or should be updated, etc. If you are OK with SpikeSDK handling these, you can use `requestHealthPermissions` only. On iOS `requestHealthPermissions` is the only method that has to be called. Keep in mind, that you should do it before trying to read data from Android Health Connect or Apple HealthKit.


### Step 3 - Get data

Info: The maximum permitted date range is 90 days

There are two types of data you can retrieve from Spike: records and statistics. Records consist of the raw data points collected from user devices or applications. Statistics, on the other hand, are calculated values derived from these samples, providing insights into user behavior.


#### Statistics

Now you can read hourly statistics data of steps and distance for today:

```javascript
const now = new Date();
const start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
const end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

const statistics = await spikeConnection.getStatistics(
    [StatisticsType.steps, StatisticsType.distanceTotal],
    start,
    end,
    StatisticsInterval.hour,
    new StatisticsFilter(false, [Provider.apple, Provider.healthConnect])
)
```

where:

```javascript

enum StatisticsType {
  steps = "steps",
  distanceTotal = "distance_total",
  distanceWalking = "distance_walking",
  distanceCycling = "distance_cycling",
  distanceRunning = "distance_running",
  caloriesBurnedTotal = "calories_burned_total",
  caloriesBurnedBasal = "calories_burned_basal",
  caloriesBurnedActive = "calories_burned_active",
}

enum StatisticsInterval {
    day, 
    hour
}

// Filter
export class StatisticsFilter {
    excludeManual: boolean = false
    providers: Provider[] | undefined

    constructor({
        excludeManual = false, 
        providers = undefined
    }: StatisticsFilterConstructorParameters) {
        this.excludeManual = excludeManual;
        this.providers = providers;
    }
}

// Result:

interface Statistic {
  start: string;
  end: string;
  duration: number;
  type: StatisticsType;
  value: number;
  unit: Unit;
  rowCount: number | null;
  recordIds: UUID[] | null;
}

```


#### Records

```javascript
const now = new Date();
const start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
const end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

const records = await spikeConnection.getRecords({
    ofTypes: metricTypes,
    from: start,
    to: end,
    filter: new StatisticsFilter({
        excludeManual: false,
        providers: [Provider.apple, Provider.healthConnect]
    })
})

```

where:

```javascript
enum MetricType {
  caloriesBurnedActive = "calories_burned_active",
  caloriesBurnedBasal = "calories_burned_basal",
  caloriesBurned = "calories_burned",
  distanceTotal = "distance",
  distanceWalking = "distance_walking",
  distanceCycling = "distance_cycling",
  distanceRunning = "distance_running",
  distanceWheelchair = "distance_wheelchair",
  distanceSwimming = "distance_swimming",
  stepsTotal = "steps",
}


// Result: 

interface SpikeRecord {
  recordId: UUID;
  inputMethod: InputMethod | null;
  startAt: string;
  endAt: string | null;
  modifiedAt: string;
  duration: number | null;
  provider: Provider | null;
  providerSource: ProviderSource | null;
  isSourceAggregated: boolean | null;
  source: RecordSource | null;
  metrics: { [key: string]: number } | null;
  activityTags: ActivityTag[] | null;
  activityType: ActivityType | null;
  sessions: ActivityEntry[] | null;
  laps: ActivityEntry[] | null;
  segments: ActivityEntry[] | null;
  splits: ActivityEntry[] | null;
  samples: ActivitySamples[] | null;
  routePoints: ActivitySamples[] | null;
}
```


# Backgrdound delivery

Background delivery ensures that data updates are sent to your backend via webhooks, even when the application is in the background or closed.

## iOS

### Important notes about background delivery on iOS

* For most data types the most possible frequency of updates is 1 hour.
* iOS can update data more frequently for some data types, for example, vo2 max.
* iOS may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Background delivery is not possible while device is locked, so it will be executed only when the device is unlocked.
* iOS may stop background delivery if it detects that the app is not active for a long time.
* The feature is available starting with iOS 15.

> **Important:** The SpikeSDK, along with any other HealthKit applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by iOS. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


### Setup

#### Enable background delivery for app target

* Open XCode with your ios project
* Open the folder of your project in Xcode
* Select the project name in the left sidebar
* Open Signing & Capabilities section 
* Select HealthKit background delivery under HealthKit section

### Initialization at app startup

> **Info:** You can skip this step if you are using Expo.

For background delivery to work properly, you need to initialize the SpikeSDK at app startup. 

`AppDelegate.swift`:
```swift
import SpikeSDK
...
  override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    ...
    Spike.configure()
    ...
  }
...
```
 or `AppDelegate.mm`:
 ```
#import <SpikeSDK/SpikeSDK-Swift.h>
...
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  ...
  [Spike configure];
  ...

}
 ``` 

## Android

### Important notes about background delivery on Android

* Background delivery is scheduled to run every hour. But ultimately Android decides when the delivery will be executed.
* Android may throttle the frequency of updates for background delivery depending on the app's activity, battery state, etc.
* Android may stop background delivery if it detects that the app is not active for a long time.
* There is a limit of queries that can be done in Health Connect and it is different for foreground and background reads, so please request only essential data to be delivered in background. More information on (Health Connect documentation)[https://developer.android.com/health-and-fitness/guides/health-connect/plan/rate-limiting]


> **Important:** The SpikeSDK, along with any other applications, cannot guarantee data synchronization on a fixed schedule. The hourly sync interval serves as a guideline rather than a strict requirement enforced by Android. Consequently, the actual synchronization frequency may vary, occurring hourly, once per day, or during specific system-defined events, such as the conclusion of Sleep Mode or when the device begins charging.


### Setup

Add the following permission to your AndroidManifest.xml:

```xml
<uses-permission android:name="android.permission.health.READ_HEALTH_DATA_IN_BACKGROUND" />
```

## React Native

Add `includeBackgroundDelivery: true` when asking for permissions:

```typescript
spikeConnection.requestHealthPermissions({
    statisticTypes: statisticTypes,
    includeBackgroundDelivery: true,
})
```

You can also ask for background delivery permission at the same time as other permissions.


Now you can enable background delivery:

```javascript
try {
    await spikeConnection.enableBackgroundDelivery({
        statisticTypes: [StatisticsType.steps, StatisticsType.distanceTotal],
        sleepConfigs: [new SleepConfig({
            includeMetricTypes: [], 
        })],
    })

    toast.show("Background delivery enabled")
    } catch (error) {
    console.log(`${error}`);
    toast.show(`${error}`);
} catch (error) {
    console.log(`${error}`);
}
```

Keep in mind that calling `enableBackgroundDelivery` will overwrite previous configuration. So you have to call it with all the data types you want in one call.

To check current configuration call `getBackgroundDeliveryConfig()` method. 

To stop background delivery call `disableBackgroundDelivery()` method.
