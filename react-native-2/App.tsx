import * as React from 'react';
import BaseComponent from './src/BaseComponent';

export default function App() {
  return <BaseComponent />;
}
