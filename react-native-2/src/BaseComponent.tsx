import * as React from 'react';
import { useState } from 'react';
import {
  ActivityIndicator,
  Button,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Spike, { SpikeConnection, SpikeDataTypes } from 'react-native-spike-sdk';
import ConsoleLogger from './ConsoleLogger';

const clientId = 'ea9e03f5-be45-49fb-bf4c-47a88c184c3b';
const authToken = 'fa0b3803-6068-4ea7-9788-eccce210d30c';
const customerId = 'my_user_id';
const callbackUrl = 'https://webhook.site/67ab4a95-e916-4918-8774-f02da5e65b69';

const BaseComponent = () => {
  const [spikeConnection, setSpikeConnection] = useState<SpikeConnection>();
  const [outputData, setOutputData] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isHealthConnectAvailable, setIsHealthConnectAvailable] =
    useState(true);

  const checkHealthConnectPackage = async () => {
    if (Platform.OS === 'android') {
      try {
        const isPackageInstalled = await Spike.isPackageInstalled();
        if (!isPackageInstalled) {
          console.log('Health Connect is not available on this device.');
          setIsHealthConnectAvailable(false);
        }
      } catch (error) {
        console.log(`${error}`);
      }
    }
  };

  React.useEffect(() => {
    checkHealthConnectPackage();
  }, []);

  /*
    Create [SpikeConnection] instance with [SpikeConnectionConfig] object.
    From the 2.1.x version Spike SDK automatically manages connection persistance
    and restore connection if finds one with same `appId`, `authToken` and `customerEndUserId`.
    With each new connection creating call `callbackUrl` and `env` could be overridden.
    Provide [SpikeLogger] implementation to handle connection logs.
  */
  const onConnect = async () => {
    setIsLoading(true);
    try {
      const conn = await Spike.createConnection(
        {
          appId: clientId,
          authToken: authToken,
          customerEndUserId: customerId,
          callbackUrl: callbackUrl, // Optional, provides functionality to send data to webhook and use background deliveries.
        },
        new ConsoleLogger() // Optional, class which conforms to SpikeLogger interface
      );
      setSpikeConnection(conn);
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    Terminates any ongoing connections with Spike’s backend servers, clears any caches, 
    and removes provided user details and tokens from the memory. 
    Once the connection is closed, it cannot be used, 
    and any method other than `close()` will throw a _SpikeConnectionIsClosedException_ exception.
  */
  const onCloseConnection = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      await spikeConnection.close();
      setSpikeConnection(undefined);
      setOutputData('');
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    Use connection methods to access connection info.
  */
  const onGetConnectionInfo = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      const appId = await spikeConnection.getAppId();
      const spikeEndUserId = await spikeConnection.getSpikeEndUserId();
      const customerEndUserId = await spikeConnection.getCustomerEndUserId();
      // const callbackUrl = await spikeConnection.getCallbackUrl() // Use if callback url provided
      const output = {
        appId,
        spikeEndUserId,
        customerEndUserId,
        // callbackUrl,
      };
      setOutputData(JSON.stringify(output, null, 2));
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    Provide permissions to access iOS HealthKit and Android HealthConnect data. 
    Spike SDK methods will check required permissions and request them if needed. 
    Permission dialog may not be shown according on platform permissions rules.
  */
  const onRequestPermissions = async () => {
    setIsLoading(true);
    try {
      if (Platform.OS === 'android') {
        if (!spikeConnection) return;
        // Android methods should be called on connection instance

        // Check and request permissions for each data type separately
        const isGranted = await spikeConnection.checkPermissionsGranted(
          SpikeDataTypes.steps
        );
        if (!isGranted) {
          await spikeConnection.requestHealthPermissions(SpikeDataTypes.steps);
        }

        // Or request permissions for multiple data types
        // await spikeConnection.requestHealthPermissions([SpikeDataTypes.steps, SpikeDataTypes.calories]);
      }

      if (Platform.OS === 'ios') {
        // iOS method should be called on Spike class
        await Spike.ensurePermissionsAreGranted([SpikeDataTypes.steps]); // Provide required Spike data types
      }
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    There are two ways to get Spike data: to your webhook or directly in your app.
    Extract data methods requires [SpikeExtractConfig] object, where date range can be provided optionally.
    By providing range greater than one day samples array of result object will be empty.
    The maximum permitted date range for iOS is 90 days, while for Android it's 30 days.
  */
  const onExtractData = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      const data = await spikeConnection.extractData({
        dataType: SpikeDataTypes.steps,
      });

      // Or provide time range

      // const today = new Date();
      // const pastDay = new Date();
      // pastDay.setDate(today.getDate() - 1);
      // const data = await spikeConnection.extractData({
      //   dataType: SpikeDataTypes.steps,
      //   from: pastDay,
      //   to: today,
      // });
      setOutputData(JSON.stringify(data, null, 2));
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  const onSendWebhook = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      const data = await spikeConnection.extractAndPostData({
        dataType: SpikeDataTypes.steps,
      });

      // Or provide time range

      // const today = new Date();
      // const pastDay = new Date();
      // pastDay.setDate(today.getDate() - 1);
      // const data = await spikeConnection.extractAndPostData({
      //   dataType: SpikeDataTypes.steps,
      //   from: pastDay,
      //   to: today,
      // });
      setOutputData(JSON.stringify(data, null, 2));
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    Provide required Spike Data types to enable background deliveries.
  */
  const onEnableBackgroundDeliveries = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      await spikeConnection.enableBackgroundDelivery([SpikeDataTypes.steps]);

      // You can check if connection have active background deliveries listeners.
      // If background delivery is not enabled, an empty set is returned.
      const bgDataTypes =
        await spikeConnection.getBackgroundDeliveryDataTypes();
      console.log(`Background data types: ${JSON.stringify(bgDataTypes)}`);
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  /*
    Provide empty to disable background deliveries.
  */
  const onDisableBackgroundDeliveries = async () => {
    if (!spikeConnection) return;

    setIsLoading(true);
    try {
      await spikeConnection.enableBackgroundDelivery([]);
    } catch (error) {
      console.error(`Spike error: ${error}`);
    }
    setIsLoading(false);
  };

  return (
    <SafeAreaView style={styles.container}>
      {!isHealthConnectAvailable && (
        <Text style={styles.notAvailable}>Health Connect is not available</Text>
      )}
      <View style={styles.actionsContainer}>
        {!spikeConnection ? (
          <Button title='Connect' onPress={onConnect} />
        ) : (
          <>
            <Button title='Get connection info' onPress={onGetConnectionInfo} />
            <Button title='Close connection' onPress={onCloseConnection} />
            <Button
              title='Request permissions'
              onPress={onRequestPermissions}
            />
            <Button title='Extract data' onPress={onExtractData} />
            <Button title='Send data to webhook' onPress={onSendWebhook} />
            {Platform.OS === 'ios' && (
              <>
                <Button
                  title='Enable background deliveries'
                  onPress={onEnableBackgroundDeliveries}
                />
                <Button
                  title='Disable background deliveries'
                  onPress={onDisableBackgroundDeliveries}
                />
              </>
            )}
          </>
        )}
      </View>
      {outputData && (
        <View style={styles.outputContainer}>
          <Text>Output</Text>
          {Platform.OS === 'ios' ? (
            <TextInput
              style={styles.dataTextView}
              multiline={true}
              editable={false}
              scrollEnabled={true}
            >
              {outputData}
            </TextInput>
          ) : (
            <ScrollView
              contentContainerStyle={{ flexGrow: 1, padding: 8 }}
              style={styles.dataTextScrollView}
            >
              <Text style={styles.dataText}>{outputData}</Text>
            </ScrollView>
          )}
        </View>
      )}
      {isLoading && (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='large' color={'white'} />
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  actionsContainer: {
    paddingTop: 16,
    paddingHorizontal: 16,
    gap: 8,
  },
  outputContainer: {
    flex: 1,
    marginTop: 24,
    paddingHorizontal: 16,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0 ,0, 0.7)',
  },
  dataTextView: {
    flex: 1,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1,
    borderRadius: 8,
    padding: 8,
    marginTop: 8,
    marginBottom: 8,
    fontSize: 15,
    color: '#000',
  },
  dataTextScrollView: {
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 8,
    marginBottom: 8,
  },
  dataText: {
    fontSize: 15,
    color: '#000',
  },
  notAvailable: {
    color: 'red',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 12,
  },
});

export default BaseComponent;
