import { SpikeConnection, SpikeLogger } from 'react-native-spike-sdk';

class ConsoleLogger implements SpikeLogger {
  isDebugEnabled() {
    return true;
  }

  isInfoEnabled() {
    return true;
  }

  isErrorEnabled() {
    return true;
  }

  debug(connection: SpikeConnection, message: String) {
    console.log(`[SPIKE_DEBUG] ${message}`);
  }

  info(connection: SpikeConnection, message: String) {
    console.log(`[SPIKE_INFO] ${message}`);
  }

  error(connection: SpikeConnection, message: String) {
    console.log(`[SPIKE_ERROR] ${message}`);
  }
}

export default ConsoleLogger;
